namespace FileImporter.AlternateDataStreamImportExport
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Trinet.Core.IO.Ntfs;





    internal class AlternateDataStreamWriter
    {
        private readonly EnhancedFileInfo _file;

        public AlternateDataStreamWriter(EnhancedFileInfo file)
        {
            _file = file;
        }


        private XDocument Document { get; set; }


        private AlternateDataStreamInfo StreamInfo { get; set; }


        private EnhancedFileInfo File
        {
            get
            {
                return _file;
            }
        }




        private bool StreamAlreadyExisted { get; set; }


        public void WriteItem()
        {
            if (!File.FileInformation.Exists)
                return;

            try
            {
                if (!GetStreamInfo())
                    return;


                GetDocument();
                WriteTag("name", File.Item.Name, true);
                WriteTag("location", File.Item.Location, true);
                WriteTag("externalid", File.Item.ExternalID, true);
                WriteTag("imagefile", File.Item.ImageFile, true);

                WriteTag
                    ("timestamp",
                    File.Item.
                    TimeStamp.ToString
                    ("yyyy-MM-dd hh:mm:ss"),
                    true);


                foreach (string tagName in File.Item.Tags.Keys)
                {


                    string tagValue = File.Item.Tags[tagName] is string
                                          ? (string) File.Item.Tags[tagName]
                                          : string.Empty;


                    WriteTag(tagName, tagValue, false);


                }
                using (var writer = new StreamWriter(StreamInfo.OpenWrite()))
                {
                    Document.Save(writer);
                }




            }
            catch (Exception e)
            {
            }




        }




        private void WriteTag(string tagName, string tagValue, bool isBaseTag)
        {
            XElement tagElement = GetTagElement(tagName, isBaseTag);
            tagElement.Value = tagValue;
        }





        private XElement GetTagElement(string tagName, bool isBaseTag)
        {
            XElement result = null;
            if (isBaseTag)
            {
                if (Document.Root != null)
                {
                    var xElement = Document.Root.Element("basetags");
                    if (xElement != null)
                        result = xElement.Elements("tag").First(el =>
                                                                    {
                                                                        var xAttribute = el.Attribute("name");
                                                                        return xAttribute != null && xAttribute.Value.Equals(tagName);
                                                                    });
                }
            }
            else
            {



                if (Document.Root != null)
                {


                    var xElement = Document.Root.Element("customtags");
                    
                    
                    if (xElement != null)
                        result = xElement.Elements("tag").FirstOrDefault
                            (el => {
                                        var xAttribute = el.Attribute("name");
                                        return xAttribute != null && xAttribute.Value.Equals(tagName);
                                    });


                    if (result == null)
                    {

                        XElement newTagElement = new XElement("tag");
                        newTagElement.SetAttributeValue("name", tagName);

                        var element = Document.Root.Element("customtags");

                        if (element != null)
                            element.Add(newTagElement);
                        
                        result = newTagElement;
                    
                    }



                }



            }



            return result;
        }







        private bool GetStreamInfo()
        {

            StreamAlreadyExisted 
                =  File.FileInformation
                .AlternateDataStreamExists
                ("MeediOS.Item");


            try
            {
                StreamInfo = File.FileInformation
                    .GetAlternateDataStream
                    ("MeediOS.Item", FileMode.OpenOrCreate);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }





        private void GetDocument()
        {


            if (StreamAlreadyExisted)
            {


                using (TextReader reader 
                    = StreamInfo.OpenText())
                {

                    Document = XDocument.Load(reader);

                }


            }
            else
            {
                /*
                XmlElement item = document.CreateElement("Item");
                document.AppendChild(item);
                XmlElement tags = document.CreateElement("Tags");
                document.DocumentElement.AppendChild(tags);
                */
                Document = XDocument.Parse("<item>" +
                                                "<basetags>" +
                                                "<tag name=\"name\">" +
                                                "</tag>" +
                                                "<tag name=\"location\">" +
                                                "</tag>" +
                                                "<tag name=\"externalid\">" +
                                                "</tag>" +
                                                "<tag name=\"imagefile\">" +
                                                "</tag>" +
                                                "<tag name=\"timestamp\">" +
                                                "</tag>" +
                                                "</basetags>" +
                                                "<customtags></customtags>" +
                                                "</item>");
            }




        }









    }




}