﻿using System.Collections.Generic;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;
using MeediOS;

namespace FileImporter
{


    class NonExistentItemRemover
    {



        internal static void DeleteNonExistentItems(TheImport theImport)
        {



            theImport.RelocatedFiles = new List<string>();
            theImport.DeletedFiles = 0;


            int oldItemCount
                = theImport.Importer
                .Section.ItemCount;



            for (int itemPos = oldItemCount - 1; itemPos >= 0; itemPos--)
            {



                IMLItem item
                    = theImport.Importer
                    .Section[itemPos];


                bool itemChanged = false;


                var locationTag 
                    = new MLTag
                        ("Location",
                        item.Location);


                int percentage = (itemPos * 10) / oldItemCount;


                string logMessage1 = 
                    "      Checking item: ID="
                    + item.ID
                    + " Location=" 
                    + locationTag.Value;


                if (!theImport.CheckProgress
                    (percentage, logMessage1))
                {
                    return;
                }



                theImport.Importer.LogMessages
                    .Enqueue(new LogMessage
                        ("Info", logMessage1));



                for (int filePos = locationTag.Values.Count - 1; filePos >= 0; filePos--)
                {


                    if (!theImport.CanDelete(locationTag.Values[filePos])) 
                        continue;


                    if (!theImport.Relocate(locationTag, filePos))
                    {
                        string logMessage2 = "Removing " + locationTag.Values[filePos] + " from item's location";
                        theImport.Importer.LogMessages.Enqueue(new LogMessage("Info", logMessage2));
                        locationTag.Values.RemoveAt(filePos);
                    }

                    itemChanged = true;
                }

                if (locationTag.Values.Count == 0)
                {
                    theImport.Importer.Section.DeleteItem(item);
                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Info", "Item deleted"));
                    theImport.DeletedFiles++;
                }
                else
                {
                    if (itemChanged)
                    {
                        item.Location = locationTag.Value;
                        item.ExternalID = locationTag.Value.ToLower();
                        item.SaveTags();
                    }


                }


            }


        }









    }




}
