﻿using System;
using System.Collections;
using System.Globalization;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;
using MeediOS;



namespace FileImporter
{




    class SingleFileImportWorker
    {



        /// <summary>
        /// Worker thread function that deques the EnhancedFileInfo objects and adds it's IMLItem to the import's section
        /// </summary>
        /// <param name="filesQueue"> </param>
        internal static void WorkerPerformSingleFileAndFileInfoImport(FilesQueue filesQueue)
        {


            while (true)
            {


                try
                {

                    EnhancedFileInfo file = null;


                    lock (((ICollection) filesQueue._files).SyncRoot)
                    {
                        if (filesQueue._files.Count > 0)
                        {
                            file = filesQueue._files.Dequeue();


                            if (file == null)
                                return;

                        }


                    }



                    if (file != null)
                    {


                        string importProgressMessage
                            = ((file.Item.ID == 0) 
                                   ? "Importing" : "Updating") 
                              + " file: " + file.FileInformation.FullName;


                        filesQueue._import.Importer.LogMessages.Enqueue(
                            new LogMessage("Info", importProgressMessage));


                        if (filesQueue._totalFiles > 0)
                        {

                            int percentage =
                                10 + (((filesQueue._import.AddedFiles
                                        + filesQueue._import.UpdatedFiles) * 90)
                                      /filesQueue._totalFiles);


                            string text = "Importing file: "
                                          + file.FileInformation.FullName;
                        

                            if (!filesQueue._import.CheckProgress(percentage, text))
                            {
                                filesQueue._import.CancelUpdate = true;
                                return;
                            }


                        }



                        if (file.Item.ID == 0)
                        {

                            IMLItem item 
                                = filesQueue._import.Importer
                                    .Section.AddNewItem
                                    (file.Item.Name,
                                     file.Item.Location);


                            MLItem.CopyItem
                                (file.Item, item);


                            item.SaveTags();
                            
                            item.Tags["meePopupProvider"]
                                = item.ID.ToString
                                    (CultureInfo.InvariantCulture);
                            
                            item.SaveTags();

                            filesQueue._import.AddedFiles++;


                        }
                        else
                        {

                            MLItem.CleanupItem(file.Item);

                            file.Item.Tags["meePopupProvider"] 
                                = file.Item.ID.ToString
                                    (CultureInfo.InvariantCulture);

                            file.Item.SaveTags();
                            filesQueue._import.UpdatedFiles++;
                        }



                        file.Dispose();
                    }
                    else
                    {
                        filesQueue._waitHandle.WaitOne(); // No more files - wait for a signal
                    }



                }
                catch (Exception ex)
                {
                    string logMessage = "Error in FileQueue worker." + Environment.NewLine + ex;
                    filesQueue._import.Importer.LogMessages.Enqueue(new LogMessage("Error", logMessage));
                }



            }



        }
    }
}
