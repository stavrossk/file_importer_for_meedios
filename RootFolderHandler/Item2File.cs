namespace FileImporter.RootFolderHandler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Remoting.Contexts;

    using MCW.Utility.MeediOS;

    using MeediOS;

    [Synchronization]
    internal class Item2File : ContextBoundObject
    {
        #region Constants and Fields

        private readonly Dictionary<IMLItem, EnhancedFileInfo> files;

        #endregion

        #region Constructors and Destructors

        public Item2File()
        {
            this.files = new Dictionary<IMLItem, EnhancedFileInfo>();
        }

        #endregion

        #region Properties

        public bool RetrivedItemWasCached { get; set; }

        #endregion

        #region Public Methods

        public void Add(EnhancedFileInfo file)
        {
            this.files.Add(file.Item, file);
        }

        public EnhancedFileInfo Query(IMLItem item)
        {
            if (this.files.Keys.Contains(item))
            {
                this.RetrivedItemWasCached = true;
                return this.files[item];
            }
            else
            {
                MLTag location = new MLTag("Location", item.Location);
                EnhancedFileInfo file = new EnhancedFileInfo(new FileInfo(location.Values[0])) { Item = item };
                this.Add(file);
                this.RetrivedItemWasCached = false;
                return file;
            }
        }

        #endregion
    }
}