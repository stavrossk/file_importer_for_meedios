﻿using System;
using CustomProperties.PropertyData;




namespace FileImporter.ImporterProperties
{



    class FileSystemInfoPropertiesGetter
    {







        internal static bool GetPropertyForFilesystemInfo
            (int index, ref int i, OptionSettings prop)
        {




            if (index == i++)
            {
                prop.Name = "FilesystemInfoToggle1";
                prop.Caption = String.Empty;
                prop.DefaultValue = "Don't import any filesystem info".Translate();
                prop.HelpText = "Click to delete all tagnames in the 'Filesystem Info' group." + Environment.NewLine +
                                "This way these tags don't get imported.";
                prop.GroupCaption = "Filesystem Info:";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "Don't import any filesystem info".Translate() +
                                    ":::CreationDateTagName,LastAccessDateTagName,LastModificationDateTagName,FileSizeTagName,SerialNumberTagName,VolumeNameTagName = ";
                prop.DataType = "toggle";
                return true;
            }




            if (index == i++)
            {
                prop.Name = "FilesystemInfoToggle2";
                prop.Caption = String.Empty;
                prop.DefaultValue = "Reset tagnames".Translate();
                prop.HelpText =
                    "Click to set all the tagnames in the 'Filesystem Info' group to their defaultvalues." +
                    Environment.NewLine + "This way all 'filesystem info' will be saved in the default tags.";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "Reset tagnames".Translate() +
                                    ":::DEFAULT CreationDateTagName,LastAccessDateTagName,LastModificationDateTagName,FileSizeTagName,SerialNumberTagName,VolumeNameTagName";
                prop.DataType = "toggle";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "CreationDateTagName";
                prop.Caption = "Creation date tag name";
                prop.HelpText = "This is the name for the tag the datetime the file was created will be saved to." +
                                Environment.NewLine + "Leave empty if you don't want to import the creation date.";
                prop.DataType = "string";
                prop.DefaultValue = "File-Created";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "default:::CreationDateTagName = " + prop.DefaultValue;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "LastAccessDateTagName";
                prop.Caption = "Last Access tag name";
                prop.HelpText =
                    "This is the name for the tag the datetime the file was last accessed will be saved to." +
                    Environment.NewLine +
                    "Leave empty if you don't want to import the datetime the file was last accessed.";
                prop.DataType = "string";
                prop.DefaultValue = "File-LastAccess";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "default:::LastAccessDateTagName = " + prop.DefaultValue;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "LastModificationDateTagName";
                prop.Caption = "Last modification tag name";
                prop.HelpText =
                    "This is the name for the tag the datetime of the files last modification will be saved to." +
                    Environment.NewLine +
                    "Leave empty if you don't want to import the datetime the file was last modified.";
                prop.DataType = "string";
                prop.DefaultValue = "File-LastModification";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "default:::LastModificationDateTagName = " + prop.DefaultValue;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "FileSizeTagName";
                prop.Caption = "Filesize tag name";
                prop.HelpText = "This is the name for the tag the size of the file will be saved to." +
                                Environment.NewLine +
                                "Leave empty if you don't want to import the size of the file.";
                prop.DataType = "string";
                prop.DefaultValue = "File-Size";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "default:::FileSizeTagName = " + prop.DefaultValue;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "SerialNumberTagName";
                prop.Caption = "Serialnumber tag name";
                prop.HelpText =
                    "This is the name for the tag the serialnumber of the drive the file is saved to will be saved to." +
                    Environment.NewLine + "Leave empty if you don't want to import the serialnumber.";
                prop.DataType = "string";
                prop.DefaultValue = "File-SerialNumber";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "default:::SerialNumberTagName = " + prop.DefaultValue;
                return true;
            }

            /*
                    if(index == i++)
                    {
                        prop.Name           = "DiscIDTagName";
                        prop.Caption        = "DiscID tag name";
                        prop.GroupCaption   = "Optional info";
                        prop.HelpText       = "This is the name for the tag the discid will be saved to";
                        prop.DataType       = "string";
                        prop.DefaultValue   = "DiscID";
                        prop.GroupName      = "FilesystemInfo";
                        return true;
                    }

    */
            if (index == i++)
            {
                prop.Name = "VolumeNameTagName";
                prop.Caption = "Volumename tag name";
                prop.HelpText =
                    "This is the name for the tag the volumename of the drive the file is on will be saved to." +
                    Environment.NewLine + "Leave empty if you don't want to import the volumename.";
                prop.DataType = "string";
                prop.DefaultValue = "File-VolumeName";
                prop.GroupName = "FilesystemInfo";
                prop.Dependencies = "default:::VolumeNameTagName = " + prop.DefaultValue;
                return true;
            }

            return false;
        }
    }
}
