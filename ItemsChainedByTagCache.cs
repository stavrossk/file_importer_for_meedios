namespace FileImporter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using MCW.Utility.MeediOS;

    using MeediOS;

    internal class ItemsChainedByTagCache
    {
        #region Constants and Fields

        private readonly Dictionary<string, IMLItem> cache;

        private readonly TheImport import;

        #endregion

        #region Constructors

        public ItemsChainedByTagCache(TheImport import)
        {
            this.import = import;
            this.cache = new Dictionary<string, IMLItem>();
        }

        #endregion

        #region Methods

        internal string ValuesOfChainingTags(IMLItem item)
        {
            MLItem tmpItem = new MLItem(null);
            tmpItem.CopyFrom(item);
            tmpItem.AddBaseTagsToTags();
            object tagValuesObject = tmpItem.Tags[this.import.Importer.TagsToChainBy[0]];
            string tagValues = (tagValuesObject == DBNull.Value) ? string.Empty : (string)tagValuesObject;
            foreach (string tagName in this.import.Importer.TagsToChainBy.Skip(1))
            {
                tagValues += "<**-**>" + tmpItem.Tags[tagName];
            }

            return tagValues;
        }

        internal IMLItem Query(IMLItem item)
        {
            string tagValues = this.ValuesOfChainingTags(item);
            if (this.cache.Keys.Contains(tagValues))
            {
                return this.cache[tagValues];
            }
            else
            {
                this.cache.Add(tagValues, item);
                return item;
            }
        }

        #endregion
    }
}