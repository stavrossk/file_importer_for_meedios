﻿using System.Globalization;
using System.Text;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;



namespace FileImporter.Debugger
{



    class LogConfigCreator
    {






        internal static void CreateLogConfig
            (CORE.FileImporter fileImporter,
            string logConfigFilename)
        {



            using (var logConfigWriter 
                = new LogConfigWriter
                (
                FolderLocations.Logs 
                +
                "FileImporter_" 
                +
                fileImporter.Section.Name 
                +
                "_" 
                + 
                fileImporter.ImportFunc
                .StartTime.ToString
                (CultureInfo.InvariantCulture)
                .Replace(':', ';') 
                +
                ".log",
                logConfigFilename,
                Encoding.UTF8))
            {
                logConfigWriter.WriteStartElement("log4net");
                
                #if TRACE
                if (IsAttached)
                {
                    logConfigWriter.AddTraceAppender();
                }
                #endif



                if (fileImporter.DebugLevel != "Off")
                {
                    logConfigWriter.AddFileAppender();
                }

                logConfigWriter.WriteStartElement("root");
                logConfigWriter.WriteStartElement("level");
                logConfigWriter.WriteAttributeString("value", fileImporter.DebugLevel.ToUpper());
                logConfigWriter.WriteEndElement();


                #if TRACE
                if (IsAttached)
                {
                    logConfigWriter.WriteStartElement("appender-ref");
                    logConfigWriter.WriteAttributeString("ref", "Trace");
                    logConfigWriter.WriteEndElement();
                }
                #endif



                if (fileImporter.DebugLevel != "Off")
                {
                    logConfigWriter.WriteStartElement("appender-ref");
                    logConfigWriter.WriteAttributeString("ref", "File");
                    logConfigWriter.WriteEndElement();
                }

                logConfigWriter.WriteEndElement();
                logConfigWriter.WriteEndElement();
            }
        }
    }
}
