﻿namespace FileImporter.TagValuesReplacement
{
    using System.Windows.Forms;

    using XPExplorerBar;

    internal partial class ReplacementRuleUI : FlowLayoutPanel
    {
        #region Constants and Fields

        private readonly Expando owner;

        private readonly ReplacementRule rule;

        #endregion

        #region Constructors and Destructors

        public ReplacementRuleUI(ReplacementRule rule, string[] tagNames, Expando owner)
        {
            this.rule = rule;
            this.owner = owner;
            this.FlowDirection = FlowDirection.LeftToRight;
            this.MouseDown += this.RuleUIMouseDown;
            this.InitializeComponent();

            this.tagName2.Items.Add("<>");
            this.tagName3.Items.Add("<>");
            this.tagName1.Items.AddRange(tagNames);
            this.tagName2.Items.AddRange(tagNames);
            this.tagName3.Items.AddRange(tagNames);
            this.tagName1.DataBindings.Add("Text", this.rule, "TagName1");
            this.tagName2.DataBindings.Add("Text", this.rule, "TagName2");
            this.tagName3.DataBindings.Add("Text", this.rule, "TagName3");
            this.tagValue1.DataBindings.Add("Text", this.rule, "TagValue1");
            this.tagValue2.DataBindings.Add("Text", this.rule, "TagValue2");
            this.tagValue3.DataBindings.Add("Text", this.rule, "TagValue3");
        }

        #endregion

        #region Properties

        internal Expando Owner
        {
            get
            {
                return this.owner;
            }
        }

        #endregion

        #region Methods

        private void RuleUIMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.DoDragDrop(sender as ReplacementRuleUI, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        #endregion
    }
}