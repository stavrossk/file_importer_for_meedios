﻿namespace FileImporter.TagValuesReplacement
{
    using System;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    using Padding = System.Windows.Forms.Padding;

    using XPExplorerBar;

    public partial class RulesSetupForm : Form
    {
        #region Constants and Fields

        private readonly ReplacementRules rules;

        private readonly string[] tagNames;

        private string[] ruleValues;

        #endregion

        #region Constructors and Destructors

        public RulesSetupForm(string[] rules, string[] tagNames)
        {
            this.tagNames = tagNames;
            this.rules = new ReplacementRules(rules);
            this.InitializeComponent();

            this.taskPane1.CustomSettings.GradientStartColor = Color.FromArgb(255, 123, 162, 231);
            this.taskPane1.CustomSettings.GradientEndColor = Color.FromArgb(255, 99, 117, 214);
            this.taskPane1.CustomSettings.Padding = new XPExplorerBar.Padding(12, 12, 12, 12);

            this.CreateRulesUI();
        }

        #endregion

        #region Properties

        public string[] TagNames
        {
            get
            {
                return this.tagNames;
            }
        }

        internal string[] RuleValues
        {
            get
            {
                return this.ruleValues;
            }

            set
            {
                this.ruleValues = value;
            }
        }

        #endregion

        #region Methods

        private void AddGroup()
        {
            this.rules.RuleGroups.Add(new ReplacementRulesGroup());
            this.rules.RuleGroups.Last().Rules.Add(ReplacementRule.DefaultRule());
            this.RefreshRulesUI();
        }

        private void AddGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.AddGroup();
        }

        private void AddRule(ReplacementRulesGroup rulesGroup)
        {
            rulesGroup.Rules.Add(ReplacementRule.DefaultRule());
            this.RefreshRulesUI();
        }

        private void AddRuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Expando groupPanel = (Expando)((ContextMenuStrip)((ToolStripItem)sender).Owner).SourceControl;
            int groupIndex = this.taskPane1.Expandos.IndexOf(groupPanel);
            ReplacementRulesGroup rulesGroup = this.rules.RuleGroups[groupIndex];
            this.AddRule(rulesGroup);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.RuleValues = this.rules.Serialize();
        }

        private void CreateRulesUI()
        {
            Expando groupPanel;
            foreach (ReplacementRulesGroup group in this.rules.RuleGroups)
            {
                groupPanel = new Expando();

                groupPanel.CustomSettings.NormalBackColor = Color.FromArgb(255, 214, 223, 247);
                groupPanel.CustomSettings.NormalBorder.Bottom = 1;
                groupPanel.CustomSettings.NormalBorder.Left = 1;
                groupPanel.CustomSettings.NormalBorder.Right = 1;
                groupPanel.CustomSettings.NormalBorder.Top = 0;
                groupPanel.CustomSettings.NormalBorderColor = Color.White;
                groupPanel.CustomSettings.NormalPadding = new XPExplorerBar.Padding(0, 0, 0, 0);

                groupPanel.CustomSettings.SpecialBackColor = Color.FromArgb(255, 239, 243, 255);
                groupPanel.CustomSettings.SpecialBorder.Bottom = 1;
                groupPanel.CustomSettings.SpecialBorder.Left = 1;
                groupPanel.CustomSettings.SpecialBorder.Right = 1;
                groupPanel.CustomSettings.SpecialBorder.Top = 0;
                groupPanel.CustomSettings.SpecialBorderColor = Color.White;
                groupPanel.CustomSettings.SpecialPadding = new XPExplorerBar.Padding(12, 10, 12, 10);
                groupPanel.Padding.Left = 0;
                groupPanel.Padding.Top = 0;
                groupPanel.Padding.Right = 0;
                groupPanel.Padding.Bottom = 0;
                groupPanel.Margin = new Padding(0, 0, 0, 0);

                groupPanel.SystemSettings.TaskItem.FontDecoration = FontStyle.Underline;
                groupPanel.SystemSettings.TaskItem.HotLinkColor = Color.FromArgb(255, 66, 142, 255);
                groupPanel.SystemSettings.TaskItem.LinkColor = Color.FromArgb(255, 33, 93, 198);
                groupPanel.SystemSettings.TaskItem.Margin = new Margin(0, 0, 0, 0);
                groupPanel.SystemSettings.TaskItem.Padding = new XPExplorerBar.Padding(6, 0, 4, 0);

                groupPanel.CustomHeaderSettings.GradientOffset = (float)0.5;
                groupPanel.CustomHeaderSettings.Margin = 15;
                groupPanel.CustomHeaderSettings.NormalAlignment = ContentAlignment.MiddleLeft;
                groupPanel.CustomHeaderSettings.NormalBackColor = Color.Transparent;
                groupPanel.CustomHeaderSettings.NormalBorder.Bottom = 0;
                groupPanel.CustomHeaderSettings.NormalBorder.Top = 2;
                groupPanel.CustomHeaderSettings.NormalBorder.Left = 2;
                groupPanel.CustomHeaderSettings.NormalBorder.Right = 2;
                groupPanel.CustomHeaderSettings.NormalBorderColor = Color.FromArgb(255, 198, 211, 247);
                groupPanel.CustomHeaderSettings.NormalGradientEndColor = Color.FromArgb(255, 49, 106, 197);
                groupPanel.CustomHeaderSettings.NormalGradientStartColor = Color.White;
                groupPanel.CustomHeaderSettings.NormalPadding = new XPExplorerBar.Padding(10, 0, 1, 0);
                groupPanel.CustomHeaderSettings.NormalTitleColor = Color.FromArgb(255, 33, 93, 198);
                groupPanel.CustomHeaderSettings.NormalTitleHotColor = Color.FromArgb(255, 66, 142, 255);

                groupPanel.CustomHeaderSettings.SpecialBackColor = Color.Transparent;
                groupPanel.CustomHeaderSettings.SpecialBorder.Bottom = 0;
                groupPanel.CustomHeaderSettings.SpecialBorder.Top = 2;
                groupPanel.CustomHeaderSettings.SpecialBorder.Left = 2;
                groupPanel.CustomHeaderSettings.SpecialBorder.Right = 2;
                groupPanel.CustomHeaderSettings.SpecialGradientEndColor = Color.FromArgb(255, 49, 106, 197);
                groupPanel.CustomHeaderSettings.SpecialGradientStartColor = Color.White;
                groupPanel.CustomHeaderSettings.SpecialPadding = new XPExplorerBar.Padding(10, 0, 0, 0);
                groupPanel.CustomHeaderSettings.SpecialTitleColor = Color.White;
                groupPanel.CustomHeaderSettings.SpecialTitleHotColor = Color.FromArgb(255, 66, 142, 255);
                groupPanel.CustomHeaderSettings.TitleFont = new Font("Tahoma", 8);
                groupPanel.CustomHeaderSettings.TitleRadius = 5;
                groupPanel.CustomHeaderSettings.TitleGradient = true;

                groupPanel.AutoLayout = true;
                groupPanel.ContextMenuStrip = this.groupPanelContextMenu;
                groupPanel.AllowDrop = true;
                
                groupPanel.DragEnter += this.GroupPanel_DragEnter;
                groupPanel.DragDrop += this.GroupPanel_DragDrop;
                ReplacementRuleUI rulePanel;
                for (int rulePos = 0; rulePos < group.Rules.Count; rulePos++)
                {
                    rulePanel = new ReplacementRuleUI(group.Rules[rulePos], this.TagNames, groupPanel);
                    Button deleteRule = new Button
                        {
                            Location = new Point(763, 3),
                            Name = "deleteRule",
                            Size = new Size(80, 20),
                            AutoSizeMode = AutoSizeMode.GrowOnly,
                            AutoSize = true,
                            TabIndex = 6,
                            Text = "Delete rule"
                        };
                    deleteRule.Click += this.DeleteRule_Click;
                    rulePanel.Width = rulePanel.Width + deleteRule.Bounds.Width + rulePanel.Margin.Horizontal;
                    rulePanel.Controls.Add(deleteRule);
                    groupPanel.Items.Add(rulePanel);
                }

                this.taskPane1.Expandos.Add(groupPanel);
            }
        }

        private void DeleteGroup(ReplacementRulesGroup rulesGroup)
        {
            this.rules.RuleGroups.Remove(rulesGroup);
            this.RefreshRulesUI();
        }

        private void DeleteGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Expando groupPanel = (Expando)((ContextMenuStrip)((ToolStripItem)sender).Owner).SourceControl;
            ReplacementRulesGroup rulesGroup = this.GetRulesGroupFromGroupPanel(groupPanel);
            this.DeleteGroup(rulesGroup);
        }

        private ReplacementRulesGroup GetRulesGroupFromGroupPanel(Expando groupPanel)
        {
            int groupIndex = this.taskPane1.Expandos.IndexOf(groupPanel);
            return this.rules.RuleGroups[groupIndex];
        }

        private void DeleteRule(ReplacementRulesGroup replacementRulesGroup, ReplacementRule replacementRule)
        {
            replacementRulesGroup.Rules.Remove(replacementRule);
            if (replacementRulesGroup.Rules.Count == 0)
            {
                this.rules.RuleGroups.Remove(replacementRulesGroup);
            }

            this.RefreshRulesUI();
        }

        private void DeleteRule_Click(object sender, EventArgs e)
        {
            ReplacementRuleUI ruleUI = (ReplacementRuleUI)((Button)sender).Parent;
            ReplacementRulesGroup rulesGroup = this.GetRulesGroupFromGroupPanel(ruleUI.Owner);
            ReplacementRule rule = this.GetRuleFromRuleUI(ruleUI, rulesGroup);
            this.DeleteRule(rulesGroup, rule);
        }

        private ReplacementRule GetRuleFromRuleUI(ReplacementRuleUI ruleUI, ReplacementRulesGroup rulesGroup)
        {
            int ruleIndex = ruleUI.Owner.Items.IndexOf(ruleUI);
            return rulesGroup.Rules[ruleIndex];
        }

        private void GroupPanel_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Effect == DragDropEffects.None)
            {
                return;
            }

            Expando destinationGroupPanel = (Expando)sender;
            ReplacementRulesGroup destinationGroup = this.GetRulesGroupFromGroupPanel(destinationGroupPanel);
            ReplacementRuleUI ruleUI = (ReplacementRuleUI)e.Data.GetData(typeof(ReplacementRuleUI));
            ReplacementRulesGroup sourceRulesGroup = this.GetRulesGroupFromGroupPanel(ruleUI.Owner);
            ReplacementRule sourceRule = this.GetRuleFromRuleUI(ruleUI, sourceRulesGroup);
            Point clientCoordinates = destinationGroupPanel.PointToClient(new Point(e.X, e.Y));
            Control controlAtDropPoint = destinationGroupPanel.GetChildAtPoint(clientCoordinates);
            int newIndex = controlAtDropPoint == null ? 0 : destinationGroupPanel.Items.IndexOf(controlAtDropPoint) + 1;
            if (e.Effect == DragDropEffects.Move)
            {
                this.DeleteRule(sourceRulesGroup, sourceRule);
            }

            if (e.Effect == DragDropEffects.Copy || e.Effect == DragDropEffects.Move)
            {
                destinationGroup.Rules.Insert(newIndex, new ReplacementRule(sourceRule.Serialize()));
                this.RefreshRulesUI();
            }
        }

        private void GroupPanel_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ReplacementRuleUI)))
            {
                if ((e.KeyState & 4) == 4)
                {
                    e.Effect = DragDropEffects.Move;
                }
                else
                {
                    e.Effect = DragDropEffects.Copy;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void RefreshRulesUI()
        {
            this.taskPane1.Expandos.Clear();
            this.CreateRulesUI();
        }

        #endregion
    }
}