﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CustomProperties;
using FileImporter.CORE;
using FileImporter.TagValuesReplacement;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;
using MeediOS;





namespace FileImporter.ImporterProperties
{





    class PluginPropertiesSetter
    {






        internal static void SetPluginProperties
            (CORE.FileImporter fileImporter, IMeedioItem properties)
        {



            if (properties["ImporterSettings"] != null)
            {
                CustomProperty.SetProperties(properties, "ImporterSettings");
            }

            if ((properties["RootFolders2"] != null) && !(properties["RootFolders2"] is DBNull))
            {
                fileImporter.RootFolders = (string[]) properties["RootFolders2"];
                fileImporter.LogMessages.Enqueue(
                    new LogMessage("debug", "RootFolders: " + String.Join(Environment.NewLine, fileImporter.RootFolders)));
            }

            if ((properties["RootFolders"] != null) && !(properties["RootFolders"] is DBNull))
            {
                if (fileImporter.RootFolders == null)
                {
                    fileImporter.RootFolders = (string[]) properties["RootFolders"];
                    for (int rootFolderPos = 0; rootFolderPos < fileImporter.RootFolders.Length; rootFolderPos++)
                    {
                        fileImporter.RootFolders[rootFolderPos] = fileImporter.RootFolders[rootFolderPos].ToLowerInvariant();
                    }

                    fileImporter.LogMessages.Enqueue(
                        new LogMessage(
                            "debug", "RootFolders: " + String.Join(Environment.NewLine, fileImporter.RootFolders)));
                }
                else
                {
                    List<string> rootFoldersList = new List<string>(fileImporter.RootFolders);
                    rootFoldersList.AddRange((string[]) properties["RootFolders"]);
                    fileImporter.RootFolders =
                        new string[rootFoldersList.Select(rf => rf.ToLowerInvariant()).Distinct().Count()];
                    rootFoldersList.Select(rf => rf.ToLowerInvariant()).Distinct().ToArray().CopyTo(
                        fileImporter.RootFolders, 0);
                    fileImporter.LogMessages.Enqueue(
                        new LogMessage(
                            "debug", "RootFolders: " + String.Join(Environment.NewLine, fileImporter.RootFolders)));
                }
            }

            fileImporter.TagMasks = new List<TagMask>();
            if ((properties["TagMasks"] != null) && !(properties["TagMasks"] is DBNull))
            {
                fileImporter.TagMasksStrings = (string[]) properties["TagMasks"];
                fileImporter.LogMessages.Enqueue(new LogMessage("debug", "TagMasks:"));
                foreach (string tagMask in fileImporter.TagMasksStrings)
                {
                    fileImporter.TagMasks.Add(new TagMask(tagMask ?? String.Empty));
                    fileImporter.LogMessages.Enqueue(
                        new LogMessage("debug", "  Mask:  " + fileImporter.TagMasks[fileImporter.TagMasks.Count - 1].Mask));
                    fileImporter.LogMessages.Enqueue(
                        new LogMessage("debug", "  RegEx: " + fileImporter.TagMasks[fileImporter.TagMasks.Count - 1].RegExp));
                }
            }

            if ((properties["ChainFiles"] != null) && !(properties["ChainFiles"] is DBNull))
            {
                fileImporter.ChainFiles = (bool) properties["ChainFiles"];
            }

            if ((properties["ChainingOptions"] != null)
                && !(properties["ChainingOptions"] is DBNull))
            {
                fileImporter.ChainingOption = (ChainOption) Convert.ToInt32((string) properties["ChainingOptions"]);
            }

            if ((properties["ChainingTags"] != null)
                && !(properties["ChainingTags"] is DBNull))
            {
                fileImporter.TagsToChainBy = (string[]) properties["ChainingTags"];
            }

            if ((properties["DeleteNonExistentFiles"] != null)
                && !(properties["DeleteNonExistentFiles"] is DBNull))
            {
                fileImporter.DeleteNonExistentFiles = (bool) properties["DeleteNonExistentFiles"];
            }

            if ((properties["AlwaysUpdate"] != null)
                && !(properties["AlwaysUpdate"] is DBNull))
            {
                fileImporter.AlwaysUpdate = (bool) properties["AlwaysUpdate"];
            }

            fileImporter.TagValuesReplacementRules = new ReplacementRules(properties["TagValuesReplacement"] as string[]);

            if ((properties["IncludeFileMasks"] != null)
                && !(properties["IncludeFileMasks"] is DBNull))
            {
                fileImporter.IncludeFileMasks = (string) properties["IncludeFileMasks"];
            }

            if (String.IsNullOrEmpty(fileImporter.IncludeFileMasks))
            {
                fileImporter.IncludeFileMasks = "*.*";
            }

            if ((properties["ExcludeFileMasks"] != null)
                && !(properties["ExcludeFileMasks"] is DBNull))
            {
                fileImporter.ExcludeFileMasks = (string) properties["ExcludeFileMasks"];
            }

            if ((properties["IncludeHiddenFiles"] != null)
                && !(properties["IncludeHiddenFiles"] is DBNull))
            {
                fileImporter.IncludeHiddenFiles = (bool) properties["IncludeHiddenFiles"];
            }

            if ((properties["ExcludeUnmatchedFiles"] != null)
                && !(properties["ExcludeUnmatchedFiles"] is DBNull))
            {
                fileImporter.ExcludeUnmatchedFiles = (bool) properties["ExcludeUnmatchedFiles"];
            }

            if ((properties["MinFileSize"] != null)
                && !(properties["MinFileSize"] is DBNull))
            {
                fileImporter.MinFileSize = (int) properties["MinFileSize"];
            }

            fileImporter.ImageLinker.ImageSearches.Clear();


            var iso = new ImageSearchOptions();


            bool addThisImageSearch = false;


            if ((properties["ImageFileMasks"] != null)
                && !(properties["ImageFileMasks"] is DBNull))
            {
                var imageFileMasks
                    = (string) properties
                                   ["ImageFileMasks"];


                if (imageFileMasks.Contains(':'))
                {
                    iso.ImageFolderSearchPattern = imageFileMasks.Split(new[] {':'})[0];
                    iso.FileFolderSearchPattern = imageFileMasks.Split(new[] {':'})[1];
                }
                else
                {
                    iso.ImageFolderSearchPattern = imageFileMasks;
                    iso.FileFolderSearchPattern = imageFileMasks;
                }
            }

            if ((properties["ImageFolder"] != null) && !(properties["ImageFolder"] is DBNull))
            {
                iso.ImageFolder = (string) properties["ImageFolder"];
            }

            if ((properties["FileFolderImage"] != null) && !(properties["FileFolderImage"] is DBNull))
            {
                iso.SearchFileFolderOptions = (string) properties["FileFolderImage"];
                fileImporter.ImportImages = !String.IsNullOrEmpty(iso.ImageFolder) ||
                                            iso.SearchFileFolderOptions != "Never".Translate();
                addThisImageSearch = !String.IsNullOrEmpty(iso.ImageFolder) ||
                                     iso.SearchFileFolderOptions != "Never".Translate();
            }

            if ((properties["ImageCount"] != null) && !(properties["ImageCount"] is DBNull))
            {
                iso.MaxImages = (int) properties["ImageCount"];
            }

            if ((properties["ImageTagName"] != null) && !(properties["ImageTagName"] is DBNull))
            {
                iso.ImageTagName = (string) properties["ImageTagName"];
            }

            if ((properties["AlwaysUpdateImages"] != null) && !(properties["AlwaysUpdateImages"] is DBNull))
            {
                fileImporter.alwaysUpdateImages = (bool) properties["AlwaysUpdateImages"];
            }

            if ((properties["SaveInImageBaseTag"] != null) && !(properties["SaveInImageBaseTag"] is DBNull))
            {
                iso.SaveInImageBaseTag = (bool) properties["SaveInImageBaseTag"];
            }

            if (addThisImageSearch)
            {
                fileImporter.ImageLinker.ImageSearches.Add(iso);
            }

            bool doNextImageSearch = false;
            if ((properties["ImageSearchActive2"] != null) && !(properties["ImageSearchActive2"] is DBNull))
            {
                doNextImageSearch = (bool) properties["ImageSearchActive2"];
            }

            if (doNextImageSearch)
            {
                iso = new ImageSearchOptions();
                if ((properties["ImageFileMasks2"] != null) && !(properties["ImageFileMasks2"] is DBNull))
                {
                    string imageFileMasks = (string) properties["ImageFileMasks2"];
                    if (imageFileMasks.Contains(':'))
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks.Split(new[] {':'})[0];
                        iso.FileFolderSearchPattern = imageFileMasks.Split(new[] {':'})[1];
                    }
                    else
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks;
                        iso.FileFolderSearchPattern = imageFileMasks;
                    }
                }

                if ((properties["ImageFolder2"] != null) && !(properties["ImageFolder2"] is DBNull))
                {
                    iso.ImageFolder = (string) properties["ImageFolder2"];
                }

                if ((properties["FileFolderImage2"] != null) && !(properties["FileFolderImage2"] is DBNull))
                {
                    iso.SearchFileFolderOptions = (string) properties["FileFolderImage2"];
                    addThisImageSearch = !String.IsNullOrEmpty(iso.ImageFolder) ||
                                         iso.SearchFileFolderOptions != "Never".Translate();
                    fileImporter.ImportImages = fileImporter.ImportImages || addThisImageSearch;
                }

                if ((properties["ImageCount2"] != null) && !(properties["ImageCount2"] is DBNull))
                {
                    iso.MaxImages = (int) properties["ImageCount2"];
                }

                if ((properties["ImageTagName2"] != null) && !(properties["ImageTagName2"] is DBNull))
                {
                    iso.ImageTagName = (string) properties["ImageTagName2"];
                }

                if ((properties["SaveInImageBaseTag2"] != null) && !(properties["SaveInImageBaseTag2"] is DBNull))
                {
                    iso.SaveInImageBaseTag = (bool) properties["SaveInImageBaseTag2"];
                }

                if (addThisImageSearch)
                {
                    fileImporter.ImageLinker.ImageSearches.Add(iso);
                }
            }

            if ((properties["ImageSearchActive3"] != null) && !(properties["ImageSearchActive3"] is DBNull))
            {
                doNextImageSearch = (bool) properties["ImageSearchActive3"];
            }

            if (doNextImageSearch)
            {
                iso = new ImageSearchOptions();
                if ((properties["ImageFileMasks3"] != null) && !(properties["ImageFileMasks3"] is DBNull))
                {
                    string imageFileMasks = (string) properties["ImageFileMasks3"];
                    if (imageFileMasks.Contains(':'))
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks.Split(new[] {':'})[0];
                        iso.FileFolderSearchPattern = imageFileMasks.Split(new[] {':'})[1];
                    }
                    else
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks;
                        iso.FileFolderSearchPattern = imageFileMasks;
                    }
                }

                if ((properties["ImageFolder3"] != null) && !(properties["ImageFolder3"] is DBNull))
                {
                    iso.ImageFolder = (string) properties["ImageFolder3"];
                }

                if ((properties["FileFolderImage3"] != null) && !(properties["FileFolderImage3"] is DBNull))
                {
                    iso.SearchFileFolderOptions = (string) properties["FileFolderImage3"];
                    addThisImageSearch = fileImporter.ImportImages || !String.IsNullOrEmpty(iso.ImageFolder) ||
                                         iso.SearchFileFolderOptions != "Never".Translate();
                    fileImporter.ImportImages = fileImporter.ImportImages || addThisImageSearch;
                }

                if ((properties["ImageCount3"] != null) && !(properties["ImageCount3"] is DBNull))
                {
                    iso.MaxImages = (int) properties["ImageCount3"];
                }

                if ((properties["ImageTagName3"] != null) && !(properties["ImageTagName3"] is DBNull))
                {
                    iso.ImageTagName = (string) properties["ImageTagName3"];
                }

                if ((properties["SaveInImageBaseTag3"] != null) && !(properties["SaveInImageBaseTag3"] is DBNull))
                {
                    iso.SaveInImageBaseTag = (bool) properties["SaveInImageBaseTag3"];
                }

                if (addThisImageSearch)
                {
                    fileImporter.ImageLinker.ImageSearches.Add(iso);
                }
            }

            if ((properties["ImageSearchActive4"] != null) && !(properties["ImageSearchActive4"] is DBNull))
            {
                doNextImageSearch = (bool) properties["ImageSearchActive4"];
            }

            if (doNextImageSearch)
            {
                iso = new ImageSearchOptions();
                if ((properties["ImageFileMasks4"] != null) && !(properties["ImageFileMasks4"] is DBNull))
                {
                    string imageFileMasks = (string) properties["ImageFileMasks4"];
                    if (imageFileMasks.Contains(':'))
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks.Split(new[] {':'})[0];
                        iso.FileFolderSearchPattern = imageFileMasks.Split(new[] {':'})[1];
                    }
                    else
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks;
                        iso.FileFolderSearchPattern = imageFileMasks;
                    }
                }

                if ((properties["ImageFolder4"] != null) && !(properties["ImageFolder4"] is DBNull))
                {
                    iso.ImageFolder = (string) properties["ImageFolder4"];
                }

                if ((properties["FileFolderImage4"] != null) && !(properties["FileFolderImage4"] is DBNull))
                {
                    iso.SearchFileFolderOptions = (string) properties["FileFolderImage4"];
                    addThisImageSearch = fileImporter.ImportImages || !String.IsNullOrEmpty(iso.ImageFolder) ||
                                         iso.SearchFileFolderOptions != "Never".Translate();
                    fileImporter.ImportImages = fileImporter.ImportImages || addThisImageSearch;
                }

                if ((properties["ImageCount4"] != null) && !(properties["ImageCount4"] is DBNull))
                {
                    iso.MaxImages = (int) properties["ImageCount4"];
                }

                if ((properties["ImageTagName4"] != null) && !(properties["ImageTagName4"] is DBNull))
                {
                    iso.ImageTagName = (string) properties["ImageTagName4"];
                }

                if ((properties["SaveInImageBaseTag4"] != null) && !(properties["SaveInImageBaseTag4"] is DBNull))
                {
                    iso.SaveInImageBaseTag = (bool) properties["SaveInImageBaseTag4"];
                }

                if (addThisImageSearch)
                {
                    fileImporter.ImageLinker.ImageSearches.Add(iso);
                }
            }

            if ((properties["ImageSearchActive5"] != null) && !(properties["ImageSearchActive5"] is DBNull))
            {
                doNextImageSearch = (bool) properties["ImageSearchActive5"];
            }

            if (doNextImageSearch)
            {
                iso = new ImageSearchOptions();
                if ((properties["ImageFileMasks5"] != null) && !(properties["ImageFileMasks5"] is DBNull))
                {
                    string imageFileMasks = (string) properties["ImageFileMasks5"];
                    if (imageFileMasks.Contains(':'))
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks.Split(new[] {':'})[0];
                        iso.FileFolderSearchPattern = imageFileMasks.Split(new[] {':'})[1];
                    }
                    else
                    {
                        iso.ImageFolderSearchPattern = imageFileMasks;
                        iso.FileFolderSearchPattern = imageFileMasks;
                    }
                }

                if ((properties["ImageFolder5"] != null) && !(properties["ImageFolder5"] is DBNull))
                {
                    iso.ImageFolder = (string) properties["ImageFolder5"];
                }

                if ((properties["FileFolderImage5"] != null) && !(properties["FileFolderImage5"] is DBNull))
                {
                    iso.SearchFileFolderOptions = (string) properties["FileFolderImage5"];
                    addThisImageSearch = fileImporter.ImportImages || !String.IsNullOrEmpty(iso.ImageFolder) ||
                                         iso.SearchFileFolderOptions != "Never".Translate();
                    fileImporter.ImportImages = fileImporter.ImportImages || addThisImageSearch;
                }

                if ((properties["ImageCount5"] != null) && !(properties["ImageCount5"] is DBNull))
                {
                    iso.MaxImages = (int) properties["ImageCount5"];
                }

                if ((properties["ImageTagName5"] != null) && !(properties["ImageTagName5"] is DBNull))
                {
                    iso.ImageTagName = (string) properties["ImageTagName5"];
                }

                if ((properties["SaveInImageBaseTag5"] != null) && !(properties["SaveInImageBaseTag5"] is DBNull))
                {
                    iso.SaveInImageBaseTag = (bool) properties["SaveInImageBaseTag5"];
                }

                if (addThisImageSearch)
                {
                    fileImporter.ImageLinker.ImageSearches.Add(iso);
                }
            }

            /*
                if (properties["FollowShortcuts"] != null)
                {
                    FollowShortcuts = (Boolean)properties["FollowShortcuts"];
                }                
                */
            if ((properties["FileSizeTagName"] != null) && !(properties["FileSizeTagName"] is DBNull))
            {
                fileImporter.FileSizeTagName = (string) properties["FileSizeTagName"];
                fileImporter.FileSize = fileImporter.FileSizeTagName.Length > 0;
            }

            if ((properties["CreationDateTagName"] != null) && !(properties["CreationDateTagName"] is DBNull))
            {
                fileImporter.CreationDateTagName = (string) properties["CreationDateTagName"];
                fileImporter.CreationDate = fileImporter.CreationDateTagName.Length > 0;
            }

            if ((properties["LastModificationDateTagName"] != null) &&
                !(properties["LastModificationDateTagName"] is DBNull))
            {
                fileImporter.LastModificationDateTagName = (string) properties["LastModificationDateTagName"];
                fileImporter.LastModificationDate = fileImporter.LastModificationDateTagName.Length > 0;
            }

            if ((properties["LastAccessDateTagName"] != null) && !(properties["LastAccessDateTagName"] is DBNull))
            {
                fileImporter.LastAccessDateTagName = (string) properties["LastAccessDateTagName"];
                fileImporter.LastAccessDate = fileImporter.LastAccessDateTagName.Length > 0;
            }

            if ((properties["SerialNumberTagName"] != null) && !(properties["SerialNumberTagName"] is DBNull))
            {
                fileImporter.SerialNumberTagName = (string) properties["SerialNumberTagName"];
                fileImporter.SerialNumber = fileImporter.SerialNumberTagName.Length > 0;
            }

            /* 
                if (properties["DiscIDTagName"] != null)
                {
                    DiscIDTagName = (string)properties["DiscIDTagName"];
                }
                */
            if ((properties["VolumeNameTagName"] != null) && !(properties["VolumeNameTagName"] is DBNull))
            {
                fileImporter.VolumeNameTagName = (string) properties["VolumeNameTagName"];
                fileImporter.VolumeName = fileImporter.VolumeNameTagName.Length > 0;
            }

            if ((properties["SingleFilename"] != null) && !(properties["SingleFilename"] is DBNull))
            {
                fileImporter.SingleFilename = (string) properties["SingleFilename"];
            }

            if (!String.IsNullOrEmpty(fileImporter.SingleFilename))
            {
                fileImporter.RootFolders = new[] {Path.GetDirectoryName(fileImporter.SingleFilename)};
                fileImporter.IncludeFileMasks = String.Empty;
                fileImporter.IncludeFileMasks = Path.GetFileName(fileImporter.SingleFilename);
            }

            if ((properties["ImportFromADS"] != null) && !(properties["ImportFromADS"] is DBNull))
            {
                fileImporter.ImportFromAlternateDataStream = (bool) properties["ImportFromADS"];
            }

            if ((properties["ExportToADS"] != null) && !(properties["ExportToADS"] is DBNull))
            {
                fileImporter.ExportToAlternateDataStream = (bool) properties["ExportToADS"];
            }

            if ((properties["DebugLevel"] != null) && !(properties["DebugLevel"] is DBNull))
            {
                fileImporter.DebugLevel = (string) properties["DebugLevel"];
            }




        }



    }





}
