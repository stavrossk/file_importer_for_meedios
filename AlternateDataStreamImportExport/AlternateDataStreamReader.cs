namespace FileImporter.AlternateDataStreamImportExport
{
    using System;
    using System.IO;
    using System.Xml.Linq;

    using Trinet.Core.IO.Ntfs;



    internal class AlternateDataStreamReader
    {




        private readonly EnhancedFileInfo _file;

        private EnhancedFileInfo File
        {
            get
            {
                return _file;
            }
        }

        private XDocument Document { get; set; }

        private AlternateDataStreamInfo StreamInfo { get; set; }

        public AlternateDataStreamReader(EnhancedFileInfo file)
        {
            _file = file;
        }

        public void ReadItem()
        {
            if (!File.FileInformation.Exists)
            {
                return;
               
            }

            if (!GetStreamInfo())
                return;


            GetDocument();


            var xElement = Document.Root;

            if (xElement != null)
                foreach (XElement tagElement in xElement.Descendants("tag"))
                {
                    var xAttribute = tagElement.Attribute("name");

                    if (xAttribute != null)
                        File.Item.Tags[xAttribute.Value]
                            = tagElement.Value;

                }

        }






        private bool GetStreamInfo()
        {


            if (File.FileInformation
                .AlternateDataStreamExists("MeediOS.Item"))
            {


                try
                {
                    StreamInfo = File.FileInformation.GetAlternateDataStream(
                        "MeediOS_Item", FileMode.Open);

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }


            return false;
        }




        private void GetDocument()
        {
            using (TextReader reader = StreamInfo.OpenText())
            {
                Document = XDocument.Load(reader);
            }
        }








    }








}