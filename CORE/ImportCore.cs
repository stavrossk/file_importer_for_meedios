﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using FileImporter.Directory_Scanner;
using FileImporter.Importer_Properties;
using FileImporter.Root_Folders_Analyzer;
using MCW.Utility.Logging;





namespace FileImporter.CORE
{


    class ImportCore
    {




        internal static bool Run(TheImport theImport)
        {
            try
            {
                theImport.StartTime = DateTime.Now;

                CoreEngineHelpers.SetupLogging(theImport.Importer);

                ExistingItemScanner.ScanExistingItems(theImport);

                theImport.Importer
                    .LogMessages.Enqueue
                    (new LogMessage("Info",
                        "Importer version: 1.0.15.338"));


                PluginPropertiesLogger
                    .LogProperties(theImport);



                if (theImport.Importer
                    .DeleteNonExistentFiles)
                {

                    theImport.Importer
                        .LogMessages.Enqueue
                        (new LogMessage("Info",
                            "Delete non existent files"));


                    theImport.Importer
                        .Section.BeginUpdate();

                    NonExistentItemRemover
                        .DeleteNonExistentItems(theImport);


                    theImport.Importer
                        .Section.EndUpdate();


                    theImport.Importer
                        .LogMessages.Enqueue
                        (new LogMessage("Info", 
                            "Deleted non existent files."));



                }




                if (theImport.Importer.RootFolders != null)
                {

                    theImport
                        .RootFolders2MappedDrives();


                    theImport.Importer
                        .LogMessages.Enqueue
                        (new LogMessage("Info",
                            "Analyzing rootfolders by drives."));


                    RootFoldersAnalyzer.AnalyzeRootFoldersByDrives(theImport);


                }
                else
                {


                    theImport.Importer
                        .LogMessages.Enqueue(
                        new LogMessage("Warn", 
                            "You haven't specified " +
                            "a rootfolder for the file search!"));

                    theImport.Importer
                        .LogMessages.Enqueue
                        (new LogMessage("Warn", 
                            TheImport.RootfolderRequestMessage));


                    theImport.Importer
                        .ImportProgress.Progress
                        (100, TheImport
                        .RootfolderRequestMessage
                        .Translate());
                   
                    
                    return true;

                }



                if (!theImport.CheckProgress(10, "Scanning Rootfolders"))
                {
                    return true;
                }

                theImport.AsyncCalls = new List<AsyncResult>();
////                this.filesLock = new object();
                theImport.Importer.Section.BeginUpdate();



                using (theImport.Files
                    = new FilesQueue(theImport))
                {




                    foreach (KeyValuePair<EnhancedDriveInfo,
                        List<string>> driveRootFolder
                        in theImport.RootFoldersOfDrive)
                    {


                        string logMessage
                            = "Drive: " 
                            + driveRootFolder.Key.SerialNumber 
                            + " - " + driveRootFolder.Value.Select
                            (Path.GetPathRoot).Aggregate
                            ((chain, link) 
                                => chain + ", " + link);


                        theImport.Importer
                            .LogMessages.Enqueue
                            (new LogMessage
                                ("Info", logMessage));


                        theImport.Importer
                            .LogMessages.Enqueue
                            (new LogMessage
                                ("Info",
                                "Rootfolders on that drive: "));


                        foreach (string rootFolder in driveRootFolder.Value)
                        {
                            theImport.Importer
                                .LogMessages
                                .Enqueue(new LogMessage
                                    ("Info", "  " + rootFolder));
                        }



                        TheImport.ScanDriveDelegate
                            delegateScanDrive
                            =
                            (KeyValuePair<EnhancedDriveInfo,
                                List<string>> driveRootFolders,
                                ref FilesQueue filesQueue)
                            => 
                            DirectoryScanner.ScanDrive
                            (theImport, driveRootFolders,
                            ref filesQueue);





                        theImport.AsyncCalls.Add(
                            (AsyncResult)
                            delegateScanDrive
                            .BeginInvoke
                            (driveRootFolder,
                            ref theImport.Files,
                            theImport.Callback, null));



                    }




                    var waitHandles = new WaitHandle[theImport.RootFoldersOfDrive.Count];
                    AsyncResult[] asyncCallsArray = theImport.AsyncCalls.ToArray();
                    for (int asyncCallPos = 0; asyncCallPos < asyncCallsArray.Length; asyncCallPos++)
                    {
                        waitHandles[asyncCallPos] = asyncCallsArray[asyncCallPos].AsyncWaitHandle;
                    }

                    for (int waitHandlePos = 0; waitHandlePos < waitHandles.Length; waitHandlePos++)
                    {
                        waitHandles[waitHandlePos].WaitOne();
                        waitHandles[waitHandlePos].Close();
                        waitHandles[waitHandlePos] = null;
                    }

                    theImport.AsyncCalls.Clear();
                    if (theImport.Importer.ChainingOption == ChainOption.ChainByTags)
                    {
                        theImport.Files.StartDequeueing();
                    }





                }






                theImport.Duration = DateTime.Now.Subtract(theImport.StartTime);
                string text = "Import finished after " +
                              (theImport.Duration.Hours > 0 ? theImport.Duration.Hours + " hours  " : String.Empty) +
                              (theImport.Duration.Minutes > 0 ? theImport.Duration.Minutes + " minutes  " : String.Empty) +
                              (theImport.Duration.Seconds + " seconds  ") + "\nAdded files:     " + theImport.AddedFiles +
                              "\nUpdated files: " + theImport.UpdatedFiles +
                              ("\nDeleted files:  " + theImport.DeletedFiles).PadRight(128);
                if (!theImport.CheckProgress(100, text))
                {
                }
                else
                {



                    theImport.Importer.Section.EndUpdate();


                    theImport.Importer
                        .LogMessages.Enqueue
                        (new LogMessage("Info",
                            "Import finished"));


                    theImport.Importer
                        .LogMessages.Enqueue
                        (new LogMessage
                            ("Info", "Added:   "
                            + theImport.AddedFiles
                            + " files"));


                    theImport.Importer.LogMessages.Enqueue(
                        new LogMessage("Info", "Updated: " + theImport.UpdatedFiles + " files"));
                    theImport.Importer.LogMessages.Enqueue(
                        new LogMessage("Info", "Deleted: " + theImport.DeletedFiles + " files"));
                    if (theImport.Importer.ExcludeUnmatchedFiles)
                    {
                        theImport.Importer.LogMessages.Enqueue(
                            new LogMessage("Info", "Ignored (unmatched tags): " + theImport.UnmatchedFiles.Count + " files"));
                        foreach (EnhancedFileInfo file in theImport.UnmatchedFiles)
                        {
                            string logMessage = "Ignored (unmatched tags): " + file.FileInformation.FullName;
                            theImport.Importer.LogMessages.Enqueue(new LogMessage("Info", logMessage));
                        }
                    }

                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Info", "Duration: " + theImport.Duration));
                
                
                
                
                }
            }
            catch (Exception ex)
            {
                string logMessage = "Import cancelled because of an unhandled error." + Environment.NewLine + ex;
                theImport.Importer.LogMessages.Enqueue(new LogMessage("Error", logMessage));
                theImport.Importer.ImportProgress.Progress(100, logMessage);
                theImport.Importer.Section.CancelUpdate();
            }
            finally
            {
                foreach (NetworkDrive drive in theImport.TemporaryMappedDrives)
                {
                    drive.UnmapDrive();
                }
            }

            return true;
        }












    }




}
