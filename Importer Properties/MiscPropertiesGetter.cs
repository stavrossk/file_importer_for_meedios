﻿using System;
using CustomProperties.PropertyData;





namespace FileImporter.ImporterProperties
{



    class MiscPropertiesGetter
    {








        internal static bool GetPropertyForMisc
            (int index, ref int i, OptionSettings prop, 
            CORE.FileImporter fileImporter,
            ImporterPropertiesCore.FileImporterProperties importerProperties)
        {





            if (index == i++)
            {
                prop.Name = "Mode";
                prop.Caption = "Mode";


                prop.HelpText = "Expert mode shows all options " +
                                "except those dependent on other options." +
                                Environment.NewLine +
                                "(Ex: 'Chaining tags' option is only visible" +
                                " if you selected 'Group/chain items" +
                                " with matching tags' in the 'Chaining options'.)" +
                                Environment.NewLine +
                                "Simple-Hide mode hides a lot of the more advanced options." +
                                " Simple-Disable will instead disable the same advanced options.";



                prop.DataType = "string";



                prop.Choices = new[]
                    {
                        "Simple-Hide".Translate(),
                        "Simple-Disable".Translate(),
                        "Expert".Translate()
                    };


                prop.Dependencies = "Simple-Hide".Translate()
                                    + 
                                    ":::HIDEGROUP" 
                                    +
                                    " ImageAssociationRules,"
                                    +
                                    "FilesystemInfo,"
                                    +
                                    "Debugging," +
                                    "ADS"
                                    +
                                    ";;;"
                                    +
                                    "Simple-Disable".Translate()
                                    +
                                    ":::DISABLEGROUP"
                                    +
                                    " ImageAssociationRules,"
                                    +
                                    "FilesystemInfo,"
                                    +
                                    "Debugging,ADS"
                                    +
                                    ";;;" 
                                    +
                                    "Simple-Hide".Translate()
                                    +
                                    ":::HIDE" 
                                    +
                                    " ImageTagName," 
                                    +
                                    "DeleteNonExistentFiles,"
                                    +
                                    "AlwaysUpdate,"
                                    +
                                    "IncludeHiddenFiles," 
                                    +
                                    "ExcludeFileMasks,"
                                    +
                                    "MinFileSize,"
                                    +
                                    "ExcludeUnmatchedFiles,"
                                    +
                                    "ChainingOptions," 
                                    +
                                    "AlwaysUpdateImages," 
                                    +
                                    "TagValuesReplacement" 
                                    +
                                    ";;;" 
                                    +
                                    "Simple-Disable".Translate()
                                    +
                                    ":::DISABLE ImageTagName,"
                                    +
                                    "DeleteNonExistentFiles," 
                                    +
                                    "AlwaysUpdate," 
                                    +
                                    "IncludeHiddenFiles," 
                                    +
                                    "ExcludeFileMasks"
                                    +
                                    ",MinFileSize,"
                                    +
                                    "ExcludeUnmatchedFiles," 
                                    +
                                    "ChainingOptions,AlwaysUpdateImages,"
                                    +
                                    "TagValuesReplacement" 
                                    +
                                    ";;;"
                                    + 
                                    "Expert".Translate()
                                    +
                                    ":::ENABLEGROUP "
                                    +
                                    "ImageAssociationRules,"
                                    +
                                    "FilesystemInfo," 
                                    +
                                    "Debugging," 
                                    +
                                    "ADS" 
                                    +
                                    ";;;"
                                    +
                                    "Expert".Translate()
                                    +
                                    ":::SHOWGROUP " 
                                    +
                                    "ImageAssociationRules,"
                                    +
                                    "FilesystemInfo," 
                                    +
                                    "Debugging,"
                                    +
                                    "ADS"
                                    +
                                    ";;;" + "Expert".Translate()
                                    +
                                    ":::ENABLE " 
                                    +
                                    "ImageTagName," 
                                    +
                                    "DeleteNonExistentFiles,"
                                    +
                                    "AlwaysUpdate,"
                                    +
                                    "IncludeHiddenFiles," 
                                    +
                                    "ExcludeFileMasks," 
                                    +
                                    "MinFileSize," 
                                    +
                                    "ExcludeUnmatchedFiles," 
                                    +
                                    "ChainingOptions," 
                                    +
                                    "AlwaysUpdateImages," 
                                    +
                                    "TagValuesReplacement" 
                                    +
                                    ";;;"
                                    +
                                    "Expert".Translate() 
                                    +
                                    ":::Show "
                                    +
                                    "ImageTagName,"
                                    +
                                    "DeleteNonExistentFiles," 
                                    +
                                    "AlwaysUpdate," 
                                    +
                                    "IncludeHiddenFiles,"
                                    +
                                    "ExcludeFileMasks,"
                                    +
                                    "MinFileSize," 
                                    +
                                    "ExcludeUnmatchedFiles," 
                                    +
                                    "ChainingOptions,"
                                    +
                                    "AlwaysUpdateImages," 
                                    +
                                    "TagValuesReplacement";




                prop.GroupName = "Misc";
                prop.DefaultValue = "Simple-Hide".Translate();
                return true;
            }





            /*
                if (index == i++)
                {
                    prop.Name = "WizardToggle";
                    prop.Caption = string.Empty;
                    prop.DefaultValue = "Start the setup wizard".Translate();
                    prop.HelpText = "Click to start a wizard guiding you through the setup of this import";
                    prop.GroupName = "Misc";
                    prop.Dependencies = "Start the setup wizard".Translate() +
                                        ":::WizardStepper = 1|||SHOW WizardStepper";
                    prop.DataType = "toggle";
                    return true;
                }

                if (index == i++)
                {  
                    prop.Name = "WizardStepper";
                    prop.Caption = "Wizard test";
                    prop.HelpText = "help";
                    prop.Choices = new List<string> { "MediaType", "RootFolders2", "TagMasks" };
                    prop.DefaultValue = 0;
                    prop.DataType = "wizard";
                    return true;
                }
                */













            //if (index == i++)
            //{

            //    prop.Name = "MediaType";

            //    prop.Caption = "Media type";

            //    prop.Choices2["any"] = "Any";
            //    prop.Choices2["music"] = "Music";
            //    prop.Choices2["musicvideos"] = "Music videos";
            //    prop.Choices2["movies"] = "Movies";
            //    prop.Choices2["tvshows"] = "TVShows";
            //    prop.Choices2["text"] = "Text";
            //    prop.Choices2["games"] = "Games";
            //    prop.Choices2["images"] = "Images";


            //    prop.HelpText = "If set other properties " +
            //                    "will be preconfigured to reflect" +
            //                    " the chosen media type.";







            ////    fileImporter.MovieProfile = "default";
            ////    fileImporter.MusicProfile = "default";
            ////    fileImporter.PicturesProfile = "default";
            ////    fileImporter.TVShowsProfile = "default";










            //    //string movieFolders = string.Empty;

            //    //if (fileImporter.MovieFolders != null)
            //    //{

            //    //     movieFolders
            //    //        = string.Join
            //    //            (Environment.NewLine,
            //    //             fileImporter.MovieFolders);

            //    //}



            //    //string tvFolders = String.Empty;

            //    //if (fileImporter.TVShowsFolders != null)
            //    //{
                    
            //    //     tvFolders
            //    //         = string.Join
            //    //         (Environment.NewLine,
            //    //         fileImporter.TVShowsFolders);

            //    //}





            //    //string musicFolders = String.Empty;

            //    //if (fileImporter.MusicFolders != null)
            //    //{

            //    //     musicFolders
            //    //         = string.Join
            //    //         (Environment.NewLine,
            //    //         fileImporter.MusicFolders);
          
            //    //}



            //    //string photoFolders = String.Empty;

            //    //if (fileImporter.PicturesFolders != null)
            //    //{

            //    //    photoFolders
            //    //       = string.Join
            //    //           (Environment.NewLine,
            //    //            fileImporter.PicturesFolders);

            //    //}




            //    //string musicVideosFolders
            //    //    = importerProperties
            //    //    .FixMediaLocation
            //    //    ("ExistingMusicVideos");



                


            //    //string moviePosterFolder
            //    //= importerProperties
            //    //.FixMediaLocation
            //    //("ExistingMoviePosters");


            //    //string movieFanartFolder
            //    //= importerProperties.FixMediaLocation
            //    //("ExistingMovieFanArt");

            //    //string tvSeriesBannerFolder
            //    //    = importerProperties.FixMediaLocation
            //    //    ("ExistingTVSeriesBanners");

            //    //string tvSeriesPosterFolder
            //    //    = importerProperties.FixMediaLocation
            //    //    ("ExistingTVSeriesPosters");

            //    //string tvSeriesFanartFolder
            //    //    = importerProperties.FixMediaLocation
            //    //    ("ExistingTVFanArt");

            //    //string tvSeasonImageFolder
            //    //    = importerProperties.FixMediaLocation
            //    //    ("ExistingTVSeasonPosters");





            //    //prop.Dependencies = ".*:::UPDATE Mode" +
            //    //                    ";;;any:::IncludeFileMasks = " +
            //    //                    "|||SHOWGROUP ImageAssociationRules" +
            //    //                    "|||ImageSearchActive2 = false" +
            //    //                    "|||ImageSearchActive3 = false" +
            //    //                    "|||ImageSearchActive4 = false" +
            //    //                    "|||ImageSearchActive5 = false" +
            //    //                    ";;;music:::IncludeFileMasks = " 
            //    //                    + 
            //    //                    FileImporter
            //    //                    .FileImporterProperties
            //    //                    .AudioExtensions 
            //    //                    +
            //    //                    "|||RootFolders2 = " 
            //    //                    +
            //    //                    musicFolders 
            //    //                    +
            //    //                    "|||TagMasks = " 
            //    //                    + 
            //    //                    importerProperties.MusicRegex 
            //    //                    +
            //    //                    "|||SHOWGROUP ImageAssociationRules"
            //    //                    +
            //    //                    "|||ImageSearchActive2 = false"
            //    //                    +
            //    //                    "|||ImageSearchActive3 = false" 
            //    //                    +
            //    //                    "|||ImageSearchActive4 = false"
            //    //                    +
            //    //                    "|||ImageSearchActive5 = false" 
            //    //                    +
            //    //                    ";;;movies:::IncludeFileMasks = "
            //    //                    +
            //    //                    FileImporter.FileImporterProperties.VideoExtensions 
            //    //                    +
            //    //                    "|||RootFolders2 = "
            //    //                    +
            //    //                    movieFolders 
            //    //                    +
            //    //                    "|||TagMasks = " 
            //    //                    +
            //    //                    importerProperties.MoviesRegex 
            //    //                    +
            //    //                    "|||SHOWGROUP ImageAssociationRules" +
            //    //                    "|||ImageSearchActive2 = true" +
            //    //                    "|||ImageSearchActive3 = false" +
            //    //                    "|||ImageSearchActive4 = false" +
            //    //                    "|||ImageSearchActive5 = false" +
            //    //                    "|||ImageFolder = " + moviePosterFolder 
            //    //                    +
            //    //                    "|||ImageFolder2 = " + movieFanartFolder 
            //    //                    +
            //    //                    ";;;musicvideos:::IncludeFileMasks = " 
            //    //                    +
            //    //                    FileImporter.FileImporterProperties.VideoExtensions 
            //    //                    +
            //    //                    "|||RootFolders2 = " + musicVideosFolders +
            //    //                    "|||SHOWGROUP ImageAssociationRules" +
            //    //                    "|||ImageSearchActive2 = false" +
            //    //                    "|||ImageSearchActive3 = false" +
            //    //                    "|||ImageSearchActive4 = false" +
            //    //                    "|||ImageSearchActive5 = false" +
            //    //                    ";;;tvshows:::IncludeFileMasks = " + FileImporter.FileImporterProperties.VideoExtensions +
            //    //                    "|||RootFolders2 = " + tvFolders +
            //    //                    "|||TagMasks = " + importerProperties.TVShowsRegex +
            //    //                    "|||TagValuesReplacement = " + importerProperties.tvShowsReplacements +
            //    //                    "|||ENABLEGROUP ImageAssociationRules" +
            //    //                    "|||SHOWGROUP ImageAssociationRules" +
            //    //                    "|||ImageSearchActive2 = true" +
            //    //                    "|||ImageSearchActive3 = true" +
            //    //                    "|||ImageSearchActive4 = true" +
            //    //                    "|||ImageSearchActive5 = true" +
            //    //                    "|||ImageFolder2 = " 
            //    //                    +
            //    //                    tvSeriesFanartFolder
            //    //                    +
            //    //                    "|||ImageFolder3 = "
            //    //                    + 
            //    //                    tvSeasonImageFolder
            //    //                    +
            //    //                    "|||ImageFolder4 = "
            //    //                    + 
            //    //                    tvSeriesPosterFolder 
            //    //                    +
            //    //                    "|||ImageFolder5 = "
            //    //                    + 
            //    //                    tvSeriesBannerFolder
            //    //                    +
            //    //                    ";;;text:::IncludeFileMasks = " 
            //    //                    +
            //    //                    FileImporter.FileImporterProperties.TextExtensions
            //    //                    +
            //    //                    "|||SHOWGROUP ImageAssociationRules"
            //    //                    +
            //    //                    "|||ImageSearchActive2 = false"
            //    //                    +
            //    //                    "|||ImageSearchActive3 = false" +
            //    //                    "|||ImageSearchActive4 = false" +
            //    //                    "|||ImageSearchActive5 = false" +
            //    //                    ";;;images:::IncludeFileMasks = " 
            //    //                    +
            //    //                    FileImporter.FileImporterProperties.ImageExtensions +
            //    //                    "|||RootFolders2 = " + photoFolders +
            //    //                    "|||HIDEGROUP ImageAssociationRules" +
            //    //                    "|||ImageSearchActive2 = false" +
            //    //                    "|||ImageSearchActive3 = false" +
            //    //                    "|||ImageSearchActive4 = false" +
            //    //                    "|||ImageSearchActive5 = false" +
            //    //                    ";;;games:::IncludeFileMasks = " + "*.lnk" +
            //    //                    "|||SHOWGROUP ImageAssociationRules" +
            //    //                    "|||ImageSearchActive2 = false" +
            //    //                    "|||ImageSearchActive3 = false" +
            //    //                    "|||ImageSearchActive4 = false" +
            //    //                    "|||ImageSearchActive5 = false";





            //    prop.DataType = "string";
            //    const string defaultValue = "any";



            ////    if (fileImporter.IsFirstLoad)
            ////    {


            ////        string sectionName 
            ////            = fileImporter.Section
            ////            .Name.ToLowerInvariant();




            ////        List<string> musicLibraryNames 
            ////            = new List<string>
            ////                  {
            ////                      "music", 
            ////                      "music".Translate()
            ////                  };


            ////        List<string> moviesLibraryNames 
            ////            = new List<string>
            ////            {
            ////                "movies",
            ////                "movies".Translate()
            ////            };

            ////        List<string> musicvideosLibraryNames 
            ////            = new List<string>
            ////            {
            ////                "musicvideos",
            ////                "musicvideos".Translate()
            ////            };


            ////        List<string> tvshowsLibraryNames = new List<string>
            ////            {
            ////                "tv-shows",
            ////                "tv-shows".Translate(),
            ////                "tvshows",
            ////                "tvshows".Translate(),
            ////                "series",
            ////                "series".Translate(),
            ////                "tv series",
            ////                "tv series".Translate()
            ////            };



            ////        List<string> gamesLibraryNames 
            ////            = new List<string> { "games", 
            ////                "games".Translate() };



            ////        List<string> imagesLibraryNames = new List<string>
            ////            {
            ////                "photos",
            ////                "photos".Translate(),
            ////                "images",
            ////                "images".Translate(),
            ////                "pictures",
            ////                "pictures".Translate()
            ////            };



            ////        List<string> booksLibraryNames = new List<string>
            ////            {
            ////                "books",
            ////                "books".Translate(),
            ////                "text",
            ////                "text".Translate()
            ////            };



            ////        if (musicLibraryNames.Contains(sectionName))
            ////        {
            ////            defaultValue = "music";
            ////        }
            ////        else if (moviesLibraryNames.Contains(sectionName))
            ////        {
            ////            defaultValue = "movies";
            ////        }
            ////        else if (musicvideosLibraryNames.Contains(sectionName))
            ////        {
            ////            defaultValue = "musicvideos";
            ////        }
            ////        else if (tvshowsLibraryNames.Contains(sectionName))
            ////        {
            ////            defaultValue = "tvshows";
            ////        }
            ////        else if (booksLibraryNames.Contains(sectionName))
            ////        {
            ////            defaultValue = "text";
            ////        }
            ////        else if (gamesLibraryNames.Contains(sectionName))
            ////        {
            ////            defaultValue = "games";
            ////        }
            ////        else if (imagesLibraryNames.Contains(sectionName))
            ////        {
            ////            defaultValue = "images";
            ////        }
            ////    }






            //    prop.DefaultValue = defaultValue;
            //    return true;

            //}


















            if (index == i++)
            {
                prop.Name = "RootFolders2";
                prop.Caption = "Root folders";
                prop.HelpText = "Select any number of root folders from which to import files";
                prop.DataType = "folderlist";
                prop.Choices = new string[] { };
                return true;
            }




            if (index == i++)
            {
                prop.Name = "TagMasks";
                prop.Caption = "Tag masks";
                prop.HelpText = "Enter one or more tag masks such as <artist>\\<album>\\<track> - <name>.<>" +
                                Environment.NewLine +
                                "The 'name' tag is important because it will be used as the item's name, that is showed on the last view step." +
                                Environment.NewLine +
                                "So usually your tag masks should at least make sure to fill this tag.";
                prop.DataType = "stringlist";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ChainFiles";
                prop.Caption = "Group/chain files in each directory together";
                prop.HelpText =
                    "If set files in a folder will be added as one item with all the filenames separated by pipes";
                prop.Dependencies = "False:::ChainingOptions = 0" + ";;;True:::ChainingOptions = 1";
                prop.DataType = "bool";
                prop.DefaultValue = false;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ChainingOptions";
                prop.Caption = "Chaining options";
                prop.Choices2["0"] = "Don't group/chain anything".Translate();
                prop.Choices2["1"] = "Group/chain files in same folder".Translate();
                prop.Choices2["2"] = "Group/chain items with matching tags".Translate();
                prop.HelpText =
                    "If set files in a folder will be added as one item with all the filenames separated by pipes";
                prop.Dependencies = "..*:::DISABLE ChainFiles|||HIDE ChainFiles" + ";;;0:::HIDE ChainingTags" +
                                    ";;;1:::HIDE ChainingTags" + ";;;2:::SHOW ChainingTags";
                prop.DataType = "string";
                prop.DefaultValue = "0";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ChainingTags";
                prop.Caption = "Chaining tags";
                prop.HelpText = "The tags used for the \"Group/chain items with matching tags\" chaining option";
                prop.DataType = "stringlist";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "DeleteNonExistentFiles";
                prop.Caption = "Delete/Relocate non existent files";
                prop.HelpText =
                    "If set all files of already existing items will be checked. If a file isn't available anymore it is first searched for in other rootfolders." +
                    Environment.NewLine +
                    "If found the location just gets replaced with the new one. If not the location gets deleted. If all locations are deleted the item will get deleted." +
                    Environment.NewLine +
                    "To prevent deleting the items just because your NAS is currently offline, the items only get deleted if the rootfolder of the item is currently available." +
                    Environment.NewLine +
                    "(If you removed the rootfolder from the 'RootFolders' property the items folder is checked instead.)";
                prop.DataType = "bool";
                prop.DefaultValue = false;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "AlwaysUpdate";
                prop.Caption = "Update all items";
                prop.HelpText = "If set all found files will be updated.\r\n" +
                                "If not only files that were modified since the last import will be updated.";
                prop.DataType = "bool";
                prop.DefaultValue = false;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "TagValuesReplacement";
                prop.Caption = "Tag values replacement";
                prop.HelpText = "Use this option to relace tag values.\r\n" +
                                "Each line is one replacement rule.\r\n" + "Ex:\r\n" +
                                "<SeriesName>=Star Trek; The Next Generation (<-?->) <SeriesName>=Star Trek: The Next Generation\r\n" +
                                "<SeriesName>=Star Trek; Voyager (<-?->) <SeriesName>=Star Trek: Voyager\r\n" +
                                "\r\n" + "<resolution>=720p (<-?->) <IsHD>=true\r\n" +
                                "<resolution>=1080p (<-?->) <IsHD>=true (<-:->) <IsHD>=false";
                prop.DataType = "customlist";
                prop.DefaultValue = new string[] { };
                prop.CanTypeChoices = true;
                return true;
            }

            return false;
        }
    }




}
