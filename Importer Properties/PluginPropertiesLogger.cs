﻿using System;
using System.Globalization;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;




namespace FileImporter.Importer_Properties
{




    class PluginPropertiesLogger
    {






        internal static void LogProperties(TheImport theImport)
        {



            theImport.Importer.LogMessages.Enqueue
                (new LogMessage("Debug", 
                    "Import Settings:"
                    + Environment.NewLine));


            theImport.Importer.LogMessages.Enqueue
                (new LogMessage("Debug",
                    "Root folders: "));


            if (theImport.Importer.RootFolders != null)
            {
                foreach (string rootFolder in theImport.Importer.RootFolders)
                {
                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", rootFolder));
                }
            }
            else
            {
                theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "null"));
            }

            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Tag masks: "));
            if (theImport.Importer.TagMasksStrings != null)
            {
                foreach (TagMask tagMask in theImport.Importer.TagMasks)
                {
                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", tagMask.Mask));
                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "RegEx: " + tagMask.RegExp));
                }
            }

            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Chain files: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.ChainFiles.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Chaining options: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.ChainingOption.ToString()));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Chaining tags: "));
            string message = (theImport.Importer.TagsToChainBy != null)
                                 ? theImport.Importer.TagsToChainBy.ToString()
                                 : String.Empty;
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", message));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Delete nonexistent files: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.DeleteNonExistentFiles.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Update all items: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.AlwaysUpdate.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Include file masks: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.IncludeFileMasks ?? "null"));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Exclude file masks: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.ExcludeFileMasks ?? "null"));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Include hidden files: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.IncludeHiddenFiles.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Min file size: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.MinFileSize.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Always update images: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.AlwaysUpdateImages.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            /*
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Image root folder: "));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", this.Importer.ImageFolder ?? "null"));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", string.Empty));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Search the files folder: "));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", this.Importer.FileFolderImage));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", string.Empty));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", "File folder image file masks: "));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", this.Importer.FileFolderImageFileMasks));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", string.Empty));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Image folder image file masks: "));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", this.Importer.ImageFolderImageFileMasks));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", string.Empty));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Image tag name: "));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", this.Importer.ImageTagName));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", string.Empty));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Image count: "));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", this.Importer.ImageCount.ToString()));
            this.Importer.LogMessages.Enqueue(new LogMessage("Debug", string.Empty));
            */
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Import creation date: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.CreationDate.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Creation date tag name: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.CreationDateTagName));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Import last access date: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.LastAccessDate.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Last access date tag name: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.LastAccessDateTagName));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Import last modification date: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.LastModificationDate.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Last modification date tag name: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.LastModificationDateTagName));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Import file size: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.FileSize.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "File size tag name: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.FileSizeTagName));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Import serial number: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.SerialNumber.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Serial number tag name: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.SerialNumberTagName));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Import volume name: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.VolumeName.ToString(CultureInfo.InvariantCulture)));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Volume name tag name: "));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", theImport.Importer.VolumeNameTagName));
            theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", String.Empty));
        }
    }
}
