﻿namespace FileImporter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    /// <summary>
    /// 
    /// </summary>
    internal class ImageCache
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly object _locker;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<string, List<string>> _foldersImages = new Dictionary<string, List<string>>();

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, List<string>> FoldersImages
        {
            get
            {
                return _foldersImages;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private object Locker
        {
            get
            {
                return _locker;
            }
        }




        public ImageCache()
        {
            _locker = new object();
        }







        public static string ReplaceInvalidFilenameChars(string filename)
        {
            string result = filename.Replace('<', '?');
            result = result.Replace('>', '?');
            result = result.Replace(':', '?');
            result = result.Replace('|', '?');
            result = result.Replace('"', '?');
            result = result.Replace('/', '?');
            result = result.Replace('\\', '?');
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static string WildcardToRegex(string pattern)
        {
            return "^" + Regex.Escape(ReplaceInvalidFilenameChars(pattern)).
                Replace("\\*", ".*").
                Replace("\\?", ".?") + "$";
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="imageFileMasks"></param>
        /// <returns></returns>
        public List<string> Query(string folder, string imageFileMasks)
        {
            lock (Locker)
            {
                var result = new List<string>();
              
                
                
                try
                {
                    if (string.IsNullOrEmpty(folder))
                    {
                        return result;
                    }


                    var searchMasks = new List<string>();


                    var imageMasksMatcher = new List<Regex>();
                  
                    
                    foreach (string imageFileMask in imageFileMasks.Split(':'))
                    {
                        imageMasksMatcher.Add(new Regex(WildcardToRegex(imageFileMask), RegexOptions.IgnoreCase));
                        string searchMask = ImageFileMask2SearchMask(imageFileMask);
                        if (!searchMasks.Contains(searchMask))
                        {
                            searchMasks.Add(searchMask);
                        }
                    }



                    if (!FoldersImages.ContainsKey(folder))
                    {
                        FoldersImages.Add(folder, new List<string>());
                        if (Directory.Exists(folder))
                        {
                            /*
                                foreach (string searchMask in searchMasks)
                                {
                                    this.FoldersImages[folder].AddRange(
                                        Directory.GetFiles(folder, searchMask, SearchOption.AllDirectories).ToList());
                                }
                                */
                            this.FoldersImages[folder].AddRange(
                                Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories).ToList());
                        }
                    }

                    result = this.FoldersImages[folder];

                    // IEnumerable<String> temp1 = Result.Where(Image => ImageMasksMatcher.Any(AMatch => AMatch.IsMatch(Path.GetFileName(Image))));
                    // IEnumerable<String> temp2 = temp1.OrderBy(Image => ImageMasksMatcher.FindIndex(AMatch2 => AMatch2.IsMatch(Path.GetFileName(Image))));
                    return
                        result.Where(
                            image => imageMasksMatcher.Any(aMatch => aMatch.IsMatch(Path.GetFileName(image)))).
                            OrderBy(
                                image =>
                                imageMasksMatcher.FindIndex(aMatch2 => aMatch2.IsMatch(Path.GetFileName(image)))).ToList
                            ();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error quering the imagecache: " + ex.Message);
                    return result;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageFileMask"></param>
        /// <returns></returns>
        private static string ImageFileMask2SearchMask(string imageFileMask)
        {
            if (!imageFileMask.Contains('.'))
            {
                return imageFileMask;
            }
            return '*' + imageFileMask.Remove(0, imageFileMask.LastIndexOf('.'));
        }










    }






}