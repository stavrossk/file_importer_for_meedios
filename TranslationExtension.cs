﻿namespace FileImporter
{
    using MeediOS.Cache;

    public static class TranslationExtension
    {
        public static object Plugin
        {
            get; set;
        }

        public static string Translate(this string text)
        {
            return Plugin != null ? TranslationProvider.Translate(text, Plugin) : text;
        }
    }
}
