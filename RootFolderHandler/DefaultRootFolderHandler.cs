namespace FileImporter.RootFolderHandler
{
    using System.Collections.Generic;

    internal class DefaultRootFolderHandler : RootFolderHandlerBase
    {
        #region Constructors and Destructors

        public DefaultRootFolderHandler(
            TheImport import,
            EnhancedDriveInfo drive_sInfo,
            string rootFolder,
            List<EnhancedFileInfo> foundFiles,
            ImageCache cachedImages)
            : base(import, drive_sInfo, rootFolder, foundFiles, cachedImages)
        {
        }

        #endregion

        #region Methods

        internal override void DoSpecificHandling()
        {
            UnchainedFileHandler.HandleUnchainedOrFirstFile(this, this.File);
        }

        #endregion
    }
}