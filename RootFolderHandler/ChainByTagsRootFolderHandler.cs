


namespace FileImporter.RootFolderHandler
{


    using System.Collections.Generic;

    using MeediOS;

    
    
    
    internal class ChainByTagsRootFolderHandler : RootFolderHandlerBase
    {


        #region Constants and Fields

        private readonly Item2File item2File;

        #endregion

        #region Constructors and Destructors

        public ChainByTagsRootFolderHandler(
            TheImport import,
            EnhancedDriveInfo driveSInfo,
            string rootFolder,
            List<EnhancedFileInfo> foundFiles,
            ImageCache cachedImages)
            : base(import, driveSInfo, rootFolder, foundFiles, cachedImages)
        {
            this.item2File = new Item2File();
        }

        #endregion

        #region Methods

        internal override void DoSpecificHandling()
        {


            if ((!IsNewItem) && (!this.Update))
            {


                if (Import.Importer.AlwaysUpdateImages)
                {
                    var tempFile = new EnhancedFileInfo
                        (File.FileInformation);

                    UnchainedFileHandler.HandleUnchainedOrFirstFile(this, tempFile);

                    File = tempFile;

                    Files[FilePos] = tempFile;

                    item2File.Add(tempFile);

                }
                else
                {
                    Files.Remove(File);
                }


            }
            else
            {


                var tempFile = new EnhancedFileInfo
                    (File.FileInformation);


                UnchainedFileHandler.HandleUnchainedOrFirstFile(this, tempFile);

                IMLItem cachedItem
                    = Import.ItemsChainedByTagCache.Query
                    (tempFile.Item);
                
                
                if (cachedItem != tempFile.Item)
                {

                    File = item2File.Query(cachedItem);

                    File.SuccessfulTagMaskMatch
                        = tempFile.SuccessfulTagMaskMatch;


                    ChainedFileHandler
                        .HandleChainedByTagFile
                        (File, tempFile);


                    if (item2File.RetrivedItemWasCached)
                    {
                        Files.RemoveAt(FilePos);
                    }
                    else
                    {
                        Files[FilePos] = File;
                    }


                }
                else
                {
                    Files[FilePos] = tempFile;
                    item2File.Add(tempFile);
                }


            }


        }

        #endregion
    }
}