namespace FileImporter
{
    using System.Collections.Generic;
    using System.Linq;

    using MCW.Utility.Logging;
    using MCW.Utility.MeediOS;

    public class ImageLinker
    {
        #region Constants and Fields

        private readonly ImageCache cachedImages;

        private readonly List<ImageSearchOptions> imageSearches;

        private readonly LogMessagesQueue logMessages;

        #endregion

        #region Constructors and Destructors

        public ImageLinker(LogMessagesQueue logMessages)
        {
            this.logMessages = logMessages;
            this.imageSearches = new List<ImageSearchOptions>();
            this.cachedImages = new ImageCache();
        }

        #endregion

        #region Properties

        private ImageCache CachedImages
        {
            get
            {
                return this.cachedImages;
            }
        }

        internal List<ImageSearchOptions> ImageSearches
        {
            get
            {
                return this.imageSearches;
            }
        }

        private LogMessagesQueue LogMessages
        {
            get
            {
                return this.logMessages;
            }
        }

        #endregion

        #region Public Methods

        public void LinkAllImages(EnhancedFileInfo file)
        {
            for (int imageSearchesPos = 0; imageSearchesPos < this.imageSearches.Count; imageSearchesPos++)
            {
                this.LinkImages(file, this.ImageSearches[imageSearchesPos]);
            }
        }

        public void LinkImages(EnhancedFileInfo file, ImageSearchOptions searchOptions)
        {
            this.LogMessages.Enqueue(new LogMessage("Info", "  Searching images"));
            if (searchOptions.ImageTagName.Length > 0)
            {
                file.Item.Tags[searchOptions.ImageTagName] = string.Empty;
            }
            if (searchOptions.ImageTagName.Length == 0 || searchOptions.SaveInImageBaseTag)
            {
                file.Item.ImageFile = string.Empty;
            }

            List<string> imagePaths = new List<string>();
            EnhancedFileInfo tmpFile = file;
            string[] parameter =
                searchOptions.ImageFolderSearchPattern.Split(';').Select(mask => MLItem.EvaluateTagValues(mask, tmpFile.Item)).
                    ToArray();
            string evaluatedImageFileMasks = string.Join(":", parameter);
            this.LogMessages.Enqueue(new LogMessage("Info", "    in the image folder"));
            imagePaths.AddRange(this.CachedImages.Query(searchOptions.ImageFolder, evaluatedImageFileMasks));
            bool shouldSearchInFileFolder = (searchOptions.SearchFileFolderOptions == "Always".Translate()) ||
                                            ((searchOptions.SearchFileFolderOptions ==
                                             "When no linked files in links rootfolder".Translate()) && (imagePaths.Count == 0));
            if (shouldSearchInFileFolder)
            {
                string[] parameter2 =
                    searchOptions.FileFolderSearchPattern.Split(';').Select(
                        mask => MLItem.EvaluateTagValues(mask, tmpFile.Item)).ToArray();
                evaluatedImageFileMasks = string.Join(":", parameter2);
                this.LogMessages.Enqueue(new LogMessage("Info", "    in the files folder"));
                imagePaths.AddRange(
                    this.CachedImages.Query(file.FileInformation.DirectoryName, evaluatedImageFileMasks));
            }

            if ((searchOptions.MaxImages > 0) && (imagePaths.Count > searchOptions.MaxImages))
            {
                imagePaths.RemoveRange(searchOptions.MaxImages, imagePaths.Count - searchOptions.MaxImages);
            }

            if (imagePaths.Count > 0)
            {
                this.LogMessages.Enqueue(new LogMessage("Info", "    Writing image tag(s)"));
                MLTag imageFile = new MLTag("ImageFile", string.Empty);
                foreach (string image in imagePaths)
                {
                    imageFile.Values.Add(image);
                }

                if (searchOptions.ImageTagName.Equals(string.Empty))
                {
                    file.Item.ImageFile = imageFile.Value;
                }
                else
                {
                    file.Item.Tags[searchOptions.ImageTagName] = imageFile.Value;
                    if (searchOptions.SaveInImageBaseTag)
                    {
                        file.Item.Tags["imagefile"] = imageFile.Values[0];
                        file.Item.ImageFile = imageFile.Values[0];
                    }
                }

                this.LogMessages.Enqueue(new LogMessage("Info", "    ..Done writing image tag(s)"));
            }
        }

        #endregion
    }
}