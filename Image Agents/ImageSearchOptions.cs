namespace FileImporter
{
    public class ImageSearchOptions
    {
        #region Properties

        public string ImageFolder { get; set; }

        public string ImageTagName { get; set; }

        public int MaxImages { get; set; }

        public string SearchFileFolderOptions { get; set; }

        public string ImageFolderSearchPattern { get; set; }

        public string FileFolderSearchPattern { get; set; }

        public bool SaveInImageBaseTag { get; set; }

        #endregion
    }
}