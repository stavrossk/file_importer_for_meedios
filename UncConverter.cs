﻿#region Usings



#endregion

namespace FileImporter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Management;
    using System.Text.RegularExpressions;
    using System.Threading;

    /// <summary>
    /// Class to store a combination of UNC path and Drive letter
    /// </summary>
    internal class UncToDrive
    {
        #region Constructors and Destructors

        /// <summary>
        /// Creates an UncToDrive object.
        /// </summary>
        /// <param name="unc"></param>
        /// <param name="drive"></param>
        public UncToDrive(String unc, String drive)
        {
            this.Unc = unc;
            //Create regex where both slash and backslash are allowed for \
            this.Regex = String.Format("^(?<unc>{0})", this.Unc.Replace(@"\", @"(/|\\)").Replace("$", @"\$"));
            this.Drive = drive;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get/Set the drive letter for this object
        /// </summary>
        public String Drive { get; set; }

        /// <summary>
        /// Get/Set the regex string for this object
        /// </summary>
        public String Regex { get; set; }

        /// <summary>
        /// Get/Set the UNC path of this object
        /// </summary>
        public String Unc { get; set; }

        #endregion
    }

    public class UncConverter
    {
        #region Constants and Fields

        private static UncConverter _instance;

        private List<UncToDrive> _drives;

        private bool _loaded;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Private constructor. Loads the drive info from management objects.
        /// </summary>
        private UncConverter()
        {
            ThreadStart loaderStart = this.LoadDrives;
            Thread loader = new Thread(loaderStart);
            loader.Start();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns an instance of the UNCConverter
        /// </summary>
        /// <returns>The UNCConverter instance</returns>
        public static UncConverter getInstance()
        {
            if (Equals(_instance, null))
            {
                UncConverter instance = new UncConverter();
                if (Equals(_instance, null))
                {
                    _instance = instance;
                }
            }
            return _instance;
        }

        /// <summary>
        /// Converts the input source to a source with drive letter if possible. Returns source if no drive letter was found
        /// </summary>
        /// <param name="uncSource">The source path in UNC style</param>
        /// <returns>The source in mounted drive letter if possible</returns>
        public String ConvertToDrive(String uncSource)
        {
            if (this._loaded)
            {
                Match match;
                foreach (UncToDrive drive in this._drives)
                {
                    match = Regex.Match(uncSource, drive.Regex, RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        String result = uncSource.Replace(match.Groups["unc"].Value, drive.Drive);
                        if (File.Exists(result) || Directory.Exists(result))
                        {
                            return result;
                        }
                    }
                }
            }
            else
            {
                return String.Empty;
            }
            return uncSource;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Uses ManagementObjectSearch to load all local and remote name mappings to the _drives list.
        /// </summary>
        private void LoadDrives()
        {
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher("SELECT RemoteName, LocalName FROM Win32_NetworkConnection");
            List<UncToDrive> drives = new List<UncToDrive>();
            foreach (ManagementObject obj in searcher.Get())
            {
                object localObj = obj["LocalName"];
                if (!Equals(localObj, null))
                {
                    drives.Add(new UncToDrive(obj["RemoteName"].ToString(), localObj.ToString()));
                }
            }
            this._drives = drives;
            this._loaded = true;
        }

        #endregion
    }

    public class UncConverter2 : IDisposable
    {
        #region Constants and Fields

        private List<UncToDrive> Drives;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Private constructor. Loads the drive info from management objects.
        /// </summary>
        public UncConverter2()
        {
            this.Drives = new List<UncToDrive>();
            this.Refresh();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Converts the input source to a source with drive letter if possible. Returns source if no drive letter was found
        /// </summary>
        /// <param name="uncSource">The source path in UNC style</param>
        /// <returns>The source in mounted drive letter if possible</returns>
        public String ConvertToDrive(String uncSource)
        {
            Match match;
            foreach (UncToDrive Drive in this.Drives)
            {
                match = Regex.Match(uncSource, Drive.Regex, RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    String result = uncSource.Replace(match.Groups["unc"].Value, Drive.Drive);
                    if (File.Exists(result) || Directory.Exists(result))
                    {
                        return result;
                    }
                }
            }
            return uncSource;
        }

        /// <summary>
        /// Uses ManagementObjectSearch to load all local and remote name mappings to the Drives list.
        /// </summary>
        public void Refresh()
        {
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher("SELECT RemoteName, LocalName FROM Win32_NetworkConnection");
            this.Drives.Clear();
            foreach (ManagementObject obj in searcher.Get())
            {
                object localObj = obj["LocalName"];
                if (!Equals(localObj, null))
                {
                    this.Drives.Add(new UncToDrive(obj["RemoteName"].ToString(), localObj.ToString()));
                }
            }
        }

        #endregion

        #region Implemented Interfaces

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #endregion

        #region Methods

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                /*
				for (Int32 DrivePos = 0; DrivePos < Drives.Count; DrivePos++)
				{
					Drives[DrivePos] = null;
				}
				*/
                this.Drives.Clear();
                //				Drives.Capacity = 0;
                this.Drives = null;
                // Free other state (managed objects).
            }
            // Free your own state (unmanaged objects).
            // Set large fields to null.
        }

        #endregion
    }
}