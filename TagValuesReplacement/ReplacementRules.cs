﻿namespace FileImporter.TagValuesReplacement
{
    using System.Collections.Generic;
    using System.Linq;
    using MeediOS;
    
    internal class ReplacementRules
    {
        private readonly List<ReplacementRulesGroup> ruleGroups = new List<ReplacementRulesGroup>();

        public ReplacementRules(string[] rules)
        {
            if ((rules != null) && (rules.Length > 0))
            {
                this.RuleGroups.Add(new ReplacementRulesGroup());
                foreach (string rule in rules)
                {
                    if (ReplacementRule.RuleVerifierParser.IsMatch(rule))
                    {
                        if (string.IsNullOrEmpty(rule) || rule.Equals("\"\""))
                        {
                            this.ruleGroups.Add(new ReplacementRulesGroup());
                        }
                        else
                        {
                            this.RuleGroups.Last().Rules.Add(new ReplacementRule(rule));
                        }
                    }
                    else
                    {
                    }
                }
            }
        }

        public List<ReplacementRulesGroup> RuleGroups
        {
            get
            {
                return this.ruleGroups;
            }
        }

        public string[] Serialize()
        {
            List<string> rules = new List<string>();
            for (int groupPos = 0; groupPos < this.RuleGroups.Count; groupPos++)
            {
                rules.AddRange(this.RuleGroups[groupPos].Serialize());
                if (groupPos < this.RuleGroups.Count - 1)
                {
                    rules.Add(string.Empty);
                }
            }

            return rules.ToArray();
        }

        public void ApplyRulesToItem(IMLItem item)
        {
            foreach (ReplacementRulesGroup group in this.ruleGroups)
            {
                group.ApplyRulesToItem(item);
            }
        }
    }
}