﻿using System.Linq;
using FileImporter.CORE;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;
using MeediOS;



namespace FileImporter
{


    class ExistingItemScanner
    {





        internal static void ScanExistingItems
            (TheImport theImport)
        {


            IMLItemList oldItems
                = theImport.Importer
                .Section.GetReadOnlyItems();



            int oldItemCount = oldItems.Count;


            const string logMessage1 
                = "Prescanning existing items:";


            if (!theImport
                .CheckProgress
                (0, logMessage1))
            {
                return;
            }




            bool createItemsChainedByTagCache
                = (theImport.Importer
                .ChainingOption
                == ChainOption.ChainByTags)
                &&   (theImport.Importer
                .TagsToChainBy.Length > 0);



            for (int itemPos = 0; itemPos < oldItemCount; itemPos++)
            {
                IMLItem item = oldItems[itemPos];
                var locationTag = new MLTag("Location", item.Location);




                foreach (string location in locationTag.Values)
                {

                    if (theImport.Location2Id.Keys.Contains
                        (location.ToLowerInvariant()))
                        continue;


                    string logMessage2 = "Mapped the location " + location  + " to the itemid " + item.ID;
                    int percentage = (itemPos * 10) / oldItemCount;
                    if (!theImport.CheckProgress(percentage, logMessage2))
                    {
                        return;
                    }

                    theImport.Location2Id.Add(location.ToLowerInvariant(), item.ID);
                }


                if (!createItemsChainedByTagCache) 
                    continue;


                IMLItem writableItem 
                    = theImport.Importer.Section
                        .FindItemByID(item.ID);


                IMLItem cachedItem 
                    = theImport.ItemsChainedByTagCache
                        .Query(writableItem);


                if (cachedItem == writableItem)
                    continue;


                string logMessage4 
                    = "You are chaining by tags " +
                      "but the item (ID: " + item.ID 
                      + " Location: " 
                      + item.Location 
                      + ") has the same values" +
                      " for the tags you're chaining" +
                      " by as the item (ID: " 
                      +  cachedItem.ID
                      + " Location: " 
                      + cachedItem.Location + ").";


                theImport.Importer.LogMessages.Enqueue
                    (new LogMessage
                         ("Warn", logMessage4));



            }




        }



    }



}
