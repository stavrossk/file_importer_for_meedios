﻿using System;
using System.IO;
using FileImporter.Debugger;
using MCW.Utility.Logging;
using RedGate.Profiler;
using log4net;
using log4net.Config;




namespace FileImporter.CORE
{




    class CoreEngineHelpers
    {




        internal static void Snapshot()
        {
            try
            {
                Api.TakeSnapshot();
            }
            catch (ProfilerNotRunningException)
            {
            }
        }







        internal static void SetupLogging(FileImporter fileImporter)
        {

            if (fileImporter.DebugLevel == "Off")
                return;


            if (!String.IsNullOrEmpty(fileImporter.DataPath) && Directory.Exists(fileImporter.DataPath))
            {
                fileImporter.Log = LogManager.GetLogger(typeof(FileImporter));
                string logConfigFilename = fileImporter.FileImporterDataPath +
                                           "LoggerConfig_" + fileImporter.Section.Name + ".xml";
                LogConfigCreator.CreateLogConfig(fileImporter, logConfigFilename);
                fileImporter.LogConfig = new FileInfo(logConfigFilename);
                XmlConfigurator.Configure(fileImporter.LogConfig);
                fileImporter.LogConfig = null;
                fileImporter.LogMessages.Log = fileImporter.Log;
            }
            else
            {
                const string logMessage1 = "DataPath wasn't retrieved correctly preventing logging in the logs folder.";
                fileImporter.LogMessages.Enqueue(new LogMessage("Warn", logMessage1));
            }




        }




    }





}
