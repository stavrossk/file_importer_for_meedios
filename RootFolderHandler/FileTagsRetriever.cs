﻿using System;
using System.Text.RegularExpressions;
using FileImporter.AlternateDataStreamImportExport;
using MCW.Utility;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;




namespace FileImporter.RootFolderHandler
{



    class FileTagsRetriever
    {






        protected internal static void GetTagsOfFile
            (RootFolderHandlerBase rootFolderHandlerBase,
             EnhancedFileInfo theFile)
        {
            bool tagMaskMatched = false;
            const string folderPattern = "(?:.*\\\\)*";

            rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Info", "  Retrieving tags"));
            MLItem.AddBaseTagsToTags(theFile.Item);
            if (rootFolderHandlerBase.Importer.ImportFromAlternateDataStream)
            {
                AlternateDataStreamReader adsReader = new AlternateDataStreamReader(rootFolderHandlerBase.File);
                adsReader.ReadItem();
            }

            theFile.SuccessfulTagMaskMatch = rootFolderHandlerBase.Importer.TagMasks.Count == 0;


            foreach (TagMask tagMask in rootFolderHandlerBase.Importer.TagMasks)
            {


                if (tagMask.Mask.Length == 0)
                {
                    tagMaskMatched = false;
                }
                else if (!tagMaskMatched)
                {


                    if (!rootFolderHandlerBase.RootFolder.EndsWith("\\"))
                    {
                        rootFolderHandlerBase.RootFolder += "\\";
                    }

                    string rootFolderPattern = RootFolderHandlerBase
                        .PatternFixEscapeChars
                        (rootFolderHandlerBase.RootFolder);
               
                    
                    string regExpPattern = rootFolderPattern + folderPattern + tagMask.RegExp;



                    try
                    {


                        Regex tagValuesFinder = new Regex
                            (regExpPattern,
                             RegexOptions.IgnoreCase);


                        MatchCollection matches 
                            = tagValuesFinder.Matches
                            (theFile.Item.Location
                            .Replace("._", ' ')
                            .ReplaceLast(' ', '.'));
                      
                        
                        
                        if (matches.Count == 1)
                        {
                            tagMaskMatched = true;
                            theFile.SuccessfulTagMaskMatch = true;
                            Match match = matches[0];
                            rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Info", "    Matched tagmask: " + tagMask.Mask));
                            int groupPos = 0;
                            int tagPos = 0;
                            foreach (Group group in match.Groups)
                            {
                                if (groupPos > 0)
                                {
                                    while (tagMask.Tags[tagPos].Name.Length == 0)
                                    {
                                        tagPos++;
                                    }

                                    theFile.Item.Tags[tagMask.Tags[tagPos].Name] = @group.Value;
                                    tagPos++;
                                }

                                groupPos++;
                            }
                        }
                        else
                        {
                            rootFolderHandlerBase.LogMessages.Enqueue(
                                new LogMessage("Info", "    No match for tagmask: " + tagMask.Mask));
                        }
                        //// LogMessages.Enqueue(new LogMessage("Info", String.Empty));
                    }
                    catch (ArgumentException ex)
                    {
                        string logMessage = "RegEx could not be generated from the tag mask \"" + 
                                            tagMask.Mask + "\"" + " (Exception: " + ex.Message + ")";
                        rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Error", logMessage));
                    }



                }


            }



            rootFolderHandlerBase.Importer
                .TagValuesReplacementRules
                .ApplyRulesToItem(theFile.Item);



        }










    }


}
