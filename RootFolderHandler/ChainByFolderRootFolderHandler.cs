namespace FileImporter.RootFolderHandler
{
    using System.Collections.Generic;

    internal class ChainByFolderRootFolderHandler : RootFolderHandlerBase
    {
        #region Constants and Fields

        private EnhancedFileInfo previousFile;

        #endregion

        #region Constructors and Destructors

        public ChainByFolderRootFolderHandler(
            TheImport import,
            EnhancedDriveInfo drive_sInfo,
            string rootFolder,
            List<EnhancedFileInfo> foundFiles,
            ImageCache cachedImages)
            : base(import, drive_sInfo, rootFolder, foundFiles, cachedImages)
        {
        }

        #endregion

        #region Methods

        internal override void DoSpecificHandling()
        {
            if ((this.previousFile == null) ||
                (this.File.FileInformation.DirectoryName != this.previousFile.FileInformation.DirectoryName))
            {
                UnchainedFileHandler.HandleUnchainedOrFirstFile(this, this.File);
                this.previousFile = this.File;
            }
            else
            {
                ChainedFileHandler.HandleChainedFile(this, this.File, this.previousFile);
                this.Files.Remove(this.File);
            }
        }

        #endregion
    }
}