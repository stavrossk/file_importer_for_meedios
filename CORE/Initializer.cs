﻿using System;
using System.IO;
using System.Windows.Forms;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;
using MCW.Utility.MeediOS.GlobalPluginProperties;
using MediaLocations = MCW.Utility.MeediOS.Locations.MediaLocations;




namespace FileImporter.CORE
{



    class Initializer
    {






        internal static void Initialize
            (FileImporter fileImporter)
        {


            if (fileImporter.Initialized)
                return;



            try
            {


                fileImporter.LogMessages
                    = new LogMessagesQueue();

                fileImporter.MediaProfilesProvider
                    = MediaLocations.GetInstance();

                fileImporter.GlobalPluginProperties
                    = new GlobalPluginProperties();

                fileImporter.ImageLinker 
                    = new ImageLinker(fileImporter.LogMessages);

                fileImporter.DataPath
                    = FolderLocations.Data;

                fileImporter.FileImporterDataPath
                    = fileImporter.DataPath + "FileImporter\\";




                if (!String.IsNullOrEmpty(fileImporter.DataPath))
                {
                    if (fileImporter.IsFirstLoad)
                    {
                        if (!Directory.Exists(fileImporter.FileImporterDataPath))
                        {
                            Directory.CreateDirectory(fileImporter.FileImporterDataPath);
                        }
                    }
                }
                else
                {
                    fileImporter
                        .LogMessages.Enqueue
                        (new LogMessage("Warn", 
                            "Invalid DataPath"));
                }

                TranslationExtension.Plugin = fileImporter;


                fileImporter.ImporterSettings
                    = new ImporterPropertiesCore
                        .FileImporterProperties(fileImporter);
            
            
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw new FileImporter.FileImporterInitializationException(ex);
            }

            fileImporter.Initialized = true;


        }








    }



}
