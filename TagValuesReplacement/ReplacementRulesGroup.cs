﻿namespace FileImporter.TagValuesReplacement
{
    using System;
    using System.Collections.Generic;
    using MeediOS;

    internal class ReplacementRulesGroup
    {
        private readonly List<ReplacementRule> rules = new List<ReplacementRule>();

        public List<ReplacementRule> Rules
        {
            get { return this.rules; }
        }

        public void ApplyRulesToItem(IMLItem item)
        {
            foreach (ReplacementRule rule in this.rules)
            {
                if (rule.ApplyRuleToItem(item))
                {
                    break;
                }
            }
        }

        internal string[] Serialize()
        {
            List<string> result = new List<string>();
            for (int rulePos = 0; rulePos < this.Rules.Count; rulePos++)
            {
                result.Add(this.Rules[rulePos].Serialize());
            }

            return result.ToArray();
        }
    }
}