﻿using System;
using MCW.Utility.Logging;



namespace FileImporter.RootFolderHandler
{



    class UnchainedFileHandler
    {




        internal static void HandleUnchainedOrFirstFile
            (RootFolderHandlerBase rootFolderHandlerBase, EnhancedFileInfo theFile)
        {

            theFile.Item.Location = theFile.FileInformation.FullName;
            theFile.Item.ExternalID = theFile.Item.Location.ToLower();

            if (rootFolderHandlerBase.Importer.TagMasks != null)
            {

                FileTagsRetriever.GetTagsOfFile
                    (rootFolderHandlerBase, theFile);

            }


            theFile.Item.Name = theFile.Item.Tags.Get("name", String.Empty) as string;

            rootFolderHandlerBase.Importer.ImageLinker.LinkAllImages(theFile);


            if (!rootFolderHandlerBase.IsNewItem && !rootFolderHandlerBase.Update)
                return;


            rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Info", "  Getting filesystem information"));

            if (rootFolderHandlerBase.Importer.FileSize)
            {
                theFile.Item.Tags[rootFolderHandlerBase.Importer.FileSizeTagName] 
                    = theFile.FileInformation.Length;
            }


            if (rootFolderHandlerBase.Importer.CreationDate)
            {
                theFile.Item.Tags[rootFolderHandlerBase.Importer.CreationDateTagName]
                    = theFile.FileInformation.CreationTime.ToString("yyyy-MM-dd hh:mm:ss");
            }


            if (rootFolderHandlerBase.Importer.LastAccessDate)
            {
                theFile.Item.Tags[rootFolderHandlerBase.Importer.LastAccessDateTagName] 
                    = theFile.FileInformation.LastAccessTime.ToString("yyyy-MM-dd hh:mm:ss");
            }



            if (rootFolderHandlerBase.Importer.LastModificationDate)
            {
                theFile.Item.Tags[rootFolderHandlerBase.Importer.LastModificationDateTagName]
                    = theFile.FileInformation.LastWriteTime.ToString("yyyy-MM-dd hh:mm:ss");
            }


            if (rootFolderHandlerBase.Importer.SerialNumber)
            {
                theFile.Item.Tags[rootFolderHandlerBase.Importer.SerialNumberTagName] = rootFolderHandlerBase.InfoOfDrive.SerialNumber;
            }



            if (rootFolderHandlerBase.Importer.VolumeName)
            {
                theFile.Item.Tags[rootFolderHandlerBase.Importer.VolumeNameTagName] = rootFolderHandlerBase.InfoOfDrive.VolumeLabel;
            }
        }
    }
}
