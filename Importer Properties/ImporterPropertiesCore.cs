﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CustomProperties;
using CustomProperties.PropertyData;
using FileImporter.ImporterProperties;
using FileImporter.TagValuesReplacement;
using MCW.Utility.Logging;



namespace FileImporter
{




    class ImporterPropertiesCore
    {



        internal class FileImporterProperties : CustomProperty
        {



            #region Constants and Fields

            internal readonly string tvShowsReplacements = String.Empty;

            internal readonly string addImageSearchToggleDefault = "Add a new file linker";

            internal readonly string addImageSearchToggleHelptext =
                "Click to add a new file linker. Each file search can have it's own settings.";

            internal readonly string AlwaysUpdateImagesCaption = "Always update linked files";

            internal readonly string AlwaysUpdateImagesHelptext =
                "If set the linked files will be updated even when the item's file didn\"t change";

            internal readonly string FileFolderImageCaption = "Search the files folder";

            internal readonly string FileFolderImageChoice1 = "When no linked files in links rootfolder";

            internal readonly string FileFolderImageChoice2 = "Never";

            internal readonly string FileFolderImageChoice3 = "Always";

            internal readonly string FileFolderImageHelptext =
                "The importer will first search for linked files in the location specified in the above 'Links rootfolder' property" +
                Environment.NewLine + "This property defines if the folder of the imported file will be searched too." +
                Environment.NewLine +
                "If set to 'When no linked files in links rootfolder' the folder of the imported file will only be searched," +
                Environment.NewLine + "if the previous search in the links rootfolder didn't find a matching file.";

            private readonly CORE.FileImporter fileImporter;

            internal readonly string imageCountCaption = "File count";

            internal readonly string imageCountHelptext = "Specify how many files should be linked to the item's file." +
                                                          Environment.NewLine +
                                                          "If set to 0 all found files will be linked to the item's file.";

            internal readonly string ImageFileMasksCaption = "File masks";

            internal readonly string ImageFileMasksHelptext =
                "Enter any number of file masks separated by semicolons to be found by this search." +
                Environment.NewLine + "For example: '<name>.png;folder.jpg;*.bmp' (without the quotes)" +
                Environment.NewLine + "If left blank, no files will be found." + Environment.NewLine +
                Environment.NewLine +
                "The 'File masks' are used for both the 'Links rootfolder' and for the item's 'Rootfolders'" +
                Environment.NewLine + "But you can also have different 'File masks' for the two." + Environment.NewLine +
                "Seperate with a ':'. The 'Links rootfolder' 'File masks' go to the left side." + Environment.NewLine +
                "Ex: '<name>.*:*.jpg;*.bmp;*.png' (without the quotes)." + Environment.NewLine +
                "Searches for all files in the 'Links rootfolder' with the filename equal to the name tag of the item" +
                Environment.NewLine +
                "and all files in the folder the item's file is in matching one of the three image extensions.";

            internal readonly string ImageFolderCaption = "Links rootfolder";

            internal readonly string ImageFolderHelptext =
                "If your linked files are in a folder other than the one you import your files from," +
                Environment.NewLine + "select it here. It will be used as the rootfolder for the search.";

            internal readonly string ImageTagNameCaption = "Links tag name";

            internal readonly string imageTagNameHelptext =
                "If blank all linked files will be saved in the default image tag." + Environment.NewLine +
                "Otherwise specify the name of the tag the linked files should be saved to.";

            internal readonly string miscGroupcaption = "File links";

            private readonly string moviesRegex;

            private readonly string musicRegex;



            internal readonly string RemoveImageSearchToggleDefault
                = "Remove this file linker";



            internal readonly string RemoveImageSearchToggleHelptext 
                = "Click to remove this file linker";



            internal readonly string SaveInImageBaseTagCaption 
                = "Save to image base tag";



            internal readonly string SaveInImageBaseTagHelptext =
                "If set the first found file will also be written to the base image tag.";



            private readonly string _tvShowsRegex;


            internal static string AudioExtensions =
                "*.aac;*.ac3;*.aif;*.ape;*.flac;" +
                "*.iff;*.mid;*.midi;*.mp2;*.mp3;" +
                "*.mp4;*.mpa;*.ogg;*.pcm;" +
                "*.ra;*.wav;*.wma";



            internal static string ImageExtensions 
                = "*.bmp;*.gif;*.jpeg;*.jpg;*.mng;" +
                  "*.pct;*.png;*.psd;*.psp;*.tif";

            internal static string TextExtensions = "*.doc;*.htm;*.html;*.ini;*.log;*.pdf;*.rtf;*.txt;*.wpd;*.wps;*.xml";

            internal static string VideoExtensions =
                "*.3g2;*.3gp;*.3gp2;*.3gpp;*.60d;*.ajp;*.asf;*.asx;*.avi;*.avs;*.bdmv;*.bik;*.bix;*.box;*.bsf;*.divx;*.dmf;*.dv;*.dvr-ms;*.evo;*.flc;*.flic;*.flv;*.flx;*.gvi;*.gvp;*.h264;*.m1v;*.m2p;*.m2t;*.m2ts;*.m2v;*.m4e;*.mjp;*.mjpeg;*.mjpg;*.mkv;*.mmv;*.moov;*.mov;*.movhd;*.movie;*.movx;*.mp4;*.mpe;*.mpeg;*.mpg;*.mpv;*.mpv2;*.mxf;*.nut;*.ogg;*.ogm;*.ps;*.qt;*.ram;*.rm;*.rmvb;*.swf;*.tod;*.trp;*.ts;*.vid;*.video;*.viv;*.vivo;*.vob;*.vro;*.wm;*.wmv;*.wmx;*.wvx;*.wx;*.xvid";

            private RulesSetupForm _tagValuesReplacementRulesSetupDialog;

            #endregion



            #region Constructors and Destructors



            public FileImporterProperties(CORE.FileImporter fileImporter)
                : base(false, ";;;", "|||", ":::")
            {
                this.fileImporter = fileImporter;
                this.Plugin = fileImporter;
                string fileImporterDataPath = this.fileImporter.DataPath + "FileImporter\\";

                if (File.Exists(fileImporterDataPath + "TVShowsReplacementRules.txt"))
                {
                    try
                    {
                        this.tvShowsReplacements = File.ReadAllText(fileImporterDataPath + "TVShowsReplacementRules.txt");
                    }
                    catch (Exception ex)
                    {
                        this.fileImporter.LogMessages.Enqueue(
                            new LogMessage(
                                "Warn", "Unable to load the default TVShowsTagValueReplacementRules." + Environment.NewLine + ex));
                    }
                }

                if (File.Exists(fileImporterDataPath + "TVShowsRegex.txt"))
                {
                    try
                    {
                        this._tvShowsRegex = File.ReadAllText(fileImporterDataPath + "TVShowsRegex.txt");
                    }
                    catch (Exception ex)
                    {
                        this.fileImporter.LogMessages.Enqueue(
                            new LogMessage(
                                "Warn", "Unable to load the default TVShowsRegex." + Environment.NewLine + ex));
                    }
                }

                if (File.Exists(fileImporterDataPath + "MoviesRegex.txt"))
                {
                    try
                    {
                        moviesRegex = File.ReadAllText(fileImporterDataPath + "MoviesRegex.txt");
                    }
                    catch (Exception ex)
                    {
                        this.fileImporter.LogMessages.Enqueue(
                            new LogMessage("Warn", "Unable to load the default MoviesRegex." + Environment.NewLine + ex));
                    }
                }

                if (File.Exists(fileImporterDataPath + "MusicRegex.txt"))
                {
                    try
                    {
                        musicRegex = File.ReadAllText(fileImporterDataPath + "MusicRegex.txt");
                    }
                    catch (Exception ex)
                    {
                        this.fileImporter.LogMessages.Enqueue(
                            new LogMessage("Warn", "Unable to load the default MusicRegex." + Environment.NewLine + ex));
                    }
                }

                InitializeOptionSettings();
            }

            #endregion




            #region Properties

            internal string MoviesRegex
            {
                get
                {
                    return moviesRegex;
                }
            }

            internal string MusicRegex
            {
                get
                {
                    return musicRegex;
                }
            }

            internal string TVShowsRegex
            {
                get
                {
                    return _tvShowsRegex;
                }
            }

            #endregion





            #region Public Methods

            public override bool GetProperty
                (int index, OptionSettings prop)
            {


                int i = 1;





                if (MiscPropertiesGetter.GetPropertyForMisc
                    (index, ref i, prop,
                     fileImporter, this))
                {
                    return true;
                }




                if (FileInclusionPropertiesGetter.GetPropertyForFileInclusion
                    (index, ref i, prop))
                {
                    return true;
                }



                if (ImageAssociationPropertiesGetter.GetPropertyForImageAssociation
                    (this, index, ref i, prop))
                {
                    return true;
                }



                if (FileSystemInfoPropertiesGetter.GetPropertyForFilesystemInfo
                    (index, ref i, prop))
                {
                    return true;
                }



                if (AdsPropertiesGetter.GetPropertyForAlternateDataStreamsOptions
                    (index, ref i, prop))
                {
                    return true;
                }





                return GetPropertyForDebugging
                    (index, i, prop);



            }

            #endregion






            #region Methods

            protected override bool EditCustomProperty
                (IntPtr window, string propertyName,
                 ref object value)
            {
                if (propertyName == "TagValuesReplacement")
                {


                    var tagNames 
                        = new List<string> 
                        { "id", "name", "externalid", 
                            "timestamp" };


                    tagNames.AddRange
                        (fileImporter.Section
                        .GetTagNames());


                    _tagValuesReplacementRulesSetupDialog
                        = new RulesSetupForm((string[])value, 
                            tagNames.ToArray());


                    if (_tagValuesReplacementRulesSetupDialog
                        .ShowDialog() 
                        == DialogResult.OK)
                    {
                        value = this._tagValuesReplacementRulesSetupDialog.RuleValues;
                        return true;
                    }


                    return false;
                }




                return true;
            }








            private bool GetPropertyForDebugging
                (int index, int i,
                 OptionSettings prop)
            {
                if (index == i)
                {
                    prop.Name = "DebugLevel";
                    prop.Caption = "Debug level";
                    prop.HelpText = "If not set to 'Off' a log file will be created." + Environment.NewLine +
                                    "The debug level defines what messages get written. (Higher levels include the lower levels.)";
                    prop.Choices = new[] { "Off", "Warn", "Info", "Debug" };
                    prop.DataType = "string";
                    prop.DefaultValue = "Off";
                    prop.GroupCaption = "Debugging";
                    prop.GroupName = "Debugging";
                    if (fileImporter.IsFirstLoad)
                    {
                        prop.Dependencies = ".*:::UPDATE MediaType";
                    }

                    return true;
                }

                return false;
            }



            internal string FixMediaLocation(string key)
            {
                if (fileImporter
                    .GlobalPluginProperties
                    .MediaLocations
                    .Locations.ContainsKey(key))
                {
                    return String.Join(
                        Environment.NewLine,
                        fileImporter.GlobalPluginProperties.MediaLocations.Locations[key].Split(new[] { '|' }));
                }


                return String.Empty;
            }







            #endregion





        }



    }






}
