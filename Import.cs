﻿
namespace FileImporter
{



    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Runtime.Remoting.Messaging;
    using System.Text.RegularExpressions;
    using MCW.Utility.Logging;
    using MCW.Utility.MeediOS;


    internal class TheImport : IDisposable
    {



        #region Field members


        internal const string RootfolderRequestMessage
            = "You should add at least one rootfolder!";



        internal List<AsyncResult> AsyncCalls;

        private UncConverter2 _uncConverter;

        internal Dictionary<string, EnhancedDriveInfo> DriveLetters;

        internal TimeSpan Duration;

        internal FilesQueue Files;

        private List<char> _remainingDriveLetters;

        internal StringDictionary RootFolderOrUnc2RootFolder;

        internal Dictionary<EnhancedDriveInfo, List<string>> RootFoldersOfDrive;

        internal List<NetworkDrive> TemporaryMappedDrives;

        internal List<string> RelocatedFiles;

        #endregion




        #region Constructors




        public TheImport(CORE.FileImporter importer)
        {


            Importer = importer;


            _uncConverter = new UncConverter2();


            RootFolderOrUnc2RootFolder = new StringDictionary();


            _remainingDriveLetters = new List<char>();


            for (int pos = 65; pos < 91; pos++)
            {
                _remainingDriveLetters.Add(Convert.ToChar(pos));
            }

            TemporaryMappedDrives = new List<NetworkDrive>();

            Location2Id = new Dictionary<string, int>();

            DriveLetters = new Dictionary<string, EnhancedDriveInfo>();
            RootFoldersOfDrive = new Dictionary<EnhancedDriveInfo, List<string>>();
            ItemsChainedByTagCache = new ItemsChainedByTagCache(this);


            if (Importer.ExcludeUnmatchedFiles)
            {
                UnmatchedFiles = new List<EnhancedFileInfo>();
            }



        }

        #endregion




        #region Nested type: ScanDriveDelegate

        internal delegate void ScanDriveDelegate(
            KeyValuePair<EnhancedDriveInfo, List<string>> driveRootFolders, ref FilesQueue files);

        #endregion




        #region Properties

        internal CORE.FileImporter Importer { get; private set; }

        internal int AddedFiles { get; set; }

        internal bool CancelUpdate { get; set; }

        internal int DeletedFiles { get; set; }

        internal ItemsChainedByTagCache ItemsChainedByTagCache { get; private set; }

        internal Dictionary<string, int> Location2Id { get; private set; }

        internal int UpdatedFiles { get; set; }

        internal List<EnhancedFileInfo> UnmatchedFiles { get; private set; }

        internal DateTime StartTime { get; set; }

        #endregion




        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);

            // GC.Collect();
            // GC.WaitForPendingFinalizers();
            GC.SuppressFinalize(this);
        }

        #endregion





        #region Member methods




        #region Public methods

        #endregion




        #region Internal methods




        internal static string WildstringToRegexPattern(string wildstring)
        {
            wildstring =
                wildstring.Replace("\"", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty).Replace(
                    "|", string.Empty);
            return Regex.Escape(wildstring).Replace("\\*", ".*").Replace("\\?", ".");
        }





        internal static Regex WildstringToRegex(string wildstring)
        {
            string tempString = wildstring;
            if (tempString.Contains(":"))
            {
                tempString = tempString.Substring(tempString.LastIndexOf(":", StringComparison.Ordinal) + 1);
            }

            if (tempString.Contains("\\"))
            {
                tempString = tempString.Substring(tempString.LastIndexOf("\\", StringComparison.Ordinal) + 1);
            }

            if (tempString.Contains("/"))
            {
                tempString = tempString.Substring(tempString.LastIndexOf("/", StringComparison.Ordinal) + 1);
            }

            tempString =
                tempString.Replace("\"", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty).Replace(
                    "|", string.Empty);
            tempString = "^" + Regex.Escape(tempString).Replace("\\*", ".*").Replace("\\?", ".") + "$";
            return new Regex(tempString, RegexOptions.IgnoreCase);
        }






        /// <exception cref="Exception"><c>Exception</c>.</exception>
        internal bool CheckProgress(int percentage, string text)
        {

            try
            {


                if (CancelUpdate
                    || (!Importer.ImportProgress.Progress(percentage, text)))
                {
                    Importer.Section
                        .CancelUpdate();


                    Importer.LogMessages.Enqueue
                        (new LogMessage("Debug", 
                        "Import cancelled by user."));



                    Importer.ImportProgress.Progress
                        (100, "Import cancelled by user.");


                    return false;
                }





            }
            catch (Exception ex)
            {


                Importer.LogMessages.Enqueue
                    (new LogMessage("Error", 
                        ex.ToString()));


                Importer.LogMessages
                    .FlushAllMessages();

                throw;
            }

            return true;
        }

        #endregion




        #region Protected methods




        protected virtual void Dispose
            (bool calledFromCode)
        {


            if (!calledFromCode)
                return;

            Importer = null;
            _uncConverter.Dispose();
            _uncConverter = null;
            RootFolderOrUnc2RootFolder.Clear();
            RootFolderOrUnc2RootFolder = null;
            _remainingDriveLetters.Clear();
            _remainingDriveLetters = null;
            TemporaryMappedDrives.Clear();
            TemporaryMappedDrives = null;
            Location2Id.Clear();
            Location2Id = null;
            RootFoldersOfDrive.Clear();
            RootFoldersOfDrive = null;
            DriveLetters.Clear();
            DriveLetters = null;
            Files = null;

            // Free other state (managed objects).

            // Free your own state (unmanaged objects).
            // Set large fields to null.
        }

        #endregion




        #region Private methods

        internal void RootFolders2MappedDrives()
        {




            foreach (string t in Importer.RootFolders)
            {


                string rootFolder = t;


                var uri = new Uri(rootFolder);


                if (uri.IsUnc)
                {
                    _uncConverter.Refresh();
                    string drive = _uncConverter.ConvertToDrive(rootFolder);


                    if (drive == rootFolder)
                    {

                        var networkDrive = new NetworkDrive();


                        try
                        {
                            networkDrive.Force = true;
                            networkDrive.ShareName = drive;
                            drive = NextFreeDriveLetter() + ":";
                            if (drive != " :")
                            {
                                networkDrive.LocalDrive = drive;
                                networkDrive.MapDrive();

                                _uncConverter.Refresh();
                                drive = _uncConverter.ConvertToDrive(rootFolder);

                                if (drive != rootFolder)
                                {
                                    TemporaryMappedDrives.Add(networkDrive);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Importer.LogMessages.Enqueue
                                (new LogMessage("Error", ex.ToString()));

                            Importer.LogMessages.FlushAllMessages();

                        }


                        //// NetworkDrive = null;
                    }



                    if (!RootFolderOrUnc2RootFolder.ContainsKey(rootFolder))
                    {
                        RootFolderOrUnc2RootFolder.Add(rootFolder, drive);
                    }


                }
                else
                {


                    if (!RootFolderOrUnc2RootFolder
                        .ContainsKey(rootFolder))
                    {


                        RootFolderOrUnc2RootFolder
                            .Add(rootFolder, rootFolder);


                    }


                }




            }



        }









        private char NextFreeDriveLetter()
        {


            foreach (string drive in Directory.GetLogicalDrives())
            {
                _remainingDriveLetters.Remove(drive[0]);
            }

            return (_remainingDriveLetters.Count > 0) 
                ? _remainingDriveLetters[0]
                : ' ';


        }


        internal bool Relocate(MLTag location, int pos)
        {

            if (Importer.RootFolders.Length > 1)
            {

                string oldRootFolder = string.Empty;
                
                
                foreach (string rootFolder in Importer.RootFolders)
                {
                    if (location.Values[pos].StartsWith(rootFolder))
                    {
                        oldRootFolder = rootFolder;
                    }
                }



                if (oldRootFolder.Equals(string.Empty))
                {
                    return false;
                }

                string filename
                    = location.Values[pos]
                    .Substring(oldRootFolder.Length);

                var possibleLocations = new List<string>();
               

                foreach (string rootFolder in Importer.RootFolders)
                {
                    if (!rootFolder.Equals(oldRootFolder))
                    {
                        possibleLocations.Add(rootFolder + filename);
                    }
                }


                foreach (string possibleLocation in possibleLocations)
                {
                    if (!File.Exists(possibleLocation)) 
                        continue;

                    string logMessage = "Found " + location.Values[pos] + " at new location: " + possibleLocation;
                    this.Importer.LogMessages.Enqueue(new LogMessage("Info", logMessage));
                    location.Values[pos] = possibleLocation;
                    this.RelocatedFiles.Add(possibleLocation);
                    return true;
                }
            }

            return false;
        }




        internal bool CanDelete(string location)
        {

            string rootFolder = string.Empty;


            foreach (string t in this.Importer.RootFolders)
            {

                if (location.ToLowerInvariant()
                    .StartsWith(t.ToLowerInvariant()))
                {

                    rootFolder = t;
                }

            }


            if (rootFolder.Length == 0)
            {

                var directoryName = Path.GetDirectoryName(location);
                if (directoryName != null)
                    rootFolder = directoryName.ToLowerInvariant();


            }


            return Directory.Exists(rootFolder)
                && !File.Exists(location);


        }



        internal void Callback(IAsyncResult ar)
        {
            var result = (AsyncResult)ar;


            var delegateScanDrive 
                = (ScanDriveDelegate)
                result.AsyncDelegate;
        
            
            try
            {

                delegateScanDrive
                    .EndInvoke(ref Files, ar);

            }
            catch (Exception ex)
            {
                Importer.LogMessages
                    .Enqueue(new LogMessage
                        ("Error", ex.ToString()));
            }


        }






        internal FileInfo NextFileInfo(string filename)
        {

            string fileRootFolder = string.Empty;
            
            foreach (string rootFolder in this.Importer.RootFolders)
            {
                if (filename.StartsWith(rootFolder))
                {
                    fileRootFolder = rootFolder + (rootFolder.EndsWith("\\") ? string.Empty : "\\");
                }
            }


            if (Importer.ExcludeFileMasks.Length > 0)
            {
                foreach (string fileMask in this.Importer.ExcludeFileMasks.Split(';'))
                {
                    string fileMaskPattern = fileMask.StartsWith("?regex? ")
                                                 ? fileMask.Remove(0, 8)
                                                 : WildstringToRegexPattern(fileMask);
                    if (
                        new Regex(
                            "^" + WildstringToRegexPattern(fileRootFolder) + fileMaskPattern + "$",
                            RegexOptions.IgnoreCase).IsMatch(filename))
                    {
                        return null;
                    }
                }
            }



            if (Importer.DeleteNonExistentFiles)
            {
                if (RelocatedFiles != null)
                {
                    if (RelocatedFiles.Count > 0)
                    {
                        if (RelocatedFiles.Contains(filename))
                        {
                            return null;
                        }
                    }
                }
            }



            var result = new FileInfo(filename);


            if (Importer.MinFileSize != 0)
            {
                if (result.Length / 1024 <= Importer.MinFileSize)
                {
                    return null;
                }
            }



            if (!Importer.IncludeHiddenFiles)
            {

                if (((result.Attributes
                      & FileAttributes.Hidden)
                     == FileAttributes.Hidden
                     ||
                     ((result.Attributes
                       & FileAttributes.System)
                      == FileAttributes.System)))
                {
                    return null;
                }

            }





            return result;
        }


        #endregion



        #endregion







    }
}