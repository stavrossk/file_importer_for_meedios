﻿using CustomProperties.PropertyData;

namespace FileImporter.ImporterProperties
{


    class AdsPropertiesGetter
    {




        internal static bool GetPropertyForAlternateDataStreamsOptions
            (int index, ref int i, OptionSettings prop)
        {





            if (index == i++)
            {
                prop.Name = "ImportFromADS";
                prop.Caption = "Import from ADS";
                prop.HelpText = "If set all tags will be imported from an alternate data stream.";
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupCaption = "Alternate data streams";
                prop.GroupName = "ADS";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ExportToADS";
                prop.Caption = "Export to ADS";
                prop.HelpText = "If set all tags will be exported to an alternate data stream.";
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupName = "ADS";
                return true;
            }

            return false;
        }
    }
}
