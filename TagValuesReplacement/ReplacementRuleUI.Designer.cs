﻿namespace FileImporter.TagValuesReplacement
{
    internal partial class ReplacementRuleUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.IContainer components;

        private System.Windows.Forms.ComboBox tagName1;
        private System.Windows.Forms.ComboBox tagName2;
        private System.Windows.Forms.ComboBox tagName3;
        private System.Windows.Forms.TextBox tagValue1;
        private System.Windows.Forms.TextBox tagValue2;
        private System.Windows.Forms.TextBox tagValue3;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tagName1 = new System.Windows.Forms.ComboBox();
            this.tagValue1 = new System.Windows.Forms.TextBox();
            this.tagName2 = new System.Windows.Forms.ComboBox();
            this.tagName3 = new System.Windows.Forms.ComboBox();
            this.tagValue3 = new System.Windows.Forms.TextBox();
            this.tagValue2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tagName1
            // 
            this.tagName1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tagName1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tagName1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.tagName1.FormattingEnabled = true;
            this.tagName1.Location = new System.Drawing.Point(3, 3);
            this.tagName1.Name = "tagName1";
            this.tagName1.Size = new System.Drawing.Size(121, 21);
            this.tagName1.TabIndex = 0;
            // 
            // tagValue1
            // 
            this.tagValue1.Location = new System.Drawing.Point(130, 3);
            this.tagValue1.Name = "tagValue1";
            this.tagValue1.Size = new System.Drawing.Size(121, 20);
            this.tagValue1.TabIndex = 1;
            // 
            // tagName2
            // 
            this.tagName2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.tagName2.FormattingEnabled = true;
            this.tagName2.Location = new System.Drawing.Point(257, 3);
            this.tagName2.Name = "tagName2";
            this.tagName2.Size = new System.Drawing.Size(121, 21);
            this.tagName2.TabIndex = 2;
            // 
            // tagName3
            // 
            this.tagName3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.tagName3.FormattingEnabled = true;
            this.tagName3.Location = new System.Drawing.Point(511, 3);
            this.tagName3.Name = "tagName3";
            this.tagName3.Size = new System.Drawing.Size(121, 21);
            this.tagName3.TabIndex = 4;
            // 
            // tagValue3
            // 
            this.tagValue3.Location = new System.Drawing.Point(638, 3);
            this.tagValue3.Name = "tagValue3";
            this.tagValue3.Size = new System.Drawing.Size(121, 20);
            this.tagValue3.TabIndex = 5;
            // 
            // tagValue2
            // 
            this.tagValue2.Location = new System.Drawing.Point(384, 3);
            this.tagValue2.Name = "tagValue2";
            this.tagValue2.Size = new System.Drawing.Size(121, 20);
            this.tagValue2.TabIndex = 3;
            // 
            // ReplacementRuleUI
            // 
            this.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tagName1);
            this.Controls.Add(this.tagValue1);
            this.Controls.Add(this.tagName2);
            this.Controls.Add(this.tagValue2);
            this.Controls.Add(this.tagName3);
            this.Controls.Add(this.tagValue3);
            this.Size = new System.Drawing.Size(856, 84);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
