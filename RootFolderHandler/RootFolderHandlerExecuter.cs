﻿using System;
using FileImporter.AlternateDataStreamImportExport;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;



namespace FileImporter.RootFolderHandler
{




    class RootFolderHandlerExecuter
    {


        /// <param name="rootFolderHandlerBase"> </param>
        /// <exception cref="Exception"><c>Exception</c>.</exception>
        internal static void Execute
            (RootFolderHandlerBase rootFolderHandlerBase)
        {


            try
            {


                //// EnhancedFileInfo PreviousFile = null;
                for (rootFolderHandlerBase.FilePos
                    = rootFolderHandlerBase.Files.Count - 1;
                    rootFolderHandlerBase.FilePos > -1;
                    rootFolderHandlerBase.FilePos--)
                {


                    if (rootFolderHandlerBase.FilePos == -1)
                    {

                        throw new Exception
                            ("CancelUpdateThreadError");

                    }


                    rootFolderHandlerBase.File
                        = rootFolderHandlerBase
                        .Files[rootFolderHandlerBase.FilePos];


                    rootFolderHandlerBase.LogMessages.Enqueue(
                        new LogMessage
                            ("Info",
                             "Getting info for file: " 
                             + rootFolderHandlerBase.File
                                   .FileInformation
                                   .FullName));
                 
                    
                    
                    if (rootFolderHandlerBase.Files.Count > 0)
                    {
                        int percentage = 100 - ((rootFolderHandlerBase.FilePos * 100) / rootFolderHandlerBase.Files.Count);
                        string text = "Getting info for file: " + rootFolderHandlerBase.File.FileInformation.FullName;
                        if (!rootFolderHandlerBase.Import.CheckProgress(percentage, text))
                        {
                            rootFolderHandlerBase.Import.CancelUpdate = true;
                            return;
                        }
                    }


                    rootFolderHandlerBase.File.Item 
                        = rootFolderHandlerBase
                        .FindItemByLocationContaining
                        (rootFolderHandlerBase.File
                        .FileInformation.FullName) ??
                        new MLItem(null);


                    rootFolderHandlerBase.IsNewItem
                        = rootFolderHandlerBase.File.Item.ID == 0;


                    rootFolderHandlerBase.Update 
                        = (!rootFolderHandlerBase.IsNewItem) &&
                        (rootFolderHandlerBase.Importer.AlwaysUpdate ||
                        (rootFolderHandlerBase.File.Item.DateChanged
                        < rootFolderHandlerBase.File.FileInformation.LastWriteTime));



                    if (rootFolderHandlerBase.IsNewItem)
                    {
                        rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Info", "  Created new item"));
                    }
                    else
                    {
                        rootFolderHandlerBase.LogMessages.Enqueue(rootFolderHandlerBase.Update
                                                                      ? new LogMessage("Info", "  Updating already existing item")
                                                                      : new LogMessage("Info", "  Skiping already existing item"));
                    }




                    if (rootFolderHandlerBase.Update
                        || rootFolderHandlerBase.Import.Importer.AlwaysUpdateImages
                        || rootFolderHandlerBase.IsNewItem)
                    {


                        rootFolderHandlerBase.DoSpecificHandling();
                        
                        
                        if (rootFolderHandlerBase.Importer.ExportToAlternateDataStream)
                        {
                            var adsWriter = new AlternateDataStreamWriter(rootFolderHandlerBase.File);
                            adsWriter.WriteItem();
                        }

                        if (rootFolderHandlerBase.Importer.ExcludeUnmatchedFiles)
                        {
////                            if ((this.Importer.TagMasks.Count > 0) && (this.File.Item.Location.Length > 0) &&
////                                (this.File.Item.Tags.Count == 0))
                            if (!rootFolderHandlerBase.File.SuccessfulTagMaskMatch)
                            {
                                rootFolderHandlerBase.Import.UnmatchedFiles.Add(rootFolderHandlerBase.File);
                                rootFolderHandlerBase.Files.Remove(rootFolderHandlerBase.File);
                            }
                        }

                        /*
                        if (Importer.ChainingOption == ChainOption.ChainByTags)
                        {
                            if ((!IsNewItem) && (!Update))
                            {
                                Files.Remove(File);
                            }
                            else
                            {
                                EnhancedFileInfo TempFile = new EnhancedFileInfo(File.FI);
                                HandleUnchainedOrFirstFile(TempFile);
                                IMLItem CachedItem = Import.itemsChainedByTagCache.Query(TempFile.Item);
                                if (CachedItem != TempFile.Item)
                                {
                                    File = Import.Item2File.Query(CachedItem);
                                    HandleChainedByTagFile(File, TempFile);
                                    if (Import.Item2File.RetrivedItemWasCached)
                                    {
                                        Files.RemoveAt(FilePos);
                                    }
                                    else
                                    {
                                        Files[FilePos] = File;
                                    }
                                }
                                else
                                {
                                    Files[FilePos] = TempFile;
                                    Import.Item2File.Add(TempFile);
                                }
                            }
                        }
                        else
                        {
                            if ((!Importer.ChainFiles) || (PreviousFile == null) || (File.FI.DirectoryName != PreviousFile.FI.DirectoryName))
                            {
                                HandleUnchainedOrFirstFile(File);
                                PreviousFile = File;
                            }
                            else
                            {
                                HandleChainedFile(File, PreviousFile);
                                Files.Remove(File);
                            }
                        }
*/
                    }
                    else
                    {
                        rootFolderHandlerBase.Files.Remove(rootFolderHandlerBase.File);
                    }

                    rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Info", "...Done"));
                    rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Info", Environment.NewLine));
                }
            }
            catch (Exception ex)
            {
                rootFolderHandlerBase.LogMessages.Enqueue(new LogMessage("Error", ex.ToString()));
                throw;
            }


        }


    }



}
