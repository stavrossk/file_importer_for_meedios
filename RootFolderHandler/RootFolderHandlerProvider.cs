using FileImporter.CORE;

namespace FileImporter.RootFolderHandler
{
    using System.Collections.Generic;

    internal class RootFolderHandlerProvider
    {
        #region Methods

        internal static RootFolderHandlerBase ProvideRootFolderHandlerInstance(
            TheImport import,
            EnhancedDriveInfo infoOfDrive,
            string rootFolder,
            List<EnhancedFileInfo> foundFiles,
            ImageCache cachedImages)
        {
            if (import.Importer.ChainingOption == ChainOption.ChainByTags)
            {
                return new ChainByTagsRootFolderHandler(import, infoOfDrive, rootFolder, foundFiles, cachedImages);
            }
            else if (import.Importer.ChainingOption == ChainOption.ChainByFolder)
            {
                return new ChainByFolderRootFolderHandler(import, infoOfDrive, rootFolder, foundFiles, cachedImages);
            }
            else
            {
                return new DefaultRootFolderHandler(import, infoOfDrive, rootFolder, foundFiles, cachedImages);
            }
        }

        #endregion
    }
}