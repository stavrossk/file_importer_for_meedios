using System.Globalization;




namespace FileImporter.RootFolderHandler
{



    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using MCW.Utility.Logging;
    using MeediOS;







    internal abstract class RootFolderHandlerBase
    {



        #region Constants and Fields

        private readonly TheImport _import;

        private readonly EnhancedDriveInfo _infoOfDrive;

        private readonly List<EnhancedFileInfo> _files;

        private readonly ImageCache _images;

        #endregion




        #region Constructors and Destructors

        protected RootFolderHandlerBase(
            TheImport import,
            EnhancedDriveInfo infoOfDrive,
            string rootFolder,
            List<EnhancedFileInfo> foundFiles,
            ImageCache cachedImages)
        {
            _import = import;
            _infoOfDrive = infoOfDrive;
            _files = foundFiles;
            RootFolder = rootFolder;
            _images = cachedImages;
        }

        #endregion




        #region Properties




        internal EnhancedDriveInfo InfoOfDrive
        {


            get
            {
                return _infoOfDrive;
            }


        }




        internal List<EnhancedFileInfo> Files
        {
            get
            {
                return _files;
            }
        }



        internal ImageCache Images
        {
            get
            {
                return _images;
            }
        }



        internal TheImport Import
        {
            get
            {
                return _import;
            }
        }



        internal CORE.FileImporter Importer
        {
            get
            {
                return Import.Importer;
            }
        }



        internal LogMessagesQueue LogMessages
        {
            get
            {
                return Importer.LogMessages;
            }
        }



        internal string RootFolder
        { get; set; }

        protected internal
            EnhancedFileInfo File
        { get; set; }

        protected internal bool IsNewItem 
        { get; set; }

        protected internal bool Update 
        { get; set; }

        protected internal int FilePos
        { get; set; }

        #endregion





        #region Methods





        internal abstract void DoSpecificHandling();


        protected internal static string PatternFixEscapeChars(string pattern)
        {

            string result = string.Empty;

            var replacements = new StringDictionary
                {
                    { @"\", @"\\" },
                    { @".", @"\." },
                    { @"+", @"\+" },
                    { @"(", @"\(" },
                    { @"[", @"\[" },
                    { @"]", @"\]" },
                    { @"{", @"\{" },
                    { @"}", @"\}" }
                };



            foreach (char character in pattern)
            {

                if (!replacements.ContainsKey
                    (character.ToString
                    (CultureInfo.InvariantCulture)))
                {
                    result += character;
                }
                else
                {

                    result += replacements
                        [character.ToString
                        (CultureInfo.InvariantCulture)];


                }


            }

            return result;
        }



        protected internal IMLItem  FindItemByLocationContaining(string filename)
        {
            try
            {
                if (!Import.Location2Id.Keys.Contains
                    (filename.ToLowerInvariant()))
                    return null;


                int id = Import.Location2Id
                    [filename.ToLowerInvariant()];

                lock (Import)
                {
                    return this.Importer
                        .Section.FindItemByID(id);
                }


            }
            catch (Exception ex)
            {

                LogMessages.Enqueue
                    (new LogMessage
                        ("Error",
                        ex.ToString()));

                return null;
            }


        }

        #endregion





    }




}