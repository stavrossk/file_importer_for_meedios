﻿using System.Globalization;
using FileImporter.CORE;




namespace FileImporter
{




    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading;
    using MCW.Utility.Logging;
    using MCW.Utility.MeediOS;
    using MeediOS;


    /// <summary>
    /// Queues EnhancedFileInfo objects for batch handling. 
    /// Once signaled all queued items get dequeued and the contained IMLItem added to the import's section
    /// </summary>
    internal class FilesQueue : IDisposable
    {



        /// <summary>
        /// The queue of EnhancedFileInfo objects 
        /// </summary>
        internal Queue<EnhancedFileInfo> _files;

        /// <summary>
        /// The import that uses this FilesQueue
        /// </summary>
        internal TheImport _import;

        /// <summary>
        /// Counts the files handled 
        /// </summary>
        internal int _totalFiles;

        /// <summary>
        /// The Event to be signaled if the worker thread should continue dequeing and handling items in the queue
        /// </summary>
        internal AutoResetEvent _waitHandle;

        /// <summary>
        /// The worker thread responsible for dequeing of and handling of the items in the queue
        /// </summary>
        private Thread _worker;

        /// <summary>
        /// Initializes a new instance of the FilesQueue class
        /// </summary>
        /// <param name="import">The import using this FilesQueue</param>
        public FilesQueue(TheImport import)
        {
            _import = import;
            _files = new Queue<EnhancedFileInfo>();
            _waitHandle = new AutoResetEvent(false);
            _worker = new Thread(() => SingleFileImportWorker.WorkerPerformSingleFileAndFileInfoImport(this)) { Name = "File Queue Thread" };
            _worker.Start();
        }







        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.Collect();
            GC.SuppressFinalize(this);
        }

        #endregion







        /// <summary>
        /// Signals the worker thread that it can start dequeing items
        /// </summary>
        public void StartDequeueing()
        {
            if (_import.Importer.ChainingOption
                == ChainOption.ChainByTags)
            {
                _waitHandle.Set();
            }
        }







        /// <summary>
        /// Adds an EnhancedFileInfo object to the FilesQueue
        /// </summary>
        /// <param name="file">The EnhancedFileInfo object to be added</param>
        public void Enqueue(EnhancedFileInfo file)
        {


            lock (((ICollection)_files).SyncRoot)
            {
                _files.Enqueue(file);
            }

            _totalFiles++;


            if ((_import.Importer.ChainingOption 
                != ChainOption.ChainByTags) || (file == null))
            {
                _waitHandle.Set();
            }



        }








        /// <summary>
        /// Adds an array of EnhancedFileInfo objects to the FilesQueue
        /// </summary>
        /// <param name="files">The array of EnhancedFileInfo objects to be added</param>
        public void Enqueue(EnhancedFileInfo[] files)
        {



            if (files.Length <= 0) return;


            lock (((ICollection)_files).SyncRoot)
            {
                foreach (EnhancedFileInfo t in files)
                {
                    _files.Enqueue(t);
                }

                _totalFiles += files.Length;
            }



        }





        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        /// <param name="calledFromCode">false if called by GC true if called from code</param>
        protected virtual void Dispose(bool calledFromCode)
        {

            if (!calledFromCode)
                return;


            Enqueue(null as EnhancedFileInfo); // Signal the consumer to exit.
            _worker.Join(); // Wait for the consumer's thread to finish.
            _waitHandle.Close(); // Release any OS resources.
            _files = null;
            _import = null;
            _worker = null;
            _waitHandle = null;

            // Free other state (managed objects).

            // Free your own state (unmanaged objects).
            // Set large fields to null.
        }
    }





}