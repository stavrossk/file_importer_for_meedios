﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileImporter.RootFolderHandler;
using MCW.Utility.Logging;



namespace FileImporter.Directory_Scanner
{



    class DirectoryScanner
    {




        private static void RecurseFolders
            (TheImport theImport, string folder, 
            List<string> foundFiles)
        {
            try
            {
                foreach (string includeFileMask in theImport.Importer.IncludeFileMasks.Split(';'))
                {
                    foundFiles.AddRange(Directory.GetFiles(folder, includeFileMask, SearchOption.TopDirectoryOnly));
                }
            }
            catch (Exception ex)
            {
                string logMessage = "Unable to get files in " + folder + Environment.NewLine + ex;
                theImport.Importer.LogMessages.Enqueue(new LogMessage("Error", logMessage));
            }

            try
            {
                foreach (string dir in Directory.GetDirectories(folder, "*", SearchOption.TopDirectoryOnly))
                {
                    RecurseFolders(theImport, dir, foundFiles);
                }
            }
            catch (Exception ex)
            {
                string logMessage = "Unable to get subfolders of " + folder + Environment.NewLine + ex;
                theImport.Importer.LogMessages.Enqueue(new LogMessage("Error", logMessage));
            }
        }




        private static IEnumerable
            <EnhancedFileInfo>
            FindFiles
            (TheImport theImport,
             string rootFolder)
        {



            var filenames = new List<string>();


            var foundFiles
                = new List<EnhancedFileInfo>();



            RecurseFolders
                (theImport, rootFolder, filenames);



            FileInfo nextFile = null;


            foreach (string filename in filenames.Distinct())
            {



                try
                {
                    nextFile = theImport.NextFileInfo(filename);
                }
                catch (Exception ex)
                {
                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Warn", "Unable to create FileInfo object for: " + filename, ex));
                }

                if (nextFile != null)
                {
                    foundFiles.Add(new EnhancedFileInfo(nextFile));
                }



            }




            return foundFiles;
        }







        /// <exception cref="Exception"><c>Exception</c>.</exception>
        internal static void ScanDrive
            (TheImport theImport,
             KeyValuePair<EnhancedDriveInfo, 
                 List<string>> driveRootFolders, 
             ref FilesQueue filesQueue)
        {



            var allFoundFiles = new List<EnhancedFileInfo>();
          
            var foundFiles = new List<EnhancedFileInfo>();



            try
            {


                ImageCache cachedImages = new ImageCache();




                foreach (string rootFolder in driveRootFolders.Value)
                {


                    if (Directory.Exists(rootFolder))
                    {


                        foundFiles.Clear();

                        foundFiles.AddRange(FindFiles(theImport, rootFolder));

                        RootFolderHandlerBase rootFolderHandler =
                            RootFolderHandlerProvider.ProvideRootFolderHandlerInstance(
                                theImport, driveRootFolders.Key, rootFolder, foundFiles, cachedImages);


                        RootFolderHandlerExecuter.Execute(rootFolderHandler);

                        allFoundFiles.AddRange(foundFiles);


                    }
                    else
                    {
                        theImport.Importer.LogMessages.Enqueue(
                            new LogMessage("Warn", "RootFolder doesn't exist: " + rootFolder));
                    }


                }

                EnhancedFileInfo[] foundFilesArray
                    = allFoundFiles.ToArray();

                filesQueue
                    .Enqueue
                    (foundFilesArray);


            }
            catch (Exception ex)
            {
                theImport.Importer.LogMessages.Enqueue(new LogMessage("Error", ex.ToString()));
                throw;
            }
            finally
            {
                allFoundFiles.Clear();
                foundFiles.Clear();
            }

            theImport.Importer.LogMessages.Enqueue
                (new LogMessage("Debug", 
                                "Return from ScanDrive"));
     
        
        
        }
    }
}
