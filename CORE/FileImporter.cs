﻿using FileImporter.ImporterProperties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using log4net;
using MCW.Utility.Logging;
using MCW.Utility.MeediOS;
using MCW.Utility.MeediOS.GlobalPluginProperties;
using MeediOS;
using MeediOS.Plugin;
using FileImporter.TagValuesReplacement;





namespace FileImporter.CORE
{





    internal enum ChainOption
    {
        DontChain,
        ChainByFolder,
        ChainByTags
    }





    public class FileImporter 
        : IMLImportPlugin,
        IMLImportPluginExtension
    {



        #region Constants and Fields

        internal bool alwaysUpdateImages;

        //// internal bool FollowShortcuts;

        internal GlobalPluginProperties GlobalPluginProperties;

        internal ImporterPropertiesCore.FileImporterProperties ImporterSettings;

        internal TheImport ImportFunc;

        internal bool ImportImages;

        internal bool Initialized;

        internal MCW.Utility.MeediOS.Locations.MediaLocations MediaProfilesProvider;

        private string[] _movieFolders;

        private readonly string[] musicFolders;

        private string[] _picturesFolders;

        private string[] _tvShowsFolders;

        private string[] _movieProfiles;

        private string[] _musicProfiles;

        private string[] _picturesProfiles;

        private string[] _tvshowsProfiles;

        internal FileInfo LogConfig;

        public FileImporter(string[] musicFolders)
        {
            this.musicFolders = musicFolders;
        }

        #endregion




        #region Properties

        public bool IsSetPropertiesForImport { get; set; }

        internal bool AlwaysUpdate { get; set; }

        internal bool AlwaysUpdateImages
        {
            get
            {
                return alwaysUpdateImages;
            }

            set
            {
                alwaysUpdateImages = value;
            }
        }

        internal bool ChainFiles { get; set; }

        internal ChainOption ChainingOption { get; set; }

        internal bool CreationDate { get; set; }

        internal string CreationDateTagName { get; set; }

        internal string DataPath { get; set; }

        internal string DebugLevel { get; set; }

        internal bool DeleteNonExistentFiles { get; set; }

        internal string ExcludeFileMasks { get; set; }

        internal bool ExcludeUnmatchedFiles { get; set; }

        internal bool ExportToAlternateDataStream { get; set; }

        internal string FileImporterDataPath { get; set; }

        internal bool FileSize { get; set; }

        internal string FileSizeTagName { get; set; }

        internal ImageLinker ImageLinker { get; set; }

        internal bool ImportFromAlternateDataStream { get; set; }

        internal IMLImportProgress ImportProgress { get; private set; }

        internal IPluginProvider ImportProvider { get; set; }

        internal string IncludeFileMasks { get; set; }

        internal bool IncludeHiddenFiles { get; set; }

        internal bool IsFirstLoad { get; set; }

        internal bool LastAccessDate { get; set; }

        internal string LastAccessDateTagName { get; set; }

        internal bool LastModificationDate { get; set; }

        internal string LastModificationDateTagName { get; set; }

        internal ILog Log { get; set; }

        internal LogMessagesQueue LogMessages { get; set; }





        internal string[] MovieFolders
        {
            get
            {
                if (_movieFolders == null)
                {

                    MessageBox.Show(MovieProfile);
                    _movieFolders = MediaProfilesProvider
                        .GetFolders("movies", MovieProfile);
                }

                return _movieFolders;
            }
        }





        internal string MovieProfile { get; set; }





        internal string[] MovieProfiles
        {

            get {
                return _movieProfiles ??
                       (_movieProfiles 
                       = MediaProfilesProvider
                       .GetProfilesForMediaType("movies"));
            }


        }





        internal int MinFileSize { get; set; }






        internal string[] MusicFolders
        {
            get
            {
                if (this.musicFolders == null)
                {
                    //MusicProfile = string.Empty;
                    //this.musicFolders = this.mediaProfilesProvider.GetFolders("music", MusicProfile);
                }

                return this.musicFolders;
            }
        }

        internal string MusicProfile { get; set; }

        internal string[] MusicProfiles
        {
            get {
                return _musicProfiles ??
                       (_musicProfiles = MediaProfilesProvider.GetProfilesForMediaType("music"));
            }
        }

        internal string[] PicturesFolders
        {


            get 
            {

                return _picturesFolders ??
                       (_picturesFolders = MediaProfilesProvider
                       .GetFolders("pictures",
                       this.PicturesProfile));

            }

        }





        internal string PicturesProfile { get; set; }






        internal string[] PicturesProfiles
        {


            get
            {


                if (_picturesProfiles == null)
                {
                    _picturesProfiles = MediaProfilesProvider
                        .GetProfilesForMediaType("pictures");
                }

                return _picturesProfiles;


            }



        }




        internal string[] RootFolders { get; set; }




        internal IMLSection Section { get; private set; }




        internal bool SerialNumber { get; set; }





        internal string SerialNumberTagName { get; set; }





        internal string SingleFilename { get; set; }





        internal IList<TagMask> TagMasks { get; set; }






        internal string[] TagMasksStrings { get; set; }





        internal string[] TagsToChainBy { get; set; }






        internal ReplacementRules TagValuesReplacementRules { get; set; }






        internal string[] TVShowsFolders
        {
            get {
                return _tvShowsFolders ??
                       (_tvShowsFolders = MediaProfilesProvider
                       .GetFolders("tvshows", TVShowsProfile));
            }
        }





        internal string TVShowsProfile { get; set; }
        




        internal string[] TVShowsProfiles
        {
            get
            {
                if (_tvshowsProfiles == null)
                {
                    _tvshowsProfiles = MediaProfilesProvider.GetProfilesForMediaType("tvshows");
                }

                return this._tvshowsProfiles;
            }
        }

        internal bool VolumeName { get; set; }

        internal string VolumeNameTagName { get; set; }

        #endregion







        #region Implemented Interfaces




        #region IBasePlugin




        public bool EditCustomProperty(IntPtr window, string propertyName, ref string value)
        {
            if (propertyName == "ImporterSettings")
            {
                ImporterSettings.Caption = "Settings: ".Translate();
                ImporterSettings.Values = value;
                if (ImporterSettings.ShowGUI(null))
                {
                    value = ImporterSettings.Values;
                    return true;
                }


                return false;
            }

            return true;
        }





        public bool GetProperty(int index, IMeedioPluginProperty prop)
        {
            try
            {
                int i = 1;
                Initializer.Initialize(this);

                if (index == i++)
                {
                    prop.Name = "ImporterSettings";
                    prop.Caption = "Importer settings".Translate();
                    prop.HelpText = "Click to open the settings dialog.".Translate();
                    prop.DataType = "custom";
                    prop.DefaultValue = this.ImporterSettings.Default;
                    prop.CanTypeChoices = false;
                    return true;
                }
                else if (index == i++)
                {
                    prop.Name = "RootFolders";
                    prop.Caption = "Root folders".Translate();
                    prop.HelpText =
                        ("Ignore this property. It is just here so other plugins can programatically set it." +
                         Environment.NewLine +
                         "Click on the button for the 'Importer settings' property to setup this import.").Translate();
                    prop.GroupCaption = "Support for other plugins".Translate();
                    prop.DataType = "folderlist";
                    return true;
                }
                else if (index == i)
                {
                    prop.Name = "SingleFilename";
                    prop.Caption = "Single filename".Translate();
                    prop.GroupCaption = "Support for other plugins".Translate();
                    prop.HelpText =
                        ("Ignore this property. It is just here so other plugins can programatically set it." +
                         Environment.NewLine +
                         "Click on the button for the 'Importer settings' property to setup this import." +
                         Environment.NewLine + "If set only the file specified gets imported.").Translate();
                    prop.DataType = "string";
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.LogMessages.Enqueue(new LogMessage("Error", ex.Message, ex));
                this.LogMessages.FlushAllMessages();
            }

            return false;
        }




        public bool SetProperties
            (IMeedioItem properties, out string errorText)
        {




            errorText = string.Empty;
            if (!this.Initialized)
            {
                errorText = "FileImporter didn't initialize correctly." + Environment.NewLine +
                            "The error should have been logged to the SystemLog." + Environment.NewLine +
                            "If you want this error fixed, please send me the SystemLog via pm or mail.";
                return false;
            }

            try
            {


                PluginPropertiesSetter.SetPluginProperties(this, properties);



            }
            catch (Exception ex)
            {
                errorText = ex.ToString();
                return false;
            }

            return true;
        }



        #endregion








        #region IMLImportPlugin

        /// <exception cref="FileImporterInitializationException"><c>FileImporterInitializationException</c>.</exception>
        public bool Import(IMLSection section, IMLImportProgress progress)
        {
            CoreEngineHelpers.Snapshot();
            try
            {
                if (!Initialized)
                {
                    throw new FileImporterInitializationException();
                }

                Section = section;
                ImportProgress = progress;
                using (ImportFunc = new TheImport(this))
                {
                    ImportCore.Run(this.ImportFunc);
                }
            }
            catch (FileImporterInitializationException ex)
            {
                MessageBox.Show(ex.Message);
            }

            CoreEngineHelpers.Snapshot();
            return true;
        }

        #endregion




        #region IMLImportPluginExtension

        public void SetExtensionProperties
            (IPluginProvider importProvider,
            IMLSection section, bool isFirstLoad)
        {
            ImportProvider = importProvider;
            Section = section;
            IsFirstLoad = isFirstLoad;
        }

        #endregion





        #endregion

        internal class FileImporterInitializationException : Exception
        {


            #region Constructors and Destructors

            public FileImporterInitializationException()
                : base("Error initializing FileImporter")
            {
            }

            public FileImporterInitializationException(Exception ex)
                : base("Error initializing FileImporter", ex)
            {
            }

            #endregion



        }






    }






}