﻿using System;
using CustomProperties.PropertyData;






namespace FileImporter.ImporterProperties
{
    class FileInclusionPropertiesGetter
    {
        internal static bool GetPropertyForFileInclusion
            (int index, ref int i, OptionSettings prop)
        {





            if (index == i++)
            {



                prop.Name = "IncludeFileMasks";
                prop.Caption = "File masks to include";
                prop.HelpText = "Enter any number of file masks separated by semicolons" + Environment.NewLine +
                                "For example: \"*.avi;*.bmp\" (without the quotes)" + Environment.NewLine +
                                "If left blank, all files will be included (same as \"*.*\")" + Environment.NewLine +
                                "(The files of course need to match the other criteria.)";
                prop.DataType = "string";
                prop.GroupCaption = "File inclusion/exclusion rules";
                prop.GroupName = "FileInEx";
                prop.CanTypeChoices = true;


                prop.Choices2[ImporterPropertiesCore
                    .FileImporterProperties.VideoExtensions] 
                    = "Video extensions".Translate();

                prop.Choices2[ImporterPropertiesCore
                    .FileImporterProperties.AudioExtensions] 
                    = "Audio extensions".Translate();

                prop.Choices2[ImporterPropertiesCore
                    .FileImporterProperties.TextExtensions]
                    = "Text extensions".Translate();

                prop.Choices2[ImporterPropertiesCore
                    .FileImporterProperties.ImageExtensions]
                    = "Image extensions".Translate();





                prop.DefaultValue = String.Empty;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ExcludeFileMasks";
                prop.Caption = "File masks to exclude";
                prop.HelpText =
                    "Enter any number of masks separated by semicolons. If left blank, no files will be excluded." +
                    Environment.NewLine + "For example: \"*.avi;*.ogm\" (without the quotes)" + Environment.NewLine +
                    "The mask is prefixed internally with the files's rootfolder for you." + Environment.NewLine +
                    "So the above example would exclude all the avi and ogm files in the rootfolder, " +
                    Environment.NewLine + "but it will not exclude them in a any subfolders." + Environment.NewLine +
                    "You have to match the subfolder in the mask if you want to exclude files in a subfolder." +
                    Environment.NewLine +
                    "For example: \"trailer\\*.*\" (without the quotes) will exclude all files in the trailer subfolder.";
                prop.DataType = "string";
                prop.GroupName = "FileInEx";
                prop.DefaultValue = String.Empty;
                return true;
            }

            if (index == i++)
            {
                prop.Name = "IncludeHiddenFiles";
                prop.Caption = "Import hidden files";
                prop.HelpText = "If set hidden files will be imported";
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupName = "FileInEx";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "MinFileSize";
                prop.Caption = "Minimum filesize (in KB)";
                prop.HelpText = "Only files at least the size specified (in KB) get imported";
                prop.DataType = "int";
                prop.DefaultValue = 0;
                prop.GroupName = "FileInEx";
                return true;
            }

            /*
                    if(index == i++)
                    {
                        prop.Name           = "FollowShortcuts";
                        prop.Caption        = "Follow shortcuts";
                        prop.HelpText       = "If set, it will use the file targeted by the shortcut";
                        prop.DataType       = "bool";
                        prop.DefaultValue   = false;
                        prop.GroupName      = "FileInEx";
                        return true;
                    }
    */
            if (index == i++)
            {
                prop.Name = "ExcludeUnmatchedFiles";
                prop.Caption = "Exclude files not matched";
                prop.HelpText =
                    "When checked files that couldn't be matched with the tag masks will not be imported." +
                    Environment.NewLine + "This way you can prevent having invalid items in your library." +
                    Environment.NewLine +
                    "All the files not imported will be listed in the log. (if 'Debug'set to INFO or higher level)";
                prop.DataType = "bool";
                prop.DefaultValue = true;
                prop.GroupName = "FileInEx";
                return true;
            }

            return false;
        }



    }




}
