namespace FileImporter.TagValuesReplacement
{
    using System;
    using System.Text.RegularExpressions;
    using MCW.Utility.MeediOS;
    using MeediOS;

    /// <summary>
    /// 
    /// </summary>
    internal class ReplacementRule
    {
        /// <summary>
        /// Regex to verify that a string representation of a rule is valid
        /// </summary>
        private static readonly Regex ruleVerifierParser = new Regex(@"^""?<(?<TagName1>.*)>=(?<TagValue1>.*) \(<-\?->\) <(?<TagName2>.*)>=(?<TagValue2>.*) \(<-\:->\) <(?<TagName3>.*)>=(?<TagValue3>.*?)""?$|^""""$|^$");

        /// <summary>
        /// String representation of the rule
        /// </summary>
        private readonly string ruleString = "<name>= (<-?->) <>= (<-:->) <>=";

        /// <summary>
        /// Name of the tag evaluated
        /// </summary>
        private string tagName1 = "<name>";

        /// <summary>
        /// Name of the tag in the true branch
        /// </summary>
        private string tagName2 = "<>";

        /// <summary>
        /// Name of the tag in the false branch
        /// </summary>
        private string tagName3 = "<>";

        /// <summary>
        /// Value of the evaluated tag
        /// </summary>
        private string tagValue1 = string.Empty;

        /// <summary>
        /// Value assigned to tagName2 if tagName1 equals tagValue1
        /// </summary>
        private string tagValue2 = string.Empty;

        /// <summary>
        /// Value assigned to tagName3 if tagName1 equals tagValue1
        /// </summary>
        private string tagValue3 = string.Empty;

        /// <summary>
        /// Initializes a new instance of the ReplacementRule class using the default values
        /// </summary>
        public ReplacementRule()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ReplacementRule class
        /// </summary>
        /// <param name="ruleString">String representation of the rule used to initialize it</param>
        /// <exception cref="ArgumentException">ruleString is not assigned or String.Empty.</exception>
        public ReplacementRule(string ruleString)
        {
            this.ruleString = ruleString;
            if (String.IsNullOrEmpty(ruleString))
            {
                throw new ArgumentException("ruleString is not assigned or String.Empty.");
            }
            else
            {
                Match match = ruleVerifierParser.Match(this.ruleString);
                this.TagName1 = match.Groups["TagName1"].Value;
                this.TagName2 = match.Groups["TagName2"].Value;
                this.TagName3 = match.Groups["TagName3"].Value;
                this.TagValue1 = match.Groups["TagValue1"].Value;
                this.TagValue2 = match.Groups["TagValue2"].Value;
                this.TagValue3 = match.Groups["TagValue3"].Value;
            }
        }

        /// <summary>
        /// Gets the Regex object used to verify that a string representation of a rule is valid
        /// </summary>
        public static Regex RuleVerifierParser
        {
            get
            {
                return ruleVerifierParser;
            }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public string TagName1
        {
            get { return this.tagName1; }
            set { this.tagName1 = value; }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public string TagName2
        {
            get { return this.tagName2; }
            set { this.tagName2 = value; }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public string TagName3
        {
            get { return this.tagName3; }
            set { this.tagName3 = value; }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public string TagValue1
        {
            get { return this.tagValue1; }
            set { this.tagValue1 = value; }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public string TagValue2
        {
            get { return this.tagValue2; }
            set { this.tagValue2 = value; }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public string TagValue3
        {
            get { return this.tagValue3; }
            set { this.tagValue3 = value; }
        }

        /// <summary>
        /// Creates a rule that 
        /// </summary>
        /// <returns></returns>
        public static ReplacementRule DefaultRule()
        {
            return new ReplacementRule("<name>= (<-?->) <>= (<-:->) <>=");
        }

        /// <summary>
        /// Creates a string representation of the replacement rule
        /// </summary>
        /// <returns></returns>
        internal string Serialize()
        {
            string result = string.Empty;
            result += "<" + this.TagName1 + ">" + "=" + this.TagValue1 + " (<-?->) " +
                      "<" + this.TagName2 + ">" + "=" + this.TagValue2 + " (<-:->) " +
                      "<" + this.TagName3 + ">" + "=" + this.TagValue3;
            return result;
        }

        /// <summary>
        /// Applies the replacement rule to the item parameter
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        internal bool ApplyRuleToItem(IMLItem item)
        {
            if (item.Tags.Contains(this.TagName1))
            {
                object tagName1Value = item.Tags[this.TagName1];
                if (tagName1Value is string)
                {
                    string evaluatedTagValue1 = MLItem.EvaluateTagValues(this.TagValue1, item);
                    if (((string)item.Tags[this.TagName1]).Equals(evaluatedTagValue1))
                    {
                        if (this.TagName2.Length > 0)
                        {
                            string evaluatedTagValue2 = MLItem.EvaluateTagValues(this.TagValue2, item);
                            item.Tags[this.TagName2] = evaluatedTagValue2;
                            return true;
                        }
                    }
                    else
                    {
                        if (this.TagName3.Length > 0)
                        {
                            string evaluatedTagValue3 = MLItem.EvaluateTagValues(this.TagValue3, item);
                            item.Tags[this.TagName3] = evaluatedTagValue3;
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}