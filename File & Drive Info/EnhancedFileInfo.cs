﻿namespace FileImporter
{
    using System;
    using System.IO;
    using MCW.Utility.MeediOS;
    using MeediOS;

    /// <summary>
    /// Combines a FileInfo and MLItem object.
    /// </summary>
    public class EnhancedFileInfo : IDisposable
    {
        /// <summary>
        /// The FileInformation object of the file
        /// </summary>
        private FileInfo fileInformation;

        /// <summary>
        /// Initializes a new instance of the EnhancedFileInfo class
        /// </summary>
        /// <param name="fileInformation">FileInfo Object that gets a MLItem connected to it</param>
        public EnhancedFileInfo(FileInfo fileInformation)
        {
            this.fileInformation = fileInformation;
            this.Item = new MLItem(null);
        }

        /// <summary>
        /// Gets or sets the FileInfo object
        /// </summary>
        public FileInfo FileInformation
        {
            get { return this.fileInformation; }
            set { this.fileInformation = value; }
        }

        /// <summary>
        /// Gets or sets a IMLItem object to hold info for the file
        /// </summary>
        public IMLItem Item { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool SuccessfulTagMaskMatch { get; set; }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        #endregion

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        /// <param name="calledFromCode">false if called by GC true if called from code</param>
        private void Dispose(bool calledFromCode)
        {
            if (calledFromCode)
            {
            }

            this.fileInformation = null;
        }
    }
}