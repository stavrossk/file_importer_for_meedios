﻿using System;
using CustomProperties.PropertyData;





namespace FileImporter.ImporterProperties
{



    class ImageAssociationPropertiesGetter
    {







        internal static bool GetPropertyForImageAssociation
            (ImporterPropertiesCore.FileImporterProperties fileImporterProperties,
            int index, ref int i, OptionSettings prop)
        {
            if (index == i++)
            {
                prop.Name = "AlwaysUpdateImages";
                prop.Caption = fileImporterProperties.AlwaysUpdateImagesCaption;
                prop.HelpText = fileImporterProperties.AlwaysUpdateImagesHelptext;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupCaption = fileImporterProperties.miscGroupcaption + ":";
                prop.GroupName = "ImageAssociationRules";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFolder";
                prop.Caption = fileImporterProperties.ImageFolderCaption;
                prop.HelpText = fileImporterProperties.ImageFolderHelptext;
                prop.DataType = "folder";
                prop.GroupName = "ImageAssociationRules";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "FileFolderImage";
                prop.Caption = fileImporterProperties.FileFolderImageCaption;
                prop.HelpText = fileImporterProperties.FileFolderImageHelptext;
                prop.DataType = "string";
                prop.Choices = new[]
                                   {
                                       fileImporterProperties.FileFolderImageChoice1.Translate(),
                                       fileImporterProperties.FileFolderImageChoice2.Translate(),
                                       fileImporterProperties.FileFolderImageChoice3.Translate()
                                   };
                prop.DefaultValue = fileImporterProperties.FileFolderImageChoice1.Translate();
                prop.GroupName = "ImageAssociationRules";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFileMasks";
                prop.Caption = fileImporterProperties.ImageFileMasksCaption;
                prop.HelpText = fileImporterProperties.ImageFileMasksHelptext;
                prop.DataType = "string";
                prop.CanTypeChoices = true;
                prop.Choices = new[] { "<name>.png;<name>.jpg;<name>.jpeg;<name>.bmp", "*.png;*.jpg;*.jpeg;*.bmp" };
                prop.DefaultValue = "<name>.png;<name>.jpg;<name>.jpeg;<name>.bmp";
                prop.GroupName = "ImageAssociationRules";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageTagName";
                prop.Caption = fileImporterProperties.ImageTagNameCaption;
                prop.HelpText = fileImporterProperties.imageTagNameHelptext;
                prop.DataType = "string";
                prop.DefaultValue = String.Empty;
                prop.GroupName = "ImageAssociationRules";
                prop.Dependencies = "default:::ImageTagName = " + prop.DefaultValue + ";;;Default:::ImageTagName = " +
                                    prop.DefaultValue + ";;;:::SaveInImageBaseTag = false|||HIDE SaveInImageBaseTag" +
                                    ";;;..*:::SHOW SaveInImageBaseTag";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageCount";
                prop.Caption = fileImporterProperties.imageCountCaption;
                prop.HelpText = fileImporterProperties.imageCountHelptext;
                prop.DataType = "int";
                prop.DefaultValue = 1;
                prop.GroupName = "ImageAssociationRules";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "SaveInImageBaseTag";
                prop.Caption = fileImporterProperties.SaveInImageBaseTagCaption;
                prop.HelpText = fileImporterProperties.SaveInImageBaseTagHelptext;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupName = "ImageAssociationRules";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "AddImageSearchToggle";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.addImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.addImageSearchToggleHelptext;
                prop.GroupName = "ImageAssociationRules";


                prop.Dependencies = fileImporterProperties
                    .addImageSearchToggleDefault.Translate() 
                    + ":::ImageSearchActive2 = true";


                prop.DataType = "toggle";
                return true;
            }




            if (index == i++)
            {
                prop.Name = "ImageSearchActive2";
                prop.Caption = String.Empty;
                prop.HelpText = String.Empty;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupCaption = fileImporterProperties.miscGroupcaption + " 2:";
                prop.GroupName = "ImageAssociationRules2";
                prop.Dependencies =
                    "True:::SHOWGROUP ImageAssociationRules2|||HIDE AddImageSearchToggle|||HIDE ImageSearchActive2;;;False:::HIDEGROUP ImageAssociationRules2|||SHOW AddImageSearchToggle|||HIDE ImageSearchActive2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFolder2";
                prop.Caption = fileImporterProperties.ImageFolderCaption;
                prop.HelpText = fileImporterProperties.ImageFolderHelptext;
                prop.DataType = "folder";
                prop.GroupName = "ImageAssociationRules2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "FileFolderImage2";
                prop.Caption = fileImporterProperties.FileFolderImageCaption;
                prop.HelpText = fileImporterProperties.FileFolderImageHelptext;
                prop.DataType = "string";
                prop.Choices = new[]
                                   {
                                       fileImporterProperties.FileFolderImageChoice1.Translate(), fileImporterProperties.FileFolderImageChoice2.Translate(),
                                       fileImporterProperties.FileFolderImageChoice3.Translate()
                                   };
                prop.DefaultValue = fileImporterProperties.FileFolderImageChoice1.Translate();
                prop.GroupName = "ImageAssociationRules2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFileMasks2";
                prop.Caption = fileImporterProperties.ImageFileMasksCaption;
                prop.HelpText = fileImporterProperties.ImageFileMasksHelptext;
                prop.DataType = "string";
                prop.CanTypeChoices = true;
                prop.Choices = new[] { "<name>.png;<name>.jpg;<name>.jpeg;<name>.bmp", "*.png;*.jpg;*.jpeg;*.bmp" };
                prop.DefaultValue = "<name>.*";
                prop.GroupName = "ImageAssociationRules2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageTagName2";
                prop.Caption = fileImporterProperties.ImageTagNameCaption;
                prop.HelpText = fileImporterProperties.imageTagNameHelptext;
                prop.DataType = "string";
                prop.DefaultValue = "Fanart";
                prop.GroupName = "ImageAssociationRules2";
                prop.Dependencies = "default:::ImageTagName2 = " + prop.DefaultValue +
                                    ";;;Default:::ImageTagName2 = " + prop.DefaultValue +
                                    ";;;:::SaveInImageBaseTag2 = false|||HIDE SaveInImageBaseTag2" +
                                    ";;;..*:::SHOW SaveInImageBaseTag2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageCount2";
                prop.Caption = fileImporterProperties.imageCountCaption;
                prop.HelpText = fileImporterProperties.imageCountHelptext;
                prop.DataType = "int";
                prop.DefaultValue = 1;
                prop.GroupName = "ImageAssociationRules2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "SaveInImageBaseTag2";
                prop.Caption = fileImporterProperties.SaveInImageBaseTagCaption;
                prop.HelpText = fileImporterProperties.SaveInImageBaseTagHelptext;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupName = "ImageAssociationRules2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "RemoveImageSearchToggle2";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.RemoveImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.RemoveImageSearchToggleHelptext;
                prop.Dependencies = fileImporterProperties.RemoveImageSearchToggleDefault.Translate() +
                                    ":::ImageSearchActive2 = false";
                prop.DataType = "toggle";
                prop.GroupName = "ImageAssociationRules2";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "AddImageSearchToggle2";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.addImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.addImageSearchToggleHelptext;
                prop.GroupName = "ImageAssociationRules2";
                prop.Dependencies = fileImporterProperties.addImageSearchToggleDefault.Translate() + ":::ImageSearchActive3 = true";
                prop.DataType = "toggle";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageSearchActive3";
                prop.Caption = String.Empty;
                prop.HelpText = String.Empty;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupCaption = fileImporterProperties.miscGroupcaption + " 3:";
                prop.GroupName = "ImageAssociationRules3";
                prop.Dependencies =
                    "True:::SHOWGROUP ImageAssociationRules3|||HIDE AddImageSearchToggle2|||HIDE ImageSearchActive3;;;False:::HIDEGROUP ImageAssociationRules3|||SHOW AddImageSearchToggle2|||HIDE ImageSearchActive3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFolder3";
                prop.Caption = fileImporterProperties.ImageFolderCaption;
                prop.HelpText = fileImporterProperties.ImageFolderHelptext;
                prop.DataType = "folder";
                prop.GroupName = "ImageAssociationRules3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "FileFolderImage3";
                prop.Caption = fileImporterProperties.FileFolderImageCaption;
                prop.HelpText = fileImporterProperties.FileFolderImageHelptext;
                prop.DataType = "string";
                prop.Choices = new[]
                                   {
                                       fileImporterProperties.FileFolderImageChoice1.Translate(), fileImporterProperties.FileFolderImageChoice2.Translate(),
                                       fileImporterProperties.FileFolderImageChoice3.Translate()
                                   };
                prop.DefaultValue = fileImporterProperties.FileFolderImageChoice1.Translate();
                prop.GroupName = "ImageAssociationRules3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFileMasks3";
                prop.Caption = fileImporterProperties.ImageFileMasksCaption;
                prop.HelpText = fileImporterProperties.ImageFileMasksHelptext;
                prop.DataType = "string";
                prop.CanTypeChoices = true;
                prop.Choices = new[] { "<name>.png;<name>.jpg;<name>.jpeg;<name>.bmp", "*.png;*.jpg;*.jpeg;*.bmp" };
                prop.DefaultValue = "<SeriesID>-<SeasonNumber>.*";
                prop.GroupName = "ImageAssociationRules3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageTagName3";
                prop.Caption = fileImporterProperties.ImageTagNameCaption;
                prop.HelpText = fileImporterProperties.imageTagNameHelptext;
                prop.DataType = "string";
                prop.DefaultValue = "SeasonImage";
                prop.GroupName = "ImageAssociationRules3";
                prop.Dependencies = "default:::ImageTagName3 = " + prop.DefaultValue +
                                    ";;;Default:::ImageTagName3 = " + prop.DefaultValue +
                                    ";;;:::SaveInImageBaseTag3 = false|||HIDE SaveInImageBaseTag3" +
                                    ";;;..*:::SHOW SaveInImageBaseTag3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageCount3";
                prop.Caption = fileImporterProperties.imageCountCaption;
                prop.HelpText = fileImporterProperties.imageCountHelptext;
                prop.DataType = "int";
                prop.DefaultValue = 1;
                prop.GroupName = "ImageAssociationRules3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "SaveInImageBaseTag3";
                prop.Caption = fileImporterProperties.SaveInImageBaseTagCaption;
                prop.HelpText = fileImporterProperties.SaveInImageBaseTagHelptext;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupName = "ImageAssociationRules3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "RemoveImageSearchToggle3";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.RemoveImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.RemoveImageSearchToggleHelptext;
                prop.Dependencies = fileImporterProperties.RemoveImageSearchToggleDefault.Translate() +
                                    ":::ImageSearchActive3 = false";
                prop.DataType = "toggle";
                prop.GroupName = "ImageAssociationRules3";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "AddImageSearchToggle3";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.addImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.addImageSearchToggleHelptext;
                prop.GroupName = "ImageAssociationRules3";
                prop.Dependencies = fileImporterProperties.addImageSearchToggleDefault.Translate() + ":::ImageSearchActive4 = true";
                prop.DataType = "toggle";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageSearchActive4";
                prop.Caption = String.Empty;
                prop.HelpText = String.Empty;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupCaption = fileImporterProperties.miscGroupcaption + " 4:";
                prop.GroupName = "ImageAssociationRules4";
                prop.Dependencies =
                    "True:::SHOWGROUP ImageAssociationRules4|||HIDE AddImageSearchToggle3|||HIDE ImageSearchActive4;;;False:::HIDEGROUP ImageAssociationRules4|||SHOW AddImageSearchToggle3|||HIDE ImageSearchActive4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFolder4";
                prop.Caption = fileImporterProperties.ImageFolderCaption;
                prop.HelpText = fileImporterProperties.ImageFolderHelptext;
                prop.DataType = "folder";
                prop.GroupName = "ImageAssociationRules4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "FileFolderImage4";
                prop.Caption = fileImporterProperties.FileFolderImageCaption;
                prop.HelpText = fileImporterProperties.FileFolderImageHelptext;
                prop.DataType = "string";
                prop.Choices = new[]
                                   {
                                       fileImporterProperties.FileFolderImageChoice1.Translate(), fileImporterProperties.FileFolderImageChoice2.Translate(),
                                       fileImporterProperties.FileFolderImageChoice3.Translate()
                                   };
                prop.DefaultValue = fileImporterProperties.FileFolderImageChoice1.Translate();
                prop.GroupName = "ImageAssociationRules4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFileMasks4";
                prop.Caption = fileImporterProperties.ImageFileMasksCaption;
                prop.HelpText = fileImporterProperties.ImageFileMasksHelptext;
                prop.DataType = "string";
                prop.CanTypeChoices = true;
                prop.Choices = new[] { "<name>.png;<name>.jpg;<name>.jpeg;<name>.bmp", "*.png;*.jpg;*.jpeg;*.bmp" };
                prop.DefaultValue = "<SeriesID>.*";
                prop.GroupName = "ImageAssociationRules4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageTagName4";
                prop.Caption = fileImporterProperties.ImageTagNameCaption;
                prop.HelpText = fileImporterProperties.imageTagNameHelptext;
                prop.DataType = "string";
                prop.DefaultValue = "SeriesPoster";
                prop.GroupName = "ImageAssociationRules4";
                prop.Dependencies = "default:::ImageTagName4 = " + prop.DefaultValue +
                                    ";;;Default:::ImageTagName4 = " + prop.DefaultValue +
                                    ";;;:::SaveInImageBaseTag4 = false|||HIDE SaveInImageBaseTag4" +
                                    ";;;..*:::SHOW SaveInImageBaseTag4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageCount4";
                prop.Caption = fileImporterProperties.imageCountCaption;
                prop.HelpText = fileImporterProperties.imageCountHelptext;
                prop.DataType = "int";
                prop.DefaultValue = 1;
                prop.GroupName = "ImageAssociationRules4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "SaveInImageBaseTag4";
                prop.Caption = fileImporterProperties.SaveInImageBaseTagCaption;
                prop.HelpText = fileImporterProperties.SaveInImageBaseTagHelptext;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupName = "ImageAssociationRules4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "RemoveImageSearchToggle4";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.RemoveImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.RemoveImageSearchToggleHelptext;
                prop.Dependencies = fileImporterProperties.RemoveImageSearchToggleDefault.Translate() +
                                    ":::ImageSearchActive4 = false";
                prop.DataType = "toggle";
                prop.GroupName = "ImageAssociationRules4";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "AddImageSearchToggle4";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.addImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.addImageSearchToggleHelptext;
                prop.GroupName = "ImageAssociationRules4";
                prop.Dependencies = fileImporterProperties.addImageSearchToggleDefault.Translate() + ":::ImageSearchActive5 = true";
                prop.DataType = "toggle";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageSearchActive5";
                prop.Caption = String.Empty;
                prop.HelpText = String.Empty;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupCaption = fileImporterProperties.miscGroupcaption + " 5:";
                prop.GroupName = "ImageAssociationRules5";
                prop.Dependencies =
                    "True:::SHOWGROUP ImageAssociationRules5|||HIDE AddImageSearchToggle4|||HIDE ImageSearchActive5;;;False:::HIDEGROUP ImageAssociationRules5|||SHOW AddImageSearchToggle4|||HIDE ImageSearchActive5";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFolder5";
                prop.Caption = fileImporterProperties.ImageFolderCaption;
                prop.HelpText = fileImporterProperties.ImageFolderHelptext;
                prop.DataType = "folder";
                prop.GroupName = "ImageAssociationRules5";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "FileFolderImage5";
                prop.Caption = fileImporterProperties.FileFolderImageCaption;
                prop.HelpText = fileImporterProperties.FileFolderImageHelptext;
                prop.DataType = "string";
                prop.Choices = new[]
                                   {
                                       fileImporterProperties.FileFolderImageChoice1.Translate(), fileImporterProperties.FileFolderImageChoice2.Translate(),
                                       fileImporterProperties.FileFolderImageChoice3.Translate()
                                   };
                prop.DefaultValue = fileImporterProperties.FileFolderImageChoice1.Translate();
                prop.GroupName = "ImageAssociationRules5";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageFileMasks5";
                prop.Caption = fileImporterProperties.ImageFileMasksCaption;
                prop.HelpText = fileImporterProperties.ImageFileMasksHelptext;
                prop.DataType = "string";
                prop.CanTypeChoices = true;
                prop.Choices = new[] { "<name>.png;<name>.jpg;<name>.jpeg;<name>.bmp", "*.png;*.jpg;*.jpeg;*.bmp" };
                prop.DefaultValue = "<SeriesID>.*";
                prop.GroupName = "ImageAssociationRules5";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageTagName5";
                prop.Caption = fileImporterProperties.ImageTagNameCaption;
                prop.HelpText = fileImporterProperties.imageTagNameHelptext;
                prop.DataType = "string";
                prop.DefaultValue = "SeriesBanner";
                prop.GroupName = "ImageAssociationRules5";
                prop.Dependencies = "default:::ImageTagName5 = " + prop.DefaultValue +
                                    ";;;Default:::ImageTagName5 = " + prop.DefaultValue +
                                    ";;;:::SaveInImageBaseTag5 = false|||HIDE SaveInImageBaseTag5" +
                                    ";;;..*:::SHOW SaveInImageBaseTag5";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "ImageCount5";
                prop.Caption = fileImporterProperties.imageCountCaption;
                prop.HelpText = fileImporterProperties.imageCountHelptext;
                prop.DataType = "int";
                prop.DefaultValue = 1;
                prop.GroupName = "ImageAssociationRules5";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "SaveInImageBaseTag5";
                prop.Caption = fileImporterProperties.SaveInImageBaseTagCaption;
                prop.HelpText = fileImporterProperties.SaveInImageBaseTagHelptext;
                prop.DataType = "bool";
                prop.DefaultValue = false;
                prop.GroupName = "ImageAssociationRules5";
                return true;
            }

            if (index == i++)
            {
                prop.Name = "RemoveImageSearchToggle5";
                prop.Caption = String.Empty;
                prop.DefaultValue = fileImporterProperties.RemoveImageSearchToggleDefault.Translate();
                prop.HelpText = fileImporterProperties.RemoveImageSearchToggleHelptext;
                prop.Dependencies = fileImporterProperties.RemoveImageSearchToggleDefault.Translate() +
                                    ":::ImageSearchActive5 = false";
                prop.DataType = "toggle";
                prop.GroupName = "ImageAssociationRules5";
                return true;
            }

            return false;
        }
    }
}
