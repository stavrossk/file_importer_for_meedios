﻿using System;
using System.Linq;
using MCW.Utility.MeediOS;



namespace FileImporter.RootFolderHandler
{



    class ChainedFileHandler
    {




        protected internal static int HandleChainedByTagFile
            (EnhancedFileInfo chain, 
             EnhancedFileInfo newElement)
        {


            MLTag location = new MLTag("Location", chain.Item.Location);
            
            
            int fileIndex =
                location.Values.Select(value => value.ToLowerInvariant()).ToList().IndexOf(
                    newElement.FileInformation.FullName.ToLowerInvariant());
            
            
            MLTag externalID = new MLTag("ExternalID", chain.Item.ExternalID);
            
            
            if (fileIndex >= 0)
            {
                location.Values[fileIndex] = newElement.Item.Location;
                chain.Item.Location = location.Value;
                externalID.Values[fileIndex] = newElement.Item.ExternalID;
                chain.Item.ExternalID = externalID.Value;
            }
            else
            {
                location.Values.Add(newElement.Item.Location);
                location.Values.Sort();
                chain.Item.Location = location.Value;
                externalID.Values.Add(newElement.Item.ExternalID);
                externalID.Values.Sort();
                chain.Item.ExternalID = externalID.Value;
            }

            return fileIndex;
        }

        internal static void HandleChainedFile
            (RootFolderHandlerBase rootFolderHandlerBase,
             EnhancedFileInfo theFile, 
             EnhancedFileInfo previousFile)
        {
            if ((!rootFolderHandlerBase.IsNewItem)
                && (!rootFolderHandlerBase.Update))
            {
                return;
            }



            var location = new MLTag
                ("item_location", 
                previousFile.Item.Location);
            

            location.Values.Add(theFile.FileInformation.FullName);
            
            location.Values.Sort();
            
            previousFile.Item.Location = location.Value;

            previousFile.Item.ExternalID
                = previousFile.Item.Location.ToLower();


            if (rootFolderHandlerBase.Importer.FileSize)
            {
                previousFile.Item.Tags[rootFolderHandlerBase.Importer.FileSizeTagName] =
                    (long)previousFile.Item.Tags[rootFolderHandlerBase.Importer.FileSizeTagName] + theFile.FileInformation.Length;
            }

            if (rootFolderHandlerBase.Importer.CreationDate)
            {
                object creationDate = previousFile.Item.Tags[rootFolderHandlerBase.Importer.CreationDateTagName];
                if (creationDate is string)
                {
                    if (!String.IsNullOrEmpty((string)creationDate))
                    {
                        if (String.CompareOrdinal(theFile.FileInformation.CreationTime.ToString("yyyy-MM-dd hh:mm:ss"), (string)creationDate) < 0)
                        {
                            previousFile.Item.Tags[rootFolderHandlerBase.Importer.CreationDateTagName] = theFile.FileInformation.CreationTime.ToString("yyyy-MM-dd hh:mm:ss");
                        }
                    }
                }
            }

            if (rootFolderHandlerBase.Importer.LastAccessDate)
            {

                object lastAccessDate = previousFile.Item.Tags[rootFolderHandlerBase.Importer.LastAccessDateTagName];


                if (lastAccessDate is string)
                {
                    if (!String.IsNullOrEmpty((string)lastAccessDate))
                    {
                        if (String.CompareOrdinal(theFile.FileInformation.LastAccessTime.ToString("yyyy-MM-dd hh:mm:ss"), (string)lastAccessDate) > 0)
                        {
                            previousFile.Item.Tags[rootFolderHandlerBase.Importer.LastAccessDateTagName] = theFile.FileInformation.LastAccessTime.ToString("yyyy-MM-dd hh:mm:ss");
                        }
                    }
                }
            }


            if (!rootFolderHandlerBase
                .Importer.LastModificationDate) 
                return;


            object lastModificationDate
                = previousFile.Item.Tags
                [rootFolderHandlerBase
                .Importer.LastModificationDateTagName];


            var modificationDate = lastModificationDate as string;

            if (modificationDate == null)
                return;

            if (String.IsNullOrEmpty
                (modificationDate)) 
                return;

            if (String.CompareOrdinal
                (theFile.FileInformation
                    .LastWriteTime.ToString
                    ("yyyy-MM-dd hh:mm:ss"),
                    modificationDate) <= 0)
                return;


            previousFile.Item.Tags
                [rootFolderHandlerBase
                .Importer.LastModificationDateTagName]
                = theFile.FileInformation
                .LastWriteTime.ToString
                ("yyyy-MM-dd hh:mm:ss");
      
        
        }




    }



}
