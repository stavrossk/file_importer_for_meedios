﻿namespace FileImporter
{
    using System;
    using System.IO;
    using System.Management;
    using MCW.Utility.WMI;
    using MCW.Utility.Logging;

    /// <summary>
    /// Enhances DriveInfo class to also return the serialnumber and discid of the drive
    /// </summary>
    public class EnhancedDriveInfo
    {
        #region Field members

        /// <summary>
        /// The drives serialnumber
        /// </summary>
        private readonly string serialNumber;

        /// <summary>
        /// The drives discid
        /// </summary>
        private string discID;

        /// <summary>
        /// The DriveInfo object that gets enhanced
        /// </summary>
        private DriveInfo driveInfo;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the EnhancedDriveInfo class.
        /// </summary>
        /// <param name="importer"></param>
        /// <param name="driveInfo"></param>
        public EnhancedDriveInfo(CORE.FileImporter importer, DriveInfo driveInfo)
        {
            this.driveInfo = driveInfo;
            this.serialNumber = string.Empty;

            if (driveInfo != null)
            {
                /*
                try
                {
                    char[] trimChars = { '\\' };
                    ManagementObject disk =
                        new ManagementObject("Win32_LogicalDisk.DeviceID=\"" + driveInfo.Name.TrimEnd(trimChars) + "\"");
                    disk.Get();
                    ManagementObject partition =
                        new ManagementObject("Win32_LogicalDiskToPartition.DeviceID=\"" + disk["DeviceID"] + "\"");
                    partition.Get();
                    ManagementObject drive =
                        new ManagementObject("Win32_DiskDrive.DeviceID=\"" + disk["DeviceID"] + "\"");
                    drive.Get();
                    ManagementObject wmi =
                        new ManagementObject("Win32_PhysicalMedia.Tag=\"" + drive["DeviceID"] + "\"");
                    wmi.Get();
//                    this.serialNumber = (string)wmi["VolumeSerialNumber"];
                    this.serialNumber = (wmi.Properties["SerialNumber"].Value != null)
                           ? wmi.Properties["SerialNumber"].Value.ToString().Trim()
                           : string.Empty;
                }
                catch (ManagementException ex)
                {
                    importer.LogMessages.Enqueue(new LogMessage("Error", ex.ToString()));
                }
                catch (Exception ex)
                {
                    importer.LogMessages.Enqueue(new LogMessage("Error", ex.ToString()));
                }
                */
                try
                {
                    this.serialNumber = new PhysicalMediaSerialNumber(driveInfo.Name).GetSerialNumber();
                }
                catch (ArgumentException ex)
                {
                    importer.LogMessages.Enqueue(new LogMessage("Warn", ex.ToString()));

                }
            }
            else
            {
                importer.LogMessages.Enqueue(new LogMessage("Warn", "DriveInfo is null -> SerialNumber and VolumeName will be string.Empty."));
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the drive's serialnumber
        /// </summary>
        public string SerialNumber
        {
            get { return this.serialNumber; }
        }

        /// <summary>
        /// Gets the DriveInfo object that is being enhanced
        /// </summary>
        public DriveInfo DriveInfo
        {
            get { return this.driveInfo; }
        }

        /// <summary>
        /// Gets the drive's volumelabel
        /// </summary>
        public string VolumeLabel
        {
            get
            {
                return this.driveInfo != null ? this.DriveInfo.VolumeLabel : string.Empty;
            }
        }

        #endregion
    }
}