﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MCW.Utility.Logging;




namespace FileImporter.Root_Folders_Analyzer
{



    class RootFoldersAnalyzer
    {




        internal static void AnalyzeRootFoldersByDrives
            (TheImport theImport)
        {



            string mappedRootFolder;




            foreach (string rootFolder in theImport.Importer.RootFolders)
            {


                mappedRootFolder = theImport.RootFolderOrUnc2RootFolder[rootFolder];


                if (mappedRootFolder == " :")
                    continue;


                theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "MappedRootFolder: " + mappedRootFolder));
                string partition = Directory.GetDirectoryRoot(mappedRootFolder);
                try
                {


                    if (!theImport.DriveLetters.Keys.Contains(partition))
                    {
                        theImport.DriveLetters.Add(partition, null);
                    }

                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Debug", "Partition: " + partition));
                    theImport.DriveLetters[partition] = new EnhancedDriveInfo(theImport.Importer, new DriveInfo(partition));
                }
                catch (ArgumentException ex)
                {
                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Error", ex.ToString()));
                    theImport.Importer.LogMessages.FlushAllMessages();
                }
                catch (Exception ex)
                {
                    theImport.Importer.LogMessages.Enqueue(new LogMessage("Error", ex.ToString()));
                    theImport.Importer.LogMessages.FlushAllMessages();
                }




            }








            foreach (KeyValuePair<string, EnhancedDriveInfo> driveLetter in theImport.DriveLetters)
            {
                KeyValuePair<string, EnhancedDriveInfo> letter = driveLetter;
                var results =
                    theImport.RootFoldersOfDrive.Keys.Where(diex => diex.SerialNumber == letter.Value.SerialNumber);
                EnhancedDriveInfo result = null;
                foreach (EnhancedDriveInfo diex in results)
                {
                    result = diex;
                }

                if (result == null)
                {
                    theImport.RootFoldersOfDrive.Add(result = driveLetter.Value, new List<string>());
                }
                
                foreach (string rootFolder in theImport.Importer.RootFolders)
                {
                    mappedRootFolder = theImport.RootFolderOrUnc2RootFolder[rootFolder];
                    if (mappedRootFolder == " :")
                        continue;


                    if (driveLetter.Key == Directory.GetDirectoryRoot(mappedRootFolder))
                    {
                        theImport.RootFoldersOfDrive[result].Add(rootFolder);
                    }


                }



            }








        }



    }





}
